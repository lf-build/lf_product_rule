﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleSegmentRepository : IRepository<ISegment>
    {
        Task<ISegment> GetByName(string entityType, string productId, string Name);
        Task<List<ISegment>> GetAllSegment(string entityType);
    }
}
