﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public class StrategyHistory : Aggregate, IStrategyHistory
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string ProductId { get; set; }
        public TimeBucket ExecutedDate { get; set; }
        public object IntermediateData { get; set; }
        public string StrategyName { get; set; }

        public string StrategyId { get; set; }
        public string StrategyStepName { get; set; }
        public Result Result { get; set; }
        public List<string> ResultDetail { get; set; }
        public List<string> ExceptionDetail { get; set; }


    }
}
