﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.ProductRule.Configurations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IStrategyExceutionHistoryRepository : IRepository<IStrategyHistory>
    {
        Task<List<IStrategyHistory>> GetByStrategyId(string entityType,string entityId, string productId, string Name);
    }
}
