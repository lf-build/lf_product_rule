﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleLookupRepository : IRepository<ILookup>
    {
        Task<ILookup> Update(string lookupId, ILookup updateRequest);
        Task<ILookup> GetByName(string entityType, string productId, string name);
        Task<ILookup> GetById(string lookupId);
        Task<List<ILookup>> GetAllLookup(string entityType);
        //Task<ILookupData> GetLookData(string entityType, string productId, string lookupName);
        //Task<ISellRateSegmentation> GetLookDataByGrade(string entityType, string riskGrade, int tier);
    }
}
