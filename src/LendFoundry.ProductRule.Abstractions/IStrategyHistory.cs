﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IStrategyHistory : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string ProductId { get; set; }
        TimeBucket ExecutedDate { get; set; }
        object IntermediateData { get; set; }
        string StrategyName { get; set; }

        string StrategyId { get; set; }
        string StrategyStepName { get; set; }
        Result Result { get; set; }
        List<string> ResultDetail { get; set; }
        List<string> ExceptionDetail { get; set; }


    }
}
