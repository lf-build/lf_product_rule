﻿namespace LendFoundry.ProductRule
{
    public enum ApplicationType
    {
        SystemDate,
        ApplicationDate,      
        CustomDate
    }
}