﻿namespace LendFoundry.ProductRule.Events
{
    public class ProductRuleExecuted
    {
        public IProductRuleResult ProductRuleResult { get; set; }
    }
}
