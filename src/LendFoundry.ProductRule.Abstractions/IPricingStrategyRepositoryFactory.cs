﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.ProductRule
{
   

    public interface IPricingStrategyRepositoryFactory
    {
        IPricingStrategyRepository Create(ITokenReader reader);
    }
}
