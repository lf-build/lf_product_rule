﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.ProductRule
{
   

    public interface IProductRuleLookupRepositoryFactory
    {
        IProductRuleLookupRepository Create(ITokenReader reader);
    }
}
