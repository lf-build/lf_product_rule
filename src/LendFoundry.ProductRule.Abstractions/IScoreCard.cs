﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public interface IScoreCard : IAggregate
    {
        string EntityType { get; set; }
        string ProductId { get; set; }
        string RuleDefinitionId { get; set; }
        string ScoreCardName { get; set; }
        string Description { get; set; }
        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }
        TimeBucket CreatedOn { get; set; }
        TimeBucket UpdatedOn { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IScoreCardVariable, ScoreCardVariable>))]
        List<IScoreCardVariable> ScoreCardVariables { get; set; }
        TimeBucket EffectiveDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
        TimeBucket CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }

        bool IsValidated { get; set; }

        string ValidationRuleId { get; set; }
    }
}
