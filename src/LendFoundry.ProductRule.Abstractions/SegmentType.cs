﻿namespace LendFoundry.ProductRule
{
    public enum SegmentType
    {
        Range = 1,
        List = 2,
        Rule = 3
    }
}
