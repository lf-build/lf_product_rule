﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public interface IPricingStrategy : IAggregate
    {
        string EntityType { get; set; }
        string ProductId { get; set; }
        string Name { get; set; }
        string Description { get; set; }

        string Version { get; set; }
        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }
        TimeBucket CreatedOn { get; set; }
        TimeBucket UpdatedOn { get; set; }

        TimeBucket EffectiveDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPricingStrategyStep, PricingStrategyStep>))]
        List<IPricingStrategyStep> PricingStrategyStep { get; set; }
    }
}
