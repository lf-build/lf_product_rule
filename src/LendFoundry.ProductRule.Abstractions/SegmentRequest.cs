﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public class SegmentRequest : ISegmentRequest
    {
    public    string SegmentName { get; set; }
        public string DataAttributeStoreName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IVariableSource, VariableSource>))]
        public IVariableSource SegmentVariableSource { get; set; }
        public string Description { get; set; }
        public SegmentType SegmentType { get; set; }//Range/List/Rule
        public string SegmentStorageName { get; set; }// Reference of mongo document name 
        public string RuleDefinitionId { get; set; }// For type rule 
        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }

        public object Data { get; set; }

        public DateTimeOffset CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }
    }
}
