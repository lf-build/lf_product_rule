﻿namespace LendFoundry.ProductRule
{
    public enum StepType
    {
        ScoreCard = 1,
        Lookup = 2,
        Segment = 3,
        Rule = 4,
        ProductParameters = 5
    }
}
