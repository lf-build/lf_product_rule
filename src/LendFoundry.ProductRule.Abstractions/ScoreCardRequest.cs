﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public class ScoreCardRequest : IScoreCardRequest
    {
        public string RuleDefinitionId { get; set; }

        public string ScoreCardName { get; set; }

        public string Description { get; set; }
        public DateTimeOffset CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }
        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }

        public bool IsValidated { get; set; }

        public string ValidationRuleId { get; set; }

    }
}
