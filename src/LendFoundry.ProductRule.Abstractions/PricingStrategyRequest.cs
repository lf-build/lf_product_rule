﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public class PricingStrategyRequest : IPricingStrategyRequest
    {
      
        public string Name { get; set; }
        public string Description { get; set; }

        public string Version { get; set; }

        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }

    }
}
