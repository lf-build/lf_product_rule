﻿using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleSourceResult
    {
        Dictionary<string, object> Data { get; set; }
        List<string> Detail { get; set; }
        Result Result { get; set; }
        List<string> Exception { get; set; }
        string RejectCode { get; set; }

        Dictionary<string, object> SourceData { get; set; }
    }
}