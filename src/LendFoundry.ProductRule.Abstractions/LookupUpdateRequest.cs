﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public class UpdateLookupRequest : IUpdateLookupRequest
    {
        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public List<string> LookupBy { get; set; }
        public List<string> LookupValues { get; set; }
        public string LookupStorageName { get; set; }
        public string Description { get; set; }
        public DateTimeOffset LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string DataAttributeStoreName { get; set; }
        public string RuleDefinitionId { get; set; }
        public object LookupData { get; set; }

        public DateTimeOffset CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }
    }
}
