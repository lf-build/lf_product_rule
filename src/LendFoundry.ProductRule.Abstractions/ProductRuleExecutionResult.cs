﻿using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class ProductRuleSourceResult : IProductRuleSourceResult
    {
        public Dictionary<string, object> Data { get; set; }
        public List<string> Detail { get; set; }
        public Result Result { get; set; }
        public List<string> Exception { get; set; }
        public string RejectCode { get; set; }
        public Dictionary<string, object> SourceData { get; set; }
    }
}
