﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IRuleDefinitionAddRequest
    {
         string RuleName { get; set; }
         string DataAttributeStoreName { get; set; }
        DateTimeOffset EffectiveDate { get; set; }
        DateTimeOffset ExpiryDate { get; set; }
         string RuleClass { get; set; }
         string RuleType { get; set; }
         string DERuleName { get; set; }
         string RuleVersion { get; set; }
         List<string> RequiredSources { get; set; }
         List<string> OptionalSources { get; set; }

        List<string> GroupNames { get; set; }
        string CreatedBy { get; set; }
         string Description { get; set; }
        DateTimeOffset CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }

    }
}
