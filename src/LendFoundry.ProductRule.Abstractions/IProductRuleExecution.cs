﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.ProductRule.Configurations;
using LendFoundry.ProductRule;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleResult : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        TimeBucket ExecutedDate { get; set; }
        object IntermediateData { get; set; }
        string Name { get; set; }
        Result Result { get; set; }
        List<string> ResultDetail { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRuleDefinition, RuleDefinition>))]
         IRuleDefinition ConfigurationDetail { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRuleDefinition, RuleDefinition>))]
        IRuleDefinition RuleDefinitionDetail { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISegment, Segment>))]
        ISegment SegmentDetail { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILookup, Lookup>))]
        ILookup lookupDetail { get; set; }
        Dictionary<string, object> SourceData { get; set; }
        List<string> ExceptionDetail { get; set; }
    }
}