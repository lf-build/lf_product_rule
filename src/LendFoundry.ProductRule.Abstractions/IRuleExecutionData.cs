﻿using System;
using LendFoundry.Foundation.Date;

namespace LendFoundry.ProductRule
{
    public  interface IRuleExecutionData
    {
        string Name { get; set; }
        string TypeOfRule { get; set; }
        object Data { get; set; }

        TimeBucket ExecutionDate { get; set; }
    }
}
