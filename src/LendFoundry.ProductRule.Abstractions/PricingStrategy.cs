﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class PricingStrategy : Aggregate, IPricingStrategy
    {
        public string EntityType { get; set; }
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public TimeBucket CreatedOn { get; set; }
        public TimeBucket UpdatedOn { get; set; }
        public TimeBucket EffectiveDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPricingStrategyStep, PricingStrategyStep>))]
        public List<IPricingStrategyStep> PricingStrategyStep { get; set; }
    }
}

