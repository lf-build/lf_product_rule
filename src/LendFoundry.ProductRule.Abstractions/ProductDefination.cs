﻿using System;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class ProductDefination 
    {
        public string ProductId { get; set; }
        public Product Product { get; set; }
        public List<ProductGroup> ProductGroup { get; set; }
        public List<ProductParameter> ProductParameter { get; set; }
        
    }


    public class ValidFrom
    {
        public DateTimeOffset Time { get; set; }
        public string Hour { get; set; }
        public string Day { get; set; }
        public string Week { get; set; }
        public string Month { get; set; }
        public string Quarter { get; set; }
        public string Year { get; set; }
    }

    public class ExpiresOn
    {
        public DateTimeOffset Time { get; set; }
        public string Hour { get; set; }
        public string Day { get; set; }
        public string Week { get; set; }
        public string Month { get; set; }
        public string Quarter { get; set; }
        public string Year { get; set; }
    }

    

    public class Document
    {
        public string ConsentId { get; set; }
        public string ConsentName { get; set; }
        public string ConsentCategory { get; set; }
        public string ConsentPurpose { get; set; }
        public string TemplateId { get; set; }
        public string DocumentDisplayName { get; set; }
        public ValidFrom ValidFrom { get; set; }
        public ExpiresOn ExpiresOn { get; set; }
        public int Format { get; set; }
        public string Version { get; set; }
        public List<string> Tags { get; set; }
    }

    public class CreatedOn
    {
        public DateTimeOffset Time { get; set; }
        public string Hour { get; set; }
        public string Day { get; set; }
        public string Week { get; set; }
        public string Month { get; set; }
        public string Quarter { get; set; }
        public string Year { get; set; }
    }

    public class WorkFlow
    {
        public string Id { get; set; }
        public string EntityType { get; set; }
        public string WorkFlowName { get; set; }
        public int WorkFlowType { get; set; }
        public string Description { get; set; }
        public ValidFrom ValidFrom { get; set; }
        public ExpiresOn ExpiresOn { get; set; }
    }

    public class Product
    {
        public string ProductId { get; set; }
        public int PortfolioType { get; set; }
        public int ProductCategory { get; set; }
        public bool IsSecured { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FamilyId { get; set; }
        public int Status { get; set; }
        public ValidFrom ValidFrom { get; set; }
        public ExpiresOn ExpiresOn { get; set; }
        public List<Document> Documents { get; set; }
        public List<WorkFlow> WorkFlow { get; set; }
        public string Id { get; set; }
    }



    public class ProductGroup
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public ValidFrom ValidFrom { get; set; }
        public ExpiresOn ExpiresOn { get; set; }
        public CreatedOn CreatedOn { get; set; }
        public object UpdatedOn { get; set; }
        public string Id { get; set; }
    }

    public class ProductParameter
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string Purpose { get; set; }

        public string ParameterType { get; set; }
        public string Value { get; set; }
        public string GroupId { get; set; }
        public string CalculationRuleId { get; set; }
        public string ApplicableRuleId { get; set; }
        public string Id { get; set; }
    }

   
}
