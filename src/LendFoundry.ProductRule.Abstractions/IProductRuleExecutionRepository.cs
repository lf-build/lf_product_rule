﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleExecutionRepository : IRepository<IProductRuleResult>
    {
        Task<List<IProductRuleResult>> GetProductRuleDetails(string entityType, string entityId);
    }
}
