﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleSegmentRepositoryFactory
    {
        IProductRuleSegmentRepository Create(ITokenReader reader);
    }
}
