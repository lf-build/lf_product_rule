﻿using System;
using LendFoundry.Foundation.Date;

namespace LendFoundry.ProductRule
{
    public interface IExtractedData
    {
        string Name { get; set; }
        TimeBucket ExtractedDate { get; set; }
        object Data { get; set; }
    }
}
