﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace LendFoundry.ProductRule
{
    public class RuleDefinitionUpdateRequest :IRuleDefinitionUpdateRequest
    {
        public string RuleClass { get; set; }
        public string DERuleName { get; set; }
        public string RuleVersion { get; set; }
        public List<string> RequiredSources { get; set; }
        public List<string> OptionalSources { get; set; }
        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public string Description { get; set; }
        public TimeBucket LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string DataAttributeStoreName { get; set; }
        public DateTimeOffset CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }

    }
}

