﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public interface ILookup : IAggregate
    {
        string EntityType { get; set; }
        string ProductId { get; set; }
        string LookupName { get; set; }
        string DataAttributeStoreName { get; set; }
        TimeBucket EffectiveDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
        List<string> LookupBy { get; set; }
        List<string> LookupValues { get; set; }
        string LookupStorageName { get; set; }
        string Description { get; set; }
        TimeBucket LastUpdatedDate { get; set; }
        string LastUpdatedBy { get; set; }
        string CreatedBy { get; set; }
        TimeBucket CreatedDate { get; set; }
        string RuleDefinitionId { get; set; }

        object Data { get; set; }
         TimeBucket CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }
    }
}
