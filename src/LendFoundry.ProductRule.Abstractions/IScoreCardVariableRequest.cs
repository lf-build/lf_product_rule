﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IScoreCardVariableRequest
    {
         string VariableName { get; set; }
         VariableType VariableType { get; set; }//data attribute/DE rule
         string DefinitionId { get; set; }
         string DataAttributeStoreName { get; set; }
         IVariableSource ScoreCardVariableSource { get; set; }
         double Weightage { get; set; }
         string Description { get; set; }
    }
}
