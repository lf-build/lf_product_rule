﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class ScoreCard : Aggregate,IScoreCard
    {
        public string EntityType { get; set; }
        public string ProductId { get; set; }
        public string RuleDefinitionId { get; set; }
        public string ScoreCardName { get; set; }

        public string Description { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public TimeBucket CreatedOn { get; set; }

        public TimeBucket UpdatedOn { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IScoreCardVariable, ScoreCardVariable>))]
        public List<IScoreCardVariable> ScoreCardVariables { get; set; }
        public TimeBucket EffectiveDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public TimeBucket CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }

        public bool IsValidated { get; set; }

        public string ValidationRuleId { get; set; }
    }
}
 
