﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public class RuleDefinitionAddRequest : IRuleDefinitionAddRequest
    {
        public string RuleName { get; set; }
        public string DataAttributeStoreName { get; set; }
        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public string RuleClass { get; set; }
        public string RuleType { get; set; }
        public string DERuleName { get; set; }
        public string RuleVersion { get; set; }
        public List<string> RequiredSources { get; set; }
        public List<string> OptionalSources { get; set; }

        public List<string> GroupNames { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }
    }
}
