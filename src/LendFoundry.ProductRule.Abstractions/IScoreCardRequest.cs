﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IScoreCardRequest
    {
        string RuleDefinitionId { get; set; }
        string ScoreCardName { get; set; }
        string Description { get; set; }

        bool IsValidated { get; set; }

        string ValidationRuleId { get; set; }
        DateTimeOffset CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }
        DateTimeOffset EffectiveDate { get; set; }
        DateTimeOffset ExpiryDate { get; set; }
    }
}
