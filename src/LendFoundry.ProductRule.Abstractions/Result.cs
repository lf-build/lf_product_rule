﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public enum Result
    {
        UnDefined = 0,
        Passed = 1,
        Failed = 2

    }
}
