﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface ITemplateRepository : IRepository<ITemplate>
    {
        Task<List<ITemplate>> GetAllTemplate(string entityType);
        Task<ITemplate> GetByEventName(string entityType, string productId, string eventName, DateTimeOffset currentDate);
      //  Task<ITemplate> GetEmailByNameAndVersion(string entityType, string productId, string Name, string Version);
      //  Task<ITemplate> GetMobileByNameAndVersion(string entityType, string productId, string Name, string Version);
    }
}
