﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IRuleDetail
    {
        string DERuleName { get; set; }
        string RuleVersion { get; set; }
        List<string> RequiredSources { get; set; }
        List<string> OptionalSources { get; set; }

       
    }
}
