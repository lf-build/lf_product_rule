﻿namespace LendFoundry.ProductRule
{
    public interface IVariableSource
    {
        string InputSource { get; set; } //Data attribute name
        string InputVariable { get; set; }
    }
}
