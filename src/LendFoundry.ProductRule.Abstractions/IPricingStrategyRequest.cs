﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IPricingStrategyRequest
    {

        string Name { get; set; }
        string Description { get; set; }

        string Version { get; set; }

        DateTimeOffset EffectiveDate { get; set; }
        DateTimeOffset ExpiryDate { get; set; }
    }
}
