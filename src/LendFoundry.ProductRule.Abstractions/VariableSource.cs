﻿namespace LendFoundry.ProductRule
{
    public class VariableSource : IVariableSource
    {
          public string InputSource { get; set; } //Data attribute name
          public string InputVariable { get; set; }
    }
}
