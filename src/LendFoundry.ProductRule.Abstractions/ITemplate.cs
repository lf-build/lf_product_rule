﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public interface ITemplate : IAggregate
    {
        string EntityType { get; set; }
        string ProductId { get; set; }
         string EmailTemplateName { get; set; }
         string EmailTemplateVersion { get; set; }
         string MobileTemplateName { get; set; }
         string MobileTemplateVersion { get; set; }
        string Description { get; set; }
        TimeBucket EffectiveDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
        bool IsEnable { get; set; }
        string EventName { get; set; }
        string RuleDefinationName { get; set; }       
        TimeBucket CreatedDate { get; set; }
        TimeBucket LastUpdatedDate { get; set; }
     
    }
}
