﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IPricingStrategyStepRequest
    {
         string StepName { get; set; }

        string ParameterName { get; set; }
        string Description { get; set; }

        List<string> ParameterTypes { get; set; }
        StepType StepType { get; set; }//Score Card,Lookup,Segment

         int SequenceNumber { get; set; }

         bool ContinueOnException { get; set; }
         bool ContinueOnNonEligibility { get; set; }

       
    }
}
