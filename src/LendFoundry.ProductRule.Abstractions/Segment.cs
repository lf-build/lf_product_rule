﻿using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public class Segment : Aggregate, ISegment
    {
        public string EntityType { get; set; }
        public string ProductId { get; set; }
        public string SegmentName { get; set; }
        public string Description { get; set; }
        public string DataAttributeStoreName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IVariableSource, VariableSource>))]
        public IVariableSource SegmentVariableSource { get; set; }
        public SegmentType SegmentType { get; set; }//Range/List/Rule
        public string SegmentStorageName { get; set; }// Reference of mongo document name 
        public string RuleDefinitionId { get; set; }// For type rule 
        public TimeBucket EffectiveDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }      
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public object Data { get; set; }
        public TimeBucket CreatedOn { get; set; }
        public TimeBucket UpdatedOn { get; set; }
        public TimeBucket CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }

    }

    public class SegmentationRange
    {
        public double? LowerLimit { get; set; }
        public double? UpperLimit { get; set; }
        public string SegmentLabel { get; set; }
    }
    public class SegmentationList
    {
        public List<string> InListValues { get; set; }
        public List<string> ExceptListValues { get; set; }
        public string SegmentLabel { get; set; }
    }
}
