﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IRuleDefinitionRepository : IRepository<IRuleDefinition>
    {
        Task<IRuleDefinition> GetById(string ruleId);

        Task<IRuleDefinition> GetByName(string entityType, string productId, string ruleName);

        Task<List<IRuleDefinition>> GetAll(string entityType, string productId, string ruleName);

        Task<IRuleDefinition> Update(string ruleId, IRuleDefinitionUpdateRequest updateRuleRequest);

        Task<List<IRuleDefinition>> GetByRuleDetails(string entityType, string productId, string className);


    }
}
