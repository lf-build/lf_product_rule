﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class PricingStrategyStepRequest : IPricingStrategyStepRequest
    {
        public string StepName { get; set; }

        public string ParameterName { get; set; }

        public List<string> ParameterTypes { get; set; }
        public string Description { get; set; }


        public StepType StepType { get; set; }//Score Card,Lookup,Segment

        public int SequenceNumber { get; set; }

        public bool ContinueOnException { get; set; }
        public bool ContinueOnNonEligibility { get; set; }



    }
}
