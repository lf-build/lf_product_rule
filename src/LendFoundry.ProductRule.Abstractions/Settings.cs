﻿using System;


namespace LendFoundry.ProductRule
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "ProductRule";
    }
}
