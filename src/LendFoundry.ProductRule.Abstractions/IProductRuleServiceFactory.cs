﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.ProductRule.Configurations;
using LendFoundry.Security.Tokens;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleServiceFactory
    {
       IProductRuleService Create(StaticTokenReader reader, ILogger logger, ITenantTime tenantTime, ProductRuleConfiguration configuration);
    }
}