﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public interface IRuleDefinitionUpdateRequest
    {
        DateTimeOffset EffectiveDate { get; set; }
        DateTimeOffset ExpiryDate { get; set; }
        string RuleClass { get; set; }
        string DERuleName { get; set; }
        string RuleVersion { get; set; }
        string DataAttributeStoreName { get; set; }
        List<string> RequiredSources { get; set; }
        List<string> OptionalSources { get; set; }
         string Description { get; set; }
        TimeBucket LastUpdatedDate { get; set; }
         string LastUpdatedBy { get; set; }
        DateTimeOffset CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }
    }
}
