﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public class ScoreCardVariable : IScoreCardVariable
    {
     
        public string ScoreCardVariableId { get; set; }
        public string VariableName { get; set; }
        public string Description { get; set; }
        public VariableType VariableType { get; set; }//data attribute/DE rule
        public string DefinitionId { get; set; }
        public string DataAttributeStoreName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IVariableSource, VariableSource>))]
        public IVariableSource ScoreCardVariableSource { get; set; }
        public double Weightage { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public TimeBucket CreatedOn { get; set; }

        public TimeBucket UpdatedOn { get; set; }

    }
}
