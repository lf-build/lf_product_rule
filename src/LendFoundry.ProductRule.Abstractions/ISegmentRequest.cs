﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface ISegmentRequest
    {
        string SegmentName { get; set; }
        string DataAttributeStoreName { get; set; }
        IVariableSource SegmentVariableSource { get; set; }
        string Description { get; set; }
        SegmentType SegmentType { get; set; }//Range/List/Rule
        string SegmentStorageName { get; set; }// Reference of mongo document name 
        string RuleDefinitionId { get; set; }// For type rule 
        DateTimeOffset EffectiveDate { get; set; }
        DateTimeOffset ExpiryDate { get; set; }

        object Data { get; set; }

        DateTimeOffset CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }
    }
}
