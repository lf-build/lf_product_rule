﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IPricingStrategyRepository : IRepository<IPricingStrategy>
    {
        Task<IPricingStrategy> GetByNameAndVersion(string entityType, string productId, string Name, string Version);
        Task<List<IPricingStrategy>> GetAllPricingStrategy(string entityType);
    }
}
