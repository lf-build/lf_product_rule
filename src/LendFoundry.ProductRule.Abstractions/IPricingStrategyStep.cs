﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public interface IPricingStrategyStep
    {

        string StrategyStepId { get; set; }
        string StepName { get; set; }

        string ParameterName { get; set; }

        List<string> ParameterTypes { get; set; }
        string Description { get; set; }

      
        StepType StepType { get; set; }//Score Card,Lookup,Segment,ProductParameter

        int SequenceNumber { get; set; }      

        bool ContinueOnException { get; set; }
        bool ContinueOnNonEligibility { get; set; }

        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }

        TimeBucket CreatedOn { get; set; }

        TimeBucket UpdatedOn { get; set; }

       

      


    }
}
