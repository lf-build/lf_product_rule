﻿using System;

namespace LendFoundry.ProductRule
{
    public interface ITemplateAddRequest
    {
         string EmailTemplateName { get; set; }
         string EmailTemplateVersion { get; set; }
         string MobileTemplateName { get; set; }
         string MobileTemplateVersion { get; set; }
        string Description { get; set; }
         DateTimeOffset EffectiveDate { get; set; }
         DateTimeOffset ExpiryDate { get; set; }
         bool IsEnable { get; set; }
         string EventName { get; set; }
         string RuleDefinationName { get; set; }       
         DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset UpdatedDate { get; set; }
    }
}