﻿
using System;

namespace LendFoundry.ProductRule
{
    public class TemplateAddRequest: ITemplateAddRequest
    {
        public string EmailTemplateName { get; set; }
        public string EmailTemplateVersion { get; set; }
        public string MobileTemplateName { get; set; }
        public string MobileTemplateVersion { get; set; }
        public string Description { get; set; }
        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public bool IsEnable { get; set; }
        public string EventName { get; set; }
        public string RuleDefinationName { get; set; }
        public string DERuleVersion { get; set; }
        public DateTimeOffset CreatedDate { get; set; }    
        public DateTimeOffset UpdatedDate { get; set; }

    }
}
