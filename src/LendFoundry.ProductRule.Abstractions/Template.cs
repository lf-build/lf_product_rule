﻿
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public class Template :  Aggregate, ITemplate
    {
        public string EntityType { get; set; }
        public string ProductId { get; set; }
        public string EmailTemplateName { get; set; }
        public string EmailTemplateVersion { get; set; }
        public string MobileTemplateName { get; set; }
        public string MobileTemplateVersion { get; set; }
        public string Description { get; set; }
        public TimeBucket EffectiveDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public bool IsEnable { get; set; }
        public string EventName { get; set; }
        public string RuleDefinationName { get; set; }     
        public TimeBucket CreatedDate { get; set; }
        public TimeBucket LastUpdatedDate { get; set; }
      

    }
}
