﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class Lookup : Aggregate, ILookup
    {
        public Lookup() { }

        public Lookup(IUpdateLookupRequest request) {
            Description = request.Description;
            DataAttributeStoreName = request.DataAttributeStoreName;
            EffectiveDate = new TimeBucket(request.EffectiveDate);
            ExpiryDate = new TimeBucket(request.ExpiryDate);
            LookupBy = request.LookupBy;
            LookupValues = request.LookupValues;
            LookupStorageName = request.LookupStorageName;
            LastUpdatedBy = request.LastUpdatedBy;
            RuleDefinitionId = request.RuleDefinitionId;
            Data = request.LookupData;
            ApplicationType = request.ApplicationType;
            CustomDate = new TimeBucket(request.CustomDate);
        }

        public Lookup(IAddLookupRequest request)
        {
            Description = request.Description;
            DataAttributeStoreName = request.DataAttributeStoreName;
            EffectiveDate = new TimeBucket(request.EffectiveDate);
            ExpiryDate = new TimeBucket(request.ExpiryDate);
            LookupBy = request.LookupBy;
            LookupValues = request.LookupValues;
            LookupStorageName = request.LookupStorageName;
            LookupName = request.LookupName;
            RuleDefinitionId = request.RuleDefinitionId;
            Data = request.LookupData;
            ApplicationType = request.ApplicationType;
            CustomDate = new TimeBucket(request.CustomDate);
        }
        public string EntityType { get; set; }
        public string ProductId { get; set; }
        public string LookupName { get; set; }
        public string Description { get; set; }
        public string DataAttributeStoreName { get; set; }
        public TimeBucket EffectiveDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public List<string> LookupBy { get; set; }
        public List<string> LookupValues { get; set; }
        public string LookupStorageName { get; set; }
        public TimeBucket CreatedDate { get; set; }
        public TimeBucket LastUpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }

        public string RuleDefinitionId { get; set; }

        public object Data { get; set; }

        public TimeBucket CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }
    }
}
