﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public interface IScoreCardVariable
    {

        string ScoreCardVariableId { get; set; }
        string VariableName { get; set; }
        string Description { get; set; }
        VariableType VariableType { get; set; }//Data Attribute,DE Rule
        string DefinitionId { get; set; }
        string DataAttributeStoreName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IVariableSource, VariableSource>))]
        IVariableSource ScoreCardVariableSource { get; set; }
        double Weightage { get; set; }

        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }

        TimeBucket CreatedOn { get; set; }

        TimeBucket UpdatedOn { get; set; }

    }
}
