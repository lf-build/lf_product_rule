﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.ProductRule.Configurations;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace LendFoundry.ProductRule
{
    public class ProductRuleResult : Aggregate, IProductRuleResult
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string Name { get; set; }
        public TimeBucket ExecutedDate { get; set; }
        public Result Result { get; set; }
        public List<string> ResultDetail { get; set; }
        public object IntermediateData { get; set; }
        public Dictionary<string, object> SourceData { get; set; }
        public List<string> ExceptionDetail { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IRuleDefinition, RuleDefinition>))]
        public IRuleDefinition ConfigurationDetail { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRuleDefinition, RuleDefinition>))]
        public IRuleDefinition RuleDefinitionDetail { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISegment, Segment>))]
        public ISegment SegmentDetail { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILookup, Lookup>))]
        public ILookup lookupDetail { get; set; }
    }
}
