﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public class AddLookupRequest : IAddLookupRequest
    {
        public string LookupName { get; set; }
        public string DataAttributeStoreName { get; set; }
        public DateTimeOffset EffectiveDate { get; set; }
        public DateTimeOffset ExpiryDate { get; set; }
        public List<string> LookupBy { get; set; }
        public List<string> LookupValues { get; set; }
        public string LookupStorageName { get; set; }
        public string Description { get; set; }

        public string RuleDefinitionId { get; set; }

        public object LookupData { get; set; }

        public DateTimeOffset CustomDate { get; set; }
        public ApplicationType ApplicationType { get; set; }

    }
}
