﻿namespace LendFoundry.ProductRule
{
    public enum VariableType
    {
        Rule = 1,
        DataAttribute = 2,
        SegmentRule = 3,
        LookupRule = 4
    }
}
