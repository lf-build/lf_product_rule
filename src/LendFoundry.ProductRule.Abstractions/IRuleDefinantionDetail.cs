﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public interface IRuleDefinition : IAggregate
    {
        string EntityType { get; set; }
        string ProductId { get; set; }
        string RuleType { get; set; }
        string RuleClass { get; set; }
        string RuleName { get; set; }
        string Description { get; set; }
        string DataAttributeStoreName { get; set; }
        TimeBucket EffectiveDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRuleDetail, RuleDetail>))]
        IRuleDetail RuleDetail { get; set; }
        TimeBucket CreatedDate { get; set; }
        TimeBucket LastUpdatedDate { get; set; }
        string CreatedBy { get; set; }
        string LastUpdatedBy { get; set; }
        TimeBucket CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }
    }
}