﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.ProductRule.Configurations;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleService
    {
        Task<ILookup> AddLookUp(string entityType, string productId, IAddLookupRequest lookupRequest);
        Task<ILookup> UpdateLookUp(string lookupId, IUpdateLookupRequest updateLookupRequest);
        Task<ILookup> GetLookUp(string lookupId);
        Task DeleteLookUp(string lookupId);
        Task<IRuleDefinition> AddRuleDefinition(string entityType, string productId, IRuleDefinitionAddRequest addRuleRequest);
        Task<IRuleDefinition> UpdateRuleDefinition(string ruleId, IRuleDefinitionUpdateRequest updateRuleRequest);
        Task DeleteRule(string ruleId);
        Task<IRuleDefinition> GetRuleDefinitionByName(string entityType, string productId, string ruleName);
        Task<List<IRuleDefinition>> GetAllRuleDefinition(string entityType, string productId, string ruleType);

        Task<IScoreCard> AddScoreCard(string entityType, string productId, IScoreCardRequest scoreCardRequest);
        Task UpdateScoreCard(string scoreCardId, IScoreCardRequest scoreCardRequest);

        Task<IScoreCard> GetScoreCard(string scoreCardId);
        Task<IRuleDefinition> GetRuleDefinitionById(string ruleId);
        Task DeleteScoreCard(string scoreCardId);
        Task<IScoreCard> AddScoreCardVariable(string scoreCardId, IScoreCardVariableRequest scoreCardVariableRequest);

        Task UpdateScoreCardVariable(string scoreCardId, string scoreCardVariableId, IScoreCardVariableRequest scoreCardVariableRequest);

        Task<IScoreCardVariable> GetCardVariable(string scoreCardId, string scoreCardVariableId);

        Task DeleteScoreCardVariable(string scoreCardId, string scoreCardVariableId);

        Task<ISegment> AddSegment(string entityType, string productId, ISegmentRequest segmentRequest);

        Task UpdateSegment(string segmentId, ISegmentRequest segmentRequest);
        Task<ISegment> GetSegment(string segmentId);
        Task DeleteSegment(string segmentId);
        Task<ILookup> GetLookupByName(string entityType, string productId, string name);
        Task<ISegment> GetSegmentByName(string entityType, string productId, string name);
        Task<IScoreCard> GetScoreCardByName(string entityType, string productId, string name);
        Task<ProductRuleResult> RunRule(string entityType, string entityId, string productId, string ruleDefinationName, string secondaryName = null);
        Task<ProductRuleResult> RunRule(string entityType, string entityId, string productId, string ruleName, object eventData, string secondaryName = null);
        Task<IProductRuleResult> RunSegment(string entityType, string entityId, string productId, string segmentId,string secondaryName = null);
        Task<IProductRuleResult> RunScoreCard(string entityType, string entityId, string productId, string scoreCardId, Dictionary<string, string> referenceNumbers = null);
        Task<IProductRuleResult> RunLookup(string entityType, string entityId, string productId, string lookupId);

        Task<List<ILookup>> GetAllLookup(string entityType);
        Task<List<ISegment>> GetAllSegment(string entityType);
        Task<List<IScoreCard>> GetAllScoreCard(string entityType);

        Task<List<IRuleDefinition>> GetRuleDefinitionDetails(string entityType, string className, string productId);

        Task<List<IProductRuleResult>> GetProductRuleResultDetail(string entityType, string entityId);

        Task<ILookup> UploadLookupData(string lookupId, byte[] file);

        Task<IPricingStrategy> AddPricingStrategy(string entityType, string productId, IPricingStrategyRequest pricingStrategyRequest);
        Task UpdatePricingStrategy(string pricingStrategyId, IPricingStrategyRequest pricingStrategyRequest);
        Task<IPricingStrategy> GetPricingStrategy(string pricingStrategyId);     
        Task DeletePricingStrategy(string pricingStrategyId);

        Task<IPricingStrategy> AddPricingStrategyStep(string pricingStrategyId, IPricingStrategyStepRequest pricingStrategyStepRequest);
        Task<IPricingStrategyStep> GetPricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId);

        Task DeletePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId);

        Task UpdatePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId, IPricingStrategyStepRequest pricingStrategyStepRequest);
        Task<IProductRuleResult> RunPricingStrategy(string entityType, string entityId, string productId, string pricingStrategyName, string version,string secondaryName = null);

        Task<List<IStrategyHistory>> GetStrategyHistory(string entityType, string entityId, string productId, string strategyName);

        Task<IProductRuleSourceResult> ValidateScoreCard(string entityType, string productId, string scoreCardId);

        Task<List<IPricingStrategy>> GetAllPricingStrategy(string entityType);
        Task<List<ITemplate>> GetAllTemplate(string entityType);
        Task<ITemplate> GetByEventName(string entityType, string productId, string eventName);
        Task<ITemplate> AddTemplate(string entityType, string productId, ITemplateAddRequest templateAddRequest);
        Task UpdateTemplate(string templateId, ITemplateAddRequest templateAddRequest);
    }
}
