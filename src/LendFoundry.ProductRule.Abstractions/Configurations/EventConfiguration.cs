﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule.Configurations 
{
    public class EventConfiguration : IEventConfiguration 
    {
        public string EntityId { get; set; }

        public string RuleName { get; set; }

        public string RuleType { get; set; }

        public string GroupName { get; set; }

        public string EntityType { get; set; }
    }
}