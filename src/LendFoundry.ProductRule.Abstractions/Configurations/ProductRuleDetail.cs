﻿namespace LendFoundry.ProductRule.Configurations
{
    public class ProductRuleDetail 
    {
        public CaseInsensitiveDictionary<RuleDetail> RuleDetails { get; set; }
    
    }
}
