﻿using LendFoundry.ProductRule;
using System.Collections.Generic;

namespace LendFoundry.ProductRule.Configurations
{
    public class RuleConfiguration
    {
        public CaseInsensitiveDictionary<List<CaseInsensitiveDictionary<List<RuleDefinition>>>> RuleDefinations { get; set; }

        public CaseInsensitiveDictionary<RuleDetail> RuleDetails { get; set; }
    }
}
 