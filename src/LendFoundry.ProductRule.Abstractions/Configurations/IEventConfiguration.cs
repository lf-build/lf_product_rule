﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule.Configurations
{
    public interface IEventConfiguration
    {
        string EntityId { get; set; }

        string RuleName { get; set; }

        string RuleType { get; set; }

        string GroupName { get; set; }

        string EntityType { get; set; }
    }
}
