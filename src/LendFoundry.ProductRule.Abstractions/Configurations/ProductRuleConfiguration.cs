﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.ProductRule.Configurations
{
    public class ProductRuleConfiguration : IDependencyConfiguration
    {
        public CaseInsensitiveDictionary<CaseInsensitiveDictionary<EventConfiguration>> DataAttributeRules { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
