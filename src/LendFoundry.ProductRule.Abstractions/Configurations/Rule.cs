﻿using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class RuleDetail : IRuleDetail
    {
        public string DERuleName { get; set; }
        public string RuleVersion { get; set; }
        public List<string> RequiredSources { get; set; }
        public List<string> OptionalSources { get; set; }

      

    }
}
