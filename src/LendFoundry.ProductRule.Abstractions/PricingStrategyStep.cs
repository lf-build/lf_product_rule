﻿using LendFoundry.Foundation.Date;
using System.Collections.Generic;

namespace LendFoundry.ProductRule
{
    public class PricingStrategyStep : IPricingStrategyStep
    {

        public string StrategyStepId { get; set; }
        public string StepName { get; set; }

        public string ParameterName { get; set; }
        public List<string> ParameterTypes { get; set; }
        public string Description { get; set; }


        public StepType StepType { get; set; }//Score Card,Lookup,Segment

        public int SequenceNumber { get; set; }

        public bool ContinueOnException { get; set; }
        public bool ContinueOnNonEligibility { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public TimeBucket CreatedOn { get; set; }

        public TimeBucket UpdatedOn { get; set; }



    }
}
