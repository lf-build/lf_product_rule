﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public class RuleDefinition : Aggregate, IRuleDefinition
    {
        public RuleDefinition() { }

        public RuleDefinition(IRuleDefinitionAddRequest request) {
            RuleName = request.RuleName;
            Description = request.Description;
            DataAttributeStoreName = request.DataAttributeStoreName;
            EffectiveDate = new TimeBucket(request.EffectiveDate);
            ExpiryDate = new TimeBucket(request.ExpiryDate);
            RuleClass = request.RuleClass;
            RuleType = request.RuleType;
            EffectiveDate = new TimeBucket(request.EffectiveDate);
            CreatedBy = request.CreatedBy;
            RuleDetail = new RuleDetail
            {
                DERuleName = request.DERuleName,
                OptionalSources = request.OptionalSources,
                RequiredSources = request.RequiredSources,             
                RuleVersion = request.RuleVersion,
            };
        }

        public string EntityType { get; set; }
        public string ProductId { get; set; }
        public string RuleName { get; set; }
        public string Description { get; set; }
        public string DataAttributeStoreName { get; set; }
        public TimeBucket EffectiveDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }

        public TimeBucket CustomDate { get; set; }
        public string RuleClass { get; set; }
        public string RuleType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRuleDetail,RuleDetail>))]
        public IRuleDetail RuleDetail { get; set; }
        public TimeBucket CreatedDate { get; set; }
        public TimeBucket LastUpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }

        public ApplicationType ApplicationType { get; set; }
    }

    
}
