﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IProductRuleScoreCardRepository : IRepository<IScoreCard>
    {
        Task<IScoreCard> GetByName(string entityType, string productId, string Name);
        Task<List<IScoreCard>> GetAllScoreCard(string entityType);
    }
}
