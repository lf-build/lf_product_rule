﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule
{
    public interface IAddLookupRequest
    {
        string LookupName { get; set; }
        string DataAttributeStoreName { get; set; }
        DateTimeOffset EffectiveDate { get; set; }
        DateTimeOffset ExpiryDate { get; set; }
        List<string> LookupBy { get; set; }
        List<string> LookupValues { get; set; }
        string LookupStorageName { get; set; }
        string Description { get; set; }
        string RuleDefinitionId { get; set; }

        object LookupData { get; set; }
         DateTimeOffset CustomDate { get; set; }
         ApplicationType ApplicationType { get; set; }
    }
}
