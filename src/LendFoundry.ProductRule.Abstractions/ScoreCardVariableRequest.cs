﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public class ScoreCardVariableRequest : IScoreCardVariableRequest
    {
        public string VariableName { get; set; }
        public VariableType VariableType { get; set; }//data attribute/DE rule
        public string DefinitionId { get; set; }
        public string DataAttributeStoreName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IVariableSource, VariableSource>))]
        public IVariableSource ScoreCardVariableSource { get; set; }
        public double Weightage { get; set; }
        public string Description { get; set; }
    }
}
