﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule
{
    public interface ISegment : IAggregate
    {
        string EntityType { get; set; }
        string ProductId { get; set; }
        string SegmentName { get; set; }
        string Description { get; set; }
        string DataAttributeStoreName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IVariableSource, VariableSource>))]
        IVariableSource SegmentVariableSource { get; set; }
        SegmentType SegmentType { get; set; }//Range/List/Rule
        string SegmentStorageName { get; set; }// Reference of mongo document name 
        string RuleDefinitionId { get; set; }// For type rule 
        TimeBucket EffectiveDate { get; set; }
        TimeBucket ExpiryDate { get; set; }
        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }
        TimeBucket CreatedOn { get; set; }

        TimeBucket UpdatedOn { get; set; }

        object Data { get; set; }

        TimeBucket CustomDate { get; set; }
        ApplicationType ApplicationType { get; set; }
    }

}
