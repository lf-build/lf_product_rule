﻿using System;

namespace LendFoundry.ProductRule
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}
