﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductRule.Configurations;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Listener;

namespace LendFoundry.ProductRule
{
    public class ProductRuleListner : ListenerBase, IProductRuleListner
    {
      public ProductRuleListner
      (
            ITokenHandler tokenHandler,
            ILoggerFactory loggerFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationFactory,
            IProductRuleServiceFactory productRuleFactory,
            IDataAttributesClientFactory dataAttributesFactory)
        : base(tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)
        {
            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(productRuleFactory), productRuleFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);

            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            TenantServiceFactory = tenantServiceFactory;
            ProductRuleFactory = productRuleFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
            DataAttributesFactory = dataAttributesFactory;
        }

        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IDecisionEngineClientFactory DecisionEngineFactory { get; }
        private IProductRuleServiceFactory ProductRuleFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IDataAttributesClientFactory DataAttributesFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configuration = ConfigurationFactory.Create<ProductRuleConfiguration>(Settings.ServiceName, reader).Get();
            if (configuration == null || configuration.DataAttributeRules == null)
                return null;
            else
            {
                return configuration.DataAttributeRules.Values
                                                       .SelectMany(entity => entity.Keys)
                                                       .Distinct()
                                                       .ToList<string>();
            }
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var serviceName = Settings.ServiceName;
            var token = TokenHandler.Issue(tenant, serviceName);
            var reader = new StaticTokenReader(token.Value);
            var eventHub = EventHubFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var configuration = ConfigurationFactory.Create<ProductRuleConfiguration>(Settings.ServiceName, reader).Get();
            var productRule = ProductRuleFactory.Create(reader, logger, tenantTime, configuration);
            var dataAttribute = DataAttributesFactory.Create(reader);

            if (configuration == null)
            {
                logger.Info($"The configuration for service #{serviceName} could not be found for {tenant}, please verify");
                return null;
            }
            else
            {                
               var uniqueEvents = configuration.DataAttributeRules.Values
                                                       .SelectMany(entity => entity.Keys)
                                                       .Distinct()
                                                       .ToList<string>();

               uniqueEvents.ForEach(eventName =>
                            {
                                eventHub.On(eventName, ParseAttributes(configuration, productRule, logger, dataAttribute));
                                logger.Info($"It was made subscription to EventHub with the Event: #{eventName} for tenant {tenant}");
                            });

                return uniqueEvents;
            }
        }
      
        private static Action<EventInfo> ParseAttributes(ProductRuleConfiguration configuration, IProductRuleService productRule, ILogger logger, IDataAttributesEngine dataAttribute)
        {
            return @event =>
            {
                var matches = FindMatchingConfigurations(configuration, @event);
                Parallel.ForEach(matches, ParseAttribute(productRule, logger, @event, dataAttribute));
            };
        }

        private static Action<dynamic> ParseAttribute(IProductRuleService productRule, ILogger logger, EventInfo @event, IDataAttributesEngine dataAttribute)
        {
            return async match =>
            {
                try
                {
                    logger.Info($"Processing {@event.Name} with Id - {@event.Id} for {@event.TenantId}");

                    var eventConfigurationEntityType = match.eventConfigurationEntityType;
                    if (!string.IsNullOrWhiteSpace(eventConfigurationEntityType))
                    {
                        if(string.Equals(eventConfigurationEntityType, match.entityType, StringComparison.InvariantCultureIgnoreCase))
                        {
                            await SetAttribute(productRule, logger, @event, dataAttribute, match);
                        }                        
                    }
                    else
                    {
                        await SetAttribute(productRule, logger, @event, dataAttribute, match);
                    }                    
                }
                catch (Exception ex)
                {
                    logger.Error($"Error while processing event #{@event.Name}. Error: {ex.Message}", ex, @event);
                    logger.Info("Service is working yet and waiting new event\n");
                }
            };
        }

        private static async Task SetAttribute(IProductRuleService productRule, ILogger logger, EventInfo @event, IDataAttributesEngine dataAttribute, dynamic match)
        {
            ProductRuleResult dataAttributeResult = null;
            string productId = await GetProductId(match.entityType, match.entityId, dataAttribute);
            dataAttributeResult = await productRule.RunRule(match.entityType, match.entityId, productId, match.ruleName, @event.Data);

            if (dataAttributeResult != null)
            {
                var intermediateResult = (Dictionary<string, object>)(dataAttributeResult.IntermediateData);
                if (intermediateResult != null && intermediateResult.Any())
                {
                    foreach (var attribute in intermediateResult)
                        await dataAttribute.SetAttribute(match.entityType, match.entityId, match.groupName, attribute.Value, attribute.Key);
                }
                else
                {
                    logger.Warn($"No Attributes returned from rule {match.ruleName}");
                }
            }
            else
            {
                logger.Warn($"No Attributes returned from rule {match.ruleName}");
            }
        }

        public static async Task<string> GetProductId(string entityType, string entityId, IDataAttributesEngine dataAttribute)
        {
            string productId = null;
            var applicationAttributes = await dataAttribute.GetAttribute(entityType, entityId, "product");
            if (applicationAttributes != null)
                productId = GetResultValueForProductId(applicationAttributes);
            // Temporary - once product comes from the data-attribute , it will generate 
            if (productId == string.Empty || productId == null)
                productId = "product1";
            return productId;
        }

        private static string GetResultValueForProductId(dynamic data)
        {
            Func<dynamic> resultProperty = () => data[0].ProductId;
            Func<dynamic> resultProperty1 = () => data[0].productId;
            if (HasProperty(resultProperty) || HasProperty(resultProperty1))
            {
                var productId = GetValue(resultProperty);
                if (string.IsNullOrWhiteSpace(productId?.ToString()))
                {
                    return GetValue(resultProperty1) ?? string.Empty;
                }
                else
                {
                    return productId;
                }
            }
            return string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

        private static IEnumerable<dynamic> FindMatchingConfigurations(ProductRuleConfiguration configuration, EventInfo @event)
        {
            return from entityConfiguration in configuration.DataAttributeRules
                   from eventConfiguration in entityConfiguration.Value
                   where string.Equals(eventConfiguration.Key, @event.Name, StringComparison.InvariantCultureIgnoreCase)
                   select new
                   {
                       entityType = entityConfiguration.Key,
                       entityId = eventConfiguration.Value.EntityId.FormatWith(@event),
                       ruleName = eventConfiguration.Value.RuleName,
                       ruleType = eventConfiguration.Value.RuleType,
                       groupName = eventConfiguration.Value.GroupName,
                       eventConfigurationEntityType = !string.IsNullOrWhiteSpace(eventConfiguration.Value.EntityType) ? eventConfiguration.Value.EntityType.FormatWith(@event) : string.Empty
                   };
        }

        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
    }
}
