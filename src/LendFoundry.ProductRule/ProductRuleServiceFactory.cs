﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.ProductRule.Configurations;
using LendFoundry.Security.Tokens;

using System;
#if DOTNET2

using Microsoft.Extensions.DependencyInjection;
#else

using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.ProductRule
{
    public class ProductRuleServiceFactory : IProductRuleServiceFactory
    {
        public ProductRuleServiceFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IProductRuleService Create(StaticTokenReader reader, ILogger logger, ITenantTime tenantTime, ProductRuleConfiguration configuration)
        {
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var repositoryFactory = Provider.GetService<IProductRuleExecutionRepositoryFactory>().Create(reader);

            var lookupRepositoryFactory = Provider.GetService<IProductRuleLookupRepositoryFactory>().Create(reader);

            return new ProductRuleService
            (
                 dataAttributesEngine: Provider.GetService<IDataAttributesClientFactory>().Create(reader),
                 decisionEngineService: Provider.GetService<IDecisionEngineClientFactory>().Create(reader),
                 productRuleExecutionRepository: repositoryFactory,
                 productRuleLookupRepository: lookupRepositoryFactory,
                 ruleDefinitionRepository: Provider.GetService<IRuleDefinitionRepositoryFactory>().Create(reader),
                 eventHub: Provider.GetService<IEventHubClientFactory>().Create(reader),
                 configuration: configuration,
                 tenantTime: Provider.GetService<ITenantTimeFactory>().Create(configurationServiceFactory, reader),
                 lookupService: Provider.GetService<ILookupClientFactory>().Create(reader),
                 productRuleScoreCardRepository: Provider.GetService<IProductRuleScoreCardRepositoryFactory>().Create(reader),
                 productRuleSegmentRepository: Provider.GetService<IProductRuleSegmentRepositoryFactory>().Create(reader),
                 pricingStrategyRepository: Provider.GetService<IPricingStrategyRepositoryFactory>().Create(reader),
                 strategyExceutionHistoryRepository: Provider.GetService<IStrategyExceutionHistoryRepositoryFactory>().Create(reader),
                  templateRepository: Provider.GetService<ITemplateRepositoryFactory>().Create(reader),
                  logger: Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance)
                 
                 );
        }
    }
}
