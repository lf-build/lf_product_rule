﻿using System;

namespace LendFoundry.ProductRule
{
    public class AlwaysRetryPolicy : IRecoverabilityPolicy
    {
        public bool CanBeRecoveredFrom(Exception ex)
        {
            return true;
        }
    }

}
