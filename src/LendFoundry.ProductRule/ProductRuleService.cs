﻿
using System;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.DataAttributes;
using LendFoundry.ProductRule.Configurations;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Lookup;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.ProductRule.Events;
using Newtonsoft.Json.Linq;
using System.IO;
using Microsoft.CSharp.RuntimeBinder;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.ProductRule
{
    public class ProductRuleService : IProductRuleService
    {
        public ProductRuleService(
            IDataAttributesEngine dataAttributesEngine,
            IDecisionEngineService decisionEngineService,
            IProductRuleExecutionRepository productRuleExecutionRepository,
            IProductRuleLookupRepository productRuleLookupRepository,
            IRuleDefinitionRepository ruleDefinitionRepository,
            IEventHubClient eventHub,
            ProductRuleConfiguration configuration,
            ITenantTime tenantTime,
            ILookupService lookupService,
            IProductRuleScoreCardRepository productRuleScoreCardRepository,
            IProductRuleSegmentRepository productRuleSegmentRepository,
             IPricingStrategyRepository pricingStrategyRepository,
               IStrategyExceutionHistoryRepository strategyExceutionHistoryRepository,
               ITemplateRepository templateRepository,
               ILogger logger)
        {
            EventHub = eventHub;
            Configuration = configuration;
            TenantTime = tenantTime;
            DataAttributesEngine = dataAttributesEngine;
            DecisionEngineService = decisionEngineService;
            ProductRuleExecutionRepository = productRuleExecutionRepository;
            ProductRuleLookupRepository = productRuleLookupRepository;
            LookupService = lookupService;
            RuleDefinitionRepository = ruleDefinitionRepository;
            ProductRuleScoreCardRepository = productRuleScoreCardRepository;
            ProductRuleSegmentRepository = productRuleSegmentRepository;
            PricingStrategyRepository = pricingStrategyRepository;
            StrategyExceutionHistoryRepository = strategyExceutionHistoryRepository;
            TemplateRepository = templateRepository;
            Logger = logger;

        }

        private IStrategyExceutionHistoryRepository StrategyExceutionHistoryRepository { get; set; }
        private IPricingStrategyRepository PricingStrategyRepository { get; set; }
        private IProductRuleSegmentRepository ProductRuleSegmentRepository { get; set; }
        private IProductRuleScoreCardRepository ProductRuleScoreCardRepository { get; set; }
        private IEventHubClient EventHub { get; }
        private ILogger Logger { get; }
        private ProductRuleConfiguration Configuration { get; }
        private ITenantTime TenantTime { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IProductRuleExecutionRepository ProductRuleExecutionRepository { get; }
        private IProductRuleLookupRepository ProductRuleLookupRepository { get; }
        private ILookupService LookupService { get; }
        private ITemplateRepository TemplateRepository { get; }
        private IRuleDefinitionRepository RuleDefinitionRepository { get; }


        public async Task<ILookup> GetLookUp(string lookupId)
        {
            if (string.IsNullOrEmpty(lookupId))
                throw new ArgumentException($"{nameof(lookupId)} is mandatory");

            var lookupData = await ProductRuleLookupRepository.GetById(lookupId);
            if (lookupData == null)
                throw new NotFoundException($"Lookup with id {lookupId} is not found");
            return lookupData;
        }
        public async Task<ILookup> AddLookUp(string entityType, string productId, IAddLookupRequest lookupRequest)
        {
            if (lookupRequest == null)
                throw new ArgumentException($"{nameof(lookupRequest)} is mandatory");

            EnsureInputIsValid(entityType, productId);

            var existsLookup = await ProductRuleLookupRepository.GetByName(entityType, productId, lookupRequest.LookupName);
            if (existsLookup != null && !string.IsNullOrWhiteSpace(existsLookup.LookupName))
            {
                throw new InvalidOperationException($"Lookup with name {lookupRequest.LookupName} is already exists for {entityType}-{productId} ");
            }
            ILookup lookup = new Lookup(lookupRequest);
            lookup.EntityType = entityType;
            lookup.ProductId = productId;
            lookup.CreatedDate = new TimeBucket(TenantTime.Now);
            ProductRuleLookupRepository.Add(lookup);
            return lookup;
        }
        public async Task<ILookup> UpdateLookUp(string lookupId, IUpdateLookupRequest updateLookupRequest)
        {
            if (string.IsNullOrEmpty(lookupId))
                throw new ArgumentException($"{nameof(lookupId)} is mandatory");

            if (updateLookupRequest == null)
                throw new ArgumentException($"{nameof(updateLookupRequest)} is mandatory");

            ILookup lookup = new Lookup(updateLookupRequest);
            lookup.LastUpdatedDate = new TimeBucket(TenantTime.Now);
            var lookupData = await ProductRuleLookupRepository.Update(lookupId, lookup);
            return lookupData;
        }
        public async Task DeleteLookUp(string lookupId)
        {
            if (string.IsNullOrEmpty(lookupId))
                throw new ArgumentException($"{nameof(lookupId)} is mandatory");

            var lookup = await ProductRuleLookupRepository.GetById(lookupId);
            if (lookup == null)
                throw new NotFoundException($"Lookup with id {lookupId} not found");

            ProductRuleLookupRepository.Remove(lookup);
        }

        public async Task<ILookup> GetLookupByName(string entityType, string productId, string name)
        {
            return await ProductRuleLookupRepository.GetByName(entityType, productId, name);
        }

        public async Task<List<ILookup>> GetAllLookup(string entityType)
        {
            return await ProductRuleLookupRepository.GetAllLookup(entityType);
        }

        public async Task<ILookup> UploadLookupData(string lookupId, byte[] file)
        {
            if (string.IsNullOrWhiteSpace(lookupId))
                throw new ArgumentException("lookupId is required");

            ILookup lookup = null;
            List<string> csvLines = new List<string>();
            var stream = new StreamReader(new MemoryStream(file));
            while (!stream.EndOfStream)
            {
                csvLines.Add(stream.ReadLine());
            }
            var csv = csvLines.Select(l => l.Split(',')).ToList();

            var headers = csv[0];
            var dicts = csv.Skip(1).Select(row => Enumerable.Zip(headers, row, Tuple.Create).ToDictionary(p => p.Item1, p => p.Item2)).ToArray();

            try
            {
                object lookupData = dicts;
                lookup = await ProductRuleLookupRepository.GetById(lookupId);
                if (lookup == null)
                    throw new NotFoundException($"No lookup data found for {lookupId}");

                lookup.Data = lookupData;
                ProductRuleLookupRepository.Update(lookup);
            }
            catch (Exception)
            {
            }
            return lookup;
        }

        public async Task<IScoreCard> AddScoreCard(string entityType, string productId, IScoreCardRequest scoreCardRequest)
        {
            if (scoreCardRequest == null)
                throw new ArgumentNullException(nameof(scoreCardRequest));

            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(scoreCardRequest.ScoreCardName))
                throw new ArgumentNullException(nameof(scoreCardRequest.ScoreCardName));

            var scoreCardNameAlreadyExists = await ProductRuleScoreCardRepository.GetByName(entityType, productId, scoreCardRequest.ScoreCardName);

            if (scoreCardNameAlreadyExists != null)
                throw new InvalidOperationException($"ScoreCard Name is already exists {scoreCardRequest.ScoreCardName}");


            IScoreCard scoreCardModel = new ScoreCard();
            scoreCardModel.EntityType = entityType;
            scoreCardModel.ProductId = productId;
            scoreCardModel.RuleDefinitionId = scoreCardRequest.RuleDefinitionId;
            scoreCardModel.ScoreCardName = scoreCardRequest.ScoreCardName;
            scoreCardModel.Description = scoreCardRequest.Description;
            scoreCardModel.ApplicationType = scoreCardRequest.ApplicationType;
            scoreCardModel.CustomDate = new TimeBucket(scoreCardRequest.CustomDate);
            scoreCardModel.EffectiveDate = new TimeBucket(scoreCardRequest.EffectiveDate);
            scoreCardModel.ExpiryDate = new TimeBucket(scoreCardRequest.ExpiryDate);
            scoreCardModel.CreatedOn = new TimeBucket(TenantTime.Now);
            scoreCardModel.IsValidated = scoreCardRequest.IsValidated;
            scoreCardModel.ValidationRuleId = scoreCardRequest.ValidationRuleId;
            await Task.Run(() => ProductRuleScoreCardRepository.Add(scoreCardModel));

            //await EventHubClient.Publish(new ApplicantCreated { Applicant = applicantModel });

            return scoreCardModel;
        }

        public async Task UpdateScoreCard(string scoreCardId, IScoreCardRequest scoreCardRequest)
        {
            if (scoreCardRequest == null)
                throw new ArgumentNullException(nameof(scoreCardRequest));

            if (string.IsNullOrEmpty(scoreCardId))
                throw new ArgumentNullException(nameof(scoreCardId));

            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);

            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");


            scoreCardDetails.RuleDefinitionId = scoreCardRequest.RuleDefinitionId;
            scoreCardDetails.ScoreCardName = scoreCardRequest.ScoreCardName;
            scoreCardDetails.Description = scoreCardRequest.Description;
            scoreCardDetails.EffectiveDate = new TimeBucket(scoreCardRequest.EffectiveDate);
            scoreCardDetails.ExpiryDate = new TimeBucket(scoreCardRequest.ExpiryDate);
            scoreCardDetails.ApplicationType = scoreCardRequest.ApplicationType;
            scoreCardDetails.CustomDate = new TimeBucket(scoreCardRequest.CustomDate);
            scoreCardDetails.UpdatedOn = new TimeBucket(TenantTime.Now);
            scoreCardDetails.IsValidated = scoreCardRequest.IsValidated;
            scoreCardDetails.ValidationRuleId = scoreCardRequest.ValidationRuleId;
            await Task.Run(() => ProductRuleScoreCardRepository.Update(scoreCardDetails));

            //await EventHubClient.Publish(new ApplicantCreated { Applicant = applicantModel });


        }

        public async Task<IScoreCard> GetScoreCard(string scoreCardId)
        {
            if (string.IsNullOrEmpty(scoreCardId))
                throw new ArgumentNullException(nameof(scoreCardId));

            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);

            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");

            return scoreCardDetails;
        }

        public async Task<IProductRuleSourceResult> ValidateScoreCard(string entityType, string productId, string scoreCardId)
        {
            EnsureInputIsValid(entityType, productId);
            if (string.IsNullOrEmpty(scoreCardId))
                throw new ArgumentNullException(nameof(scoreCardId));

            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);

            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");
            IProductRuleSourceResult ruleResultDetail = null;
            if (!string.IsNullOrEmpty(scoreCardDetails.ValidationRuleId))
            {
                IRuleDefinition ruleDetail = await GetRuleDefinitionById(scoreCardDetails.ValidationRuleId);

                if (ruleDetail == null)
                    throw new NotFoundException($"RuleDetail is not forunf for {scoreCardDetails.ValidationRuleId}");

                if (scoreCardDetails.ScoreCardVariables != null && scoreCardDetails.ScoreCardVariables.Any())
                {
                    var data = new Dictionary<string, object>();
                    data.Add("scoreCardVariableData", scoreCardDetails.ScoreCardVariables);
                    data.Add("entityType", entityType);
                    ruleResultDetail = ExecuteRule(data, ruleDetail.RuleDetail);
                }
                if (ruleResultDetail.Result == Result.Failed)
                {
                    scoreCardDetails.IsValidated = false;
                    ProductRuleScoreCardRepository.Update(scoreCardDetails);
                    throw new InvalidDataException($"ScoreCard is not Validated {scoreCardDetails.ValidationRuleId}");
                }
                else if (ruleResultDetail.Result == Result.Passed)
                {
                    scoreCardDetails.IsValidated = true;
                    ProductRuleScoreCardRepository.Update(scoreCardDetails);
                    return ruleResultDetail;
                }

            }
            scoreCardDetails.IsValidated = true;
            ProductRuleScoreCardRepository.Update(scoreCardDetails);
            return ruleResultDetail;
        }

        public async Task DeleteScoreCard(string scoreCardId)
        {
            if (string.IsNullOrEmpty(scoreCardId))
                throw new ArgumentNullException(nameof(scoreCardId));

            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);

            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");


            await Task.Run(() => ProductRuleScoreCardRepository.Remove(scoreCardDetails));

        }

        public async Task<List<IStrategyHistory>> GetStrategyHistory(string entityType, string entityId, string productId, string strategyId)
        {
            EnsureInputIsValid(entityType, entityId, productId);

            if (string.IsNullOrWhiteSpace(strategyId))
                throw new ArgumentException("strategyName is required");
            entityType = EnsureEntityType(entityType);

            return await StrategyExceutionHistoryRepository.GetByStrategyId(entityType, entityId, productId, strategyId);

        }

        public async Task<IPricingStrategy> AddPricingStrategy(string entityType, string productId, IPricingStrategyRequest pricingStrategyRequest)
        {
            if (pricingStrategyRequest == null)
                throw new ArgumentNullException(nameof(pricingStrategyRequest));

            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(pricingStrategyRequest.Name))
                throw new ArgumentNullException(nameof(pricingStrategyRequest.Name));

            var pricingStrategyNameAlreadyExists = await PricingStrategyRepository.GetByNameAndVersion(entityType, productId, pricingStrategyRequest.Name, pricingStrategyRequest.Version);

            if (pricingStrategyNameAlreadyExists != null)
                throw new InvalidOperationException($"ScoreCard Name is already exists {pricingStrategyRequest.Name} and {pricingStrategyRequest.Version}");


            IPricingStrategy pricingStrategyModel = new PricingStrategy();
            pricingStrategyModel.EntityType = entityType;
            pricingStrategyModel.ProductId = productId;
            pricingStrategyModel.Name = pricingStrategyRequest.Name;
            pricingStrategyModel.Version = pricingStrategyRequest.Version;
            pricingStrategyModel.Description = pricingStrategyRequest.Description;
            pricingStrategyModel.EffectiveDate = new TimeBucket(pricingStrategyRequest.EffectiveDate);
            pricingStrategyModel.ExpiryDate = new TimeBucket(pricingStrategyRequest.ExpiryDate);
            pricingStrategyModel.CreatedOn = new TimeBucket(TenantTime.Now);
            await Task.Run(() => PricingStrategyRepository.Add(pricingStrategyModel));
            return pricingStrategyModel;
        }


        public async Task UpdatePricingStrategy(string pricingStrategyId, IPricingStrategyRequest pricingStrategyRequest)
        {
            if (pricingStrategyRequest == null)
                throw new ArgumentNullException(nameof(pricingStrategyRequest));

            if (string.IsNullOrEmpty(pricingStrategyId))
                throw new ArgumentNullException(nameof(pricingStrategyId));

            var pricingStrategyDetails = await PricingStrategyRepository.Get(pricingStrategyId);

            if (pricingStrategyDetails == null)
                throw new NotFoundException($"ScoreCard {pricingStrategyId} not found");

            pricingStrategyDetails.Name = pricingStrategyRequest.Name;
            pricingStrategyDetails.Description = pricingStrategyRequest.Description;
            pricingStrategyDetails.Version = pricingStrategyRequest.Version;
            pricingStrategyDetails.EffectiveDate = new TimeBucket(pricingStrategyRequest.EffectiveDate);
            pricingStrategyDetails.ExpiryDate = new TimeBucket(pricingStrategyRequest.ExpiryDate);
            pricingStrategyDetails.Version = pricingStrategyRequest.Version;
            pricingStrategyDetails.UpdatedOn = new TimeBucket(TenantTime.Now);
            await Task.Run(() => PricingStrategyRepository.Update(pricingStrategyDetails));

        }

        public async Task<IPricingStrategy> GetPricingStrategy(string pricingStrategyId)
        {
            if (string.IsNullOrEmpty(pricingStrategyId))
                throw new ArgumentNullException(nameof(pricingStrategyId));

            var pricingStrategyDetails = await PricingStrategyRepository.Get(pricingStrategyId);

            if (pricingStrategyDetails == null)
                throw new NotFoundException($"pricingStrategyDetails {pricingStrategyId} not found");

            return pricingStrategyDetails;
        }

        public async Task DeletePricingStrategy(string pricingStrategyId)
        {
            if (string.IsNullOrEmpty(pricingStrategyId))
                throw new ArgumentNullException(nameof(pricingStrategyId));

            var pricingStrategyDetails = await PricingStrategyRepository.Get(pricingStrategyId);

            if (pricingStrategyDetails == null)
                throw new NotFoundException($"pricingStrategy {pricingStrategyId} not found");


            await Task.Run(() => PricingStrategyRepository.Remove(pricingStrategyDetails));

        }

        public async Task<IPricingStrategy> AddPricingStrategyStep(string pricingStrategyId, IPricingStrategyStepRequest pricingStrategyStepRequest)
        {
            if (pricingStrategyStepRequest == null)
                throw new ArgumentNullException(nameof(pricingStrategyStepRequest));

            if (string.IsNullOrEmpty(pricingStrategyStepRequest.StepName))
                throw new ArgumentNullException(nameof(pricingStrategyStepRequest.StepName));

            if (string.IsNullOrEmpty(pricingStrategyStepRequest.ParameterName))
                throw new ArgumentNullException(nameof(pricingStrategyStepRequest.ParameterName));


            var pricingStrategyDetails = await PricingStrategyRepository.Get(pricingStrategyId);
            if (pricingStrategyDetails == null)
                throw new NotFoundException($"PricingStrategy {pricingStrategyId} not found");

            if (pricingStrategyDetails.PricingStrategyStep != null)
            {
                var parameterNameAlreadyExits = pricingStrategyDetails.PricingStrategyStep.FirstOrDefault(a => a.ParameterName.ToLower() == pricingStrategyStepRequest.ParameterName.ToLower() && a.SequenceNumber == pricingStrategyStepRequest.SequenceNumber);

                if (parameterNameAlreadyExits != null)
                    throw new InvalidOperationException($"Step Name  {pricingStrategyStepRequest.ParameterName} with same SequenceNumber {pricingStrategyStepRequest.SequenceNumber} is already exists ");
            }

            IPricingStrategyStep pricingStrategyStepModel = new PricingStrategyStep();
            pricingStrategyStepModel.StrategyStepId = GenerateUniqueId();
            pricingStrategyStepModel.StepName = pricingStrategyStepRequest.StepName;
            pricingStrategyStepModel.ParameterName = pricingStrategyStepRequest.ParameterName;
            pricingStrategyStepModel.StepType = pricingStrategyStepRequest.StepType;
            pricingStrategyStepModel.SequenceNumber = pricingStrategyStepRequest.SequenceNumber;
            pricingStrategyStepModel.Description = pricingStrategyStepRequest.Description;
            pricingStrategyStepModel.ParameterTypes = pricingStrategyStepRequest.ParameterTypes;
            pricingStrategyStepModel.ContinueOnException = pricingStrategyStepRequest.ContinueOnException;
            pricingStrategyStepModel.ContinueOnNonEligibility = pricingStrategyStepRequest.ContinueOnNonEligibility;
            pricingStrategyStepModel.CreatedOn = new TimeBucket(TenantTime.Now);

            if (pricingStrategyDetails.PricingStrategyStep != null && pricingStrategyDetails.PricingStrategyStep.Count > 0)
                pricingStrategyDetails.PricingStrategyStep.Add(pricingStrategyStepModel);
            else
            {
                List<IPricingStrategyStep> pricingStrategySteps = new List<IPricingStrategyStep>();
                pricingStrategySteps.Add(pricingStrategyStepModel);
                pricingStrategyDetails.PricingStrategyStep = pricingStrategySteps;
            }

            await Task.Run(() => PricingStrategyRepository.Update(pricingStrategyDetails));

            //await EventHubClient.Publish(new ApplicantCreated { Applicant = applicantModel });

            return pricingStrategyDetails;
        }

        public async Task<IPricingStrategyStep> GetPricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId)
        {

            if (string.IsNullOrEmpty(pricingStrategyId))
                throw new ArgumentNullException(nameof(pricingStrategyId));

            if (string.IsNullOrEmpty(pricingStrategyStepId))
                throw new ArgumentNullException(nameof(pricingStrategyStepId));

            var pricingStrategyDetails = await PricingStrategyRepository.Get(pricingStrategyId);

            if (pricingStrategyDetails == null)
                throw new NotFoundException($"Pricing Strategy {pricingStrategyId} not found");

            var pricingStrategyStep = pricingStrategyDetails.PricingStrategyStep.FirstOrDefault(a => a.StrategyStepId == pricingStrategyStepId);
            if (pricingStrategyStep == null)
                throw new NotFoundException($"Pricing Strategy Step is not exists for given {pricingStrategyStepId}");


            return pricingStrategyStep;
        }

        public async Task DeletePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId)
        {

            if (string.IsNullOrEmpty(pricingStrategyId))
                throw new ArgumentNullException(nameof(pricingStrategyId));

            if (string.IsNullOrEmpty(pricingStrategyStepId))
                throw new ArgumentNullException(nameof(pricingStrategyStepId));

            var pricingStrategyDetails = await PricingStrategyRepository.Get(pricingStrategyId);

            if (pricingStrategyDetails == null)
                throw new NotFoundException($"Pricing Strategy {pricingStrategyId} not found");

            var pricingStrategyStep = pricingStrategyDetails.PricingStrategyStep.FirstOrDefault(a => a.StrategyStepId == pricingStrategyStepId);
            if (pricingStrategyStep == null)
                throw new NotFoundException($"ScoreCard Variable is not exists for given {pricingStrategyStepId}");

            await Task.Run(() => pricingStrategyDetails.PricingStrategyStep.Remove(pricingStrategyStep));
        }

        public async Task UpdatePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId, IPricingStrategyStepRequest pricingStrategyStepRequest)
        {
            if (pricingStrategyStepRequest == null)
                throw new ArgumentNullException(nameof(pricingStrategyStepRequest));

            if (string.IsNullOrEmpty(pricingStrategyId))
                throw new ArgumentNullException(nameof(pricingStrategyId));

            if (string.IsNullOrEmpty(pricingStrategyStepId))
                throw new ArgumentNullException(nameof(pricingStrategyStepId));

            var pricingStrategyDetails = await PricingStrategyRepository.Get(pricingStrategyId);

            if (pricingStrategyDetails == null)
                throw new NotFoundException($"Pricing Strategy {pricingStrategyId} not found");
            var PricingStrategySteps = pricingStrategyDetails.PricingStrategyStep ?? new List<IPricingStrategyStep>();
            var pricingStrategyStep = PricingStrategySteps.FirstOrDefault(a => a.StrategyStepId == pricingStrategyStepId);
            if (pricingStrategyStep == null)
                throw new NotFoundException($"Pricing Strategy Step is not exists for given {pricingStrategyStepId}");


            pricingStrategyStep.StepName = pricingStrategyStepRequest.StepName;
            pricingStrategyStep.ParameterName = pricingStrategyStepRequest.ParameterName;
            pricingStrategyStep.StepType = pricingStrategyStepRequest.StepType;
            pricingStrategyStep.SequenceNumber = pricingStrategyStepRequest.SequenceNumber;
            pricingStrategyStep.Description = pricingStrategyStepRequest.Description;
            pricingStrategyStep.ParameterTypes = pricingStrategyStepRequest.ParameterTypes;
            pricingStrategyStep.ContinueOnException = pricingStrategyStepRequest.ContinueOnException;
            pricingStrategyStep.ContinueOnNonEligibility = pricingStrategyStepRequest.ContinueOnNonEligibility;
            pricingStrategyStep.UpdatedOn = new TimeBucket(TenantTime.Now);

            PricingStrategySteps = PricingStrategySteps.Select(pricingstrategyStepId => pricingstrategyStepId.StrategyStepId == pricingStrategyStepId ? pricingStrategyStep : pricingstrategyStepId).ToList();

            pricingStrategyDetails.PricingStrategyStep = PricingStrategySteps;


            await Task.Run(() => PricingStrategyRepository.Update(pricingStrategyDetails));

        }

        public async Task<IScoreCard> AddScoreCardVariable(string scoreCardId, IScoreCardVariableRequest scoreCardVariableRequest)
        {
            if (scoreCardVariableRequest == null)
                throw new ArgumentNullException(nameof(scoreCardVariableRequest));

            if (string.IsNullOrEmpty(scoreCardVariableRequest.VariableName))
                throw new ArgumentNullException(nameof(scoreCardVariableRequest.VariableName));


            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);
            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");

            if (scoreCardDetails.ScoreCardVariables != null)
            {
                var VariableNameAlreadyExits = scoreCardDetails.ScoreCardVariables.FirstOrDefault(a => a.VariableName.ToLower() == scoreCardVariableRequest.VariableName.ToLower());

                if (VariableNameAlreadyExits != null)
                    throw new InvalidOperationException($"Variable Name is already exists {scoreCardVariableRequest.VariableName}");
            }

            IScoreCardVariable scoreCardVariableModel = new ScoreCardVariable();
            scoreCardVariableModel.ScoreCardVariableId = GenerateUniqueId();
            scoreCardVariableModel.DefinitionId = scoreCardVariableRequest.DefinitionId;
            scoreCardVariableModel.VariableName = scoreCardVariableRequest.VariableName;
            scoreCardVariableModel.VariableType = scoreCardVariableRequest.VariableType;
            scoreCardVariableModel.Weightage = scoreCardVariableRequest.Weightage;
            scoreCardVariableModel.Description = scoreCardVariableRequest.Description;
            scoreCardVariableModel.DataAttributeStoreName = scoreCardVariableRequest.DataAttributeStoreName;
            scoreCardVariableModel.ScoreCardVariableSource = scoreCardVariableRequest.ScoreCardVariableSource;
            scoreCardVariableModel.CreatedOn = new TimeBucket(TenantTime.Now);

            if (scoreCardDetails.ScoreCardVariables != null && scoreCardDetails.ScoreCardVariables.Count > 0)
                scoreCardDetails.ScoreCardVariables.Add(scoreCardVariableModel);
            else
            {
                List<IScoreCardVariable> scoreCardVariables = new List<IScoreCardVariable>();
                scoreCardVariables.Add(scoreCardVariableModel);
                scoreCardDetails.ScoreCardVariables = scoreCardVariables;
            }

            await Task.Run(() => ProductRuleScoreCardRepository.Update(scoreCardDetails));

            //await EventHubClient.Publish(new ApplicantCreated { Applicant = applicantModel });

            return scoreCardDetails;
        }
        public async Task<IRuleDefinition> GetRuleDefinitionById(string ruleId)
        {

            if (string.IsNullOrEmpty(ruleId))
                throw new ArgumentException($"{nameof(ruleId)} is mandatory");

            var ruleDetail = await RuleDefinitionRepository.GetById(ruleId);
            if (ruleDetail == null)
                throw new NotFoundException($"rule detail not found for {ruleId}");

            return ruleDetail;
        }
        public async Task<ProductRuleResult> RunRule(string entityType, string entityId, string productId, string ruleName, object eventData, string secondaryName = null)
        {
            EnsureInputIsValid(entityType, productId);

            if (string.IsNullOrWhiteSpace(ruleName))
                throw new ArgumentException($"{nameof(ruleName)} is mandatory");

            entityType = EnsureEntityType(entityType);

            IRuleDefinition ruleDetail = await GetRuleDefinitionByName(entityType, productId, ruleName);

            if (ruleDetail == null)
                throw new NotFoundException($"RuleDetail is not found for {ruleName} is not found for {entityType}-{productId}");

            var data = new Dictionary<string, object>();
            var requiredSources = ruleDetail?.RuleDetail?.RequiredSources;

            var sources = new List<string>();
            if (requiredSources != null && requiredSources.Any())
            {
                sources.AddRange(requiredSources);
            }

            var optionalSources = ruleDetail?.RuleDetail?.OptionalSources;
            if (optionalSources != null && optionalSources.Any())
            {
                sources.AddRange(optionalSources);
            }
            FaultRetry.RunWithAlwaysRetry(() =>
            {
                if (sources.Any())
                {
                    if (!string.IsNullOrEmpty(secondaryName))
                    {
                        var requestModel = new AttributeRequest();
                        requestModel.Names = sources;
                        requestModel.SecondaryName = secondaryName;
                        data = DataAttributesEngine.GetAttribute(entityType, entityId, requestModel).Result;
                    }
                    else
                        data = DataAttributesEngine.GetAttribute(entityType, entityId, sources).Result;

                    if (data == null || !data.Any() || data.FirstOrDefault().Value == null)
                        throw new NotFoundException("No data attributes found for sources.");
                }
                if (requiredSources != null && requiredSources.Any())
                {
                    var isAllAttributes = requiredSources.All(r => data.Any(a => string.Equals(r, a.Key, StringComparison.InvariantCultureIgnoreCase)));
                    if (!isAllAttributes)
                        throw new InvalidOperationException("Data attributes for all sources are not found");
                }
            });
            data.Add("eventData", eventData);
            data.Add("entityId", entityId);
            data.Add("entityType", entityType);

            var ruleResultDetail = ExecuteRule(data, ruleDetail.RuleDetail);

            var executionDetail = new ProductRuleResult
            {
                EntityId = entityId,
                EntityType = entityType,
                ExecutedDate = new TimeBucket(TenantTime.Now),
                IntermediateData = ruleResultDetail.Data,
                Name = ruleDetail.RuleName,
                ResultDetail = ruleResultDetail.Detail,
                Result = ruleResultDetail.Result,
                ConfigurationDetail = ruleDetail,
                SourceData = ruleResultDetail.SourceData,
                ExceptionDetail = ruleResultDetail.Exception
            };
            ProductRuleExecutionRepository.Add(executionDetail);
            await EventHub.Publish(new ProductRuleExecuted { ProductRuleResult = executionDetail });
            return executionDetail;
        }
        public async Task UpdateScoreCardVariable(string scoreCardId, string scoreCardVariableId, IScoreCardVariableRequest scoreCardVariableRequest)
        {
            if (scoreCardVariableRequest == null)
                throw new ArgumentNullException(nameof(scoreCardVariableRequest));

            if (string.IsNullOrEmpty(scoreCardId))
                throw new ArgumentNullException(nameof(scoreCardId));

            if (string.IsNullOrEmpty(scoreCardVariableId))
                throw new ArgumentNullException(nameof(scoreCardVariableId));

            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);

            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");
            var ScoreCardVariables = scoreCardDetails.ScoreCardVariables ?? new List<IScoreCardVariable>();
            var scoreCardVariable = ScoreCardVariables.FirstOrDefault(a => a.ScoreCardVariableId == scoreCardVariableId);
            if (scoreCardVariable == null)
                throw new NotFoundException($"ScoreCard Variable is not exists for given {scoreCardVariableId}");

            scoreCardVariable.DefinitionId = scoreCardVariableRequest.DefinitionId;
            scoreCardVariable.VariableName = scoreCardVariableRequest.VariableName;
            scoreCardVariable.VariableType = scoreCardVariableRequest.VariableType;
            scoreCardVariable.Weightage = scoreCardVariableRequest.Weightage;
            scoreCardVariable.Description = scoreCardVariableRequest.Description;
            scoreCardVariable.DataAttributeStoreName = scoreCardVariableRequest.DataAttributeStoreName;
            scoreCardVariable.ScoreCardVariableSource = scoreCardVariableRequest.ScoreCardVariableSource;
            scoreCardVariable.UpdatedOn = new TimeBucket(TenantTime.Now);

            ScoreCardVariables = ScoreCardVariables.Select(scorecardVariableId => scorecardVariableId.ScoreCardVariableId == scoreCardVariableId ? scoreCardVariable : scorecardVariableId).ToList();

            scoreCardDetails.ScoreCardVariables = ScoreCardVariables;


            await Task.Run(() => ProductRuleScoreCardRepository.Update(scoreCardDetails));

            //await EventHubClient.Publish(new ApplicantCreated { Applicant = applicantModel });


        }

        public async Task<IScoreCardVariable> GetCardVariable(string scoreCardId, string scoreCardVariableId)
        {

            if (string.IsNullOrEmpty(scoreCardId))
                throw new ArgumentNullException(nameof(scoreCardId));

            if (string.IsNullOrEmpty(scoreCardVariableId))
                throw new ArgumentNullException(nameof(scoreCardVariableId));

            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);

            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");

            var scoreCardVariable = scoreCardDetails.ScoreCardVariables.FirstOrDefault(a => a.ScoreCardVariableId == scoreCardVariableId);
            if (scoreCardVariable == null)
                throw new NotFoundException($"ScoreCard Variable is not exists for given {scoreCardVariableId}");


            return scoreCardVariable;
        }

        public async Task DeleteScoreCardVariable(string scoreCardId, string scoreCardVariableId)
        {

            if (string.IsNullOrEmpty(scoreCardId))
                throw new ArgumentNullException(nameof(scoreCardId));

            if (string.IsNullOrEmpty(scoreCardVariableId))
                throw new ArgumentNullException(nameof(scoreCardVariableId));

            var scoreCardDetails = await ProductRuleScoreCardRepository.Get(scoreCardId);

            if (scoreCardDetails == null)
                throw new NotFoundException($"ScoreCard {scoreCardId} not found");

            var scoreCardVariable = scoreCardDetails.ScoreCardVariables.FirstOrDefault(a => a.ScoreCardVariableId == scoreCardVariableId);
            if (scoreCardVariable == null)
                throw new NotFoundException($"ScoreCard Variable is not exists for given {scoreCardVariableId}");

            await Task.Run(() => scoreCardDetails.ScoreCardVariables.Remove(scoreCardVariable));
        }
        public async Task<ISegment> GetSegmentByName(string entityType, string productId, string name)
        {
            return await ProductRuleSegmentRepository.GetByName(entityType, productId, name);
        }

        public async Task<IScoreCard> GetScoreCardByName(string entityType, string productId, string name)
        {
            return await ProductRuleScoreCardRepository.GetByName(entityType, productId, name);
        }

        public async Task<List<ISegment>> GetAllSegment(string entityType)
        {
            return await ProductRuleSegmentRepository.GetAllSegment(entityType);
        }

        public async Task<List<IPricingStrategy>> GetAllPricingStrategy(string entityType)
        {
            return await PricingStrategyRepository.GetAllPricingStrategy(entityType);
        }

        public async Task<List<IScoreCard>> GetAllScoreCard(string entityType)
        {
            return await ProductRuleScoreCardRepository.GetAllScoreCard(entityType);
        }
        public async Task<ISegment> AddSegment(string entityType, string productId, ISegmentRequest segmentRequest)
        {
            if (segmentRequest == null)
                throw new ArgumentNullException(nameof(segmentRequest));

            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrEmpty(segmentRequest.SegmentName))
                throw new ArgumentNullException(nameof(segmentRequest.SegmentName));


            var scoreCardNameAlreadyExists = await ProductRuleSegmentRepository.GetByName(entityType, productId, segmentRequest.SegmentName);

            if (scoreCardNameAlreadyExists != null)
                throw new InvalidOperationException($"Segment Name is already exists {segmentRequest.SegmentName}");


            ISegment segmentModel = new Segment();
            segmentModel.EntityType = entityType;
            segmentModel.ProductId = productId;
            segmentModel.RuleDefinitionId = segmentRequest.RuleDefinitionId;
            segmentModel.SegmentName = segmentRequest.SegmentName;
            segmentModel.SegmentStorageName = segmentRequest.SegmentStorageName;
            segmentModel.SegmentType = segmentRequest.SegmentType;
            segmentModel.SegmentVariableSource = segmentRequest.SegmentVariableSource;
            segmentModel.Description = segmentRequest.Description;
            segmentModel.DataAttributeStoreName = segmentRequest.DataAttributeStoreName;
            segmentModel.Data = segmentRequest.Data;
            segmentModel.ExpiryDate = new TimeBucket(segmentRequest.ExpiryDate);
            segmentModel.EffectiveDate = new TimeBucket(segmentRequest.EffectiveDate);
            segmentModel.CreatedOn = new TimeBucket(TenantTime.Now);
            segmentModel.ApplicationType = segmentRequest.ApplicationType;
            segmentModel.CustomDate = new TimeBucket(segmentRequest.CustomDate);
            await Task.Run(() => ProductRuleSegmentRepository.Add(segmentModel));

            //await EventHubClient.Publish(new ApplicantCreated { Applicant = applicantModel });

            return segmentModel;
        }

        public async Task UpdateSegment(string segmentId, ISegmentRequest segmentRequest)
        {
            if (segmentRequest == null)
                throw new ArgumentNullException(nameof(segmentRequest));

            if (string.IsNullOrEmpty(segmentId))
                throw new ArgumentNullException(nameof(segmentId));

            var segmentDetails = await ProductRuleSegmentRepository.Get(segmentId);

            if (segmentDetails == null)
                throw new NotFoundException($"Segment {segmentId} not found");

            segmentDetails.RuleDefinitionId = segmentRequest.RuleDefinitionId;
            segmentDetails.SegmentName = segmentRequest.SegmentName;
            segmentDetails.SegmentStorageName = segmentRequest.SegmentStorageName;
            segmentDetails.SegmentType = segmentRequest.SegmentType;
            segmentDetails.SegmentVariableSource = segmentRequest.SegmentVariableSource;
            segmentDetails.DataAttributeStoreName = segmentRequest.DataAttributeStoreName;
            segmentDetails.Description = segmentRequest.Description;
            segmentDetails.Data = segmentRequest.Data;
            segmentDetails.UpdatedOn = new TimeBucket(TenantTime.Now);
            segmentDetails.ExpiryDate = new TimeBucket(segmentRequest.ExpiryDate);
            segmentDetails.EffectiveDate = new TimeBucket(segmentRequest.EffectiveDate);
            segmentDetails.ApplicationType = segmentRequest.ApplicationType;
            segmentDetails.CustomDate = new TimeBucket(segmentRequest.CustomDate);
            await Task.Run(() => ProductRuleSegmentRepository.Update(segmentDetails));

            //await EventHubClient.Publish(new ApplicantCreated { Applicant = applicantModel });


        }

        public async Task<ISegment> GetSegment(string segmentId)
        {
            if (string.IsNullOrEmpty(segmentId))
                throw new ArgumentNullException(nameof(segmentId));

            var segmentDetails = await ProductRuleSegmentRepository.Get(segmentId);

            if (segmentDetails == null)
                throw new NotFoundException($"Segment {segmentId} not found");

            return segmentDetails;

        }


        public async Task<List<IRuleDefinition>> GetAllRuleDefinition(string entityType, string productId, string ruleType)
        {
            EnsureInputIsValid(entityType, productId);
            if (string.IsNullOrEmpty(ruleType))
                throw new ArgumentException($"{nameof(ruleType)} is mandatory");

            var ruleDetails = await RuleDefinitionRepository.GetAll(entityType, productId, ruleType);
            return ruleDetails;
        }
        public async Task<IRuleDefinition> GetRuleDefinitionByName(string entityType, string productId, string ruleName)
        {
            EnsureInputIsValid(entityType, productId);
            if (string.IsNullOrEmpty(ruleName))
                throw new ArgumentException($"{nameof(ruleName)} is mandatory");

            var ruleDetail = await RuleDefinitionRepository.GetByName(entityType, productId, ruleName);
            return ruleDetail;
        }

        public async Task<List<IRuleDefinition>> GetRuleDefinitionDetails(string entityType, string className, string productId)
        {
            entityType = EnsureEntityType(entityType);

            var ruleDetail = await RuleDefinitionRepository.GetByRuleDetails(entityType, className, productId);
            return ruleDetail;
        }
        public async Task<IRuleDefinition> AddRuleDefinition(string entityType, string productId, IRuleDefinitionAddRequest addRuleRequest)
        {
            if (addRuleRequest == null)
                throw new ArgumentException($"{nameof(addRuleRequest)} is mandatory");

            EnsureInputIsValid(entityType, productId);
            var existsRule = await GetRuleDefinitionByName(entityType, productId, addRuleRequest.RuleName);
            if (existsRule != null && !string.IsNullOrWhiteSpace(existsRule.Id))
            {
                throw new InvalidOperationException($"Rule with name {addRuleRequest.RuleName} is already exists for {entityType}-{productId} ");
            }

            IRuleDefinition ruleDefinition = new RuleDefinition(addRuleRequest);
            ruleDefinition.EntityType = entityType;
            ruleDefinition.ProductId = productId;
            ruleDefinition.ApplicationType = addRuleRequest.ApplicationType;
            ruleDefinition.CustomDate = new TimeBucket(addRuleRequest.CustomDate);
            ruleDefinition.CreatedDate = new TimeBucket(TenantTime.Now);
            RuleDefinitionRepository.Add(ruleDefinition);
            return ruleDefinition;
        }
        public async Task<IRuleDefinition> UpdateRuleDefinition(string ruleId, IRuleDefinitionUpdateRequest updateRuleRequest)
        {
            if (string.IsNullOrEmpty(ruleId))
                throw new ArgumentException($"{nameof(ruleId)} is mandatory");

            if (updateRuleRequest == null)
                throw new ArgumentException($"{nameof(updateRuleRequest)} is mandatory");

            updateRuleRequest.LastUpdatedDate = new TimeBucket(TenantTime.Now);
            var ruleDetail = await RuleDefinitionRepository.Update(ruleId, updateRuleRequest);
            return ruleDetail;
        }
        public async Task DeleteRule(string ruleId)
        {
            if (string.IsNullOrEmpty(ruleId))
                throw new ArgumentException($"{nameof(ruleId)} is mandatory");

            var rule = await RuleDefinitionRepository.GetById(ruleId);
            if (rule == null)
                throw new NotFoundException($"Rule definition with id {ruleId} not found");

            RuleDefinitionRepository.Remove(rule);
        }


        public async Task DeleteSegment(string segmentId)
        {
            if (string.IsNullOrEmpty(segmentId))
                throw new ArgumentNullException(nameof(segmentId));

            var segmentDetails = await ProductRuleSegmentRepository.Get(segmentId);

            if (segmentDetails == null)
                throw new NotFoundException($"Segment {segmentId} not found");


            await Task.Run(() => ProductRuleSegmentRepository.Remove(segmentDetails));

        }


        public async Task<ProductRuleResult> RunRule(string entityType, string entityId, string productId, string ruleDefinationName, string secondaryName = null)
        {
            EnsureInputIsValid(entityType, entityId, productId);

            if (string.IsNullOrWhiteSpace(ruleDefinationName))
                throw new ArgumentException($"{nameof(ruleDefinationName)} is mandatory");

            entityType = EnsureEntityType(entityType);

            IRuleDefinition ruleDetail = await GetRuleDefinitionByName(entityType, productId, ruleDefinationName);

            if (ruleDetail == null)
                throw new NotFoundException($"{ruleDefinationName} is not found for {entityType}-{productId}");

            //   ruleDetail = await CheckDefinitionByDate(entityType, entityId, ruleDetail);

            IProductRuleSourceResult ruleResult = new ProductRuleSourceResult();


            ruleResult = await ExecuteRule(entityType, entityId, productId, ruleDetail.RuleDetail, ruleDetail.DataAttributeStoreName, secondaryName);

            if (!string.IsNullOrEmpty(ruleDetail.DataAttributeStoreName) && (ruleResult != null && ruleResult.Data != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDetail.DataAttributeStoreName, ruleResult.Data);
            }

            //var executionResult = new ProductRuleResult();
            var executionDetail = new ProductRuleResult
            {
                EntityId = entityId,
                EntityType = entityType,
                ExecutedDate = new TimeBucket(TenantTime.Now),
                IntermediateData = ruleResult.Data,
                Name = ruleDetail.RuleName,
                ResultDetail = ruleResult.Detail,
                Result = ruleResult.Result,
                ConfigurationDetail = ruleDetail,
                SourceData = ruleResult.SourceData,
                ExceptionDetail = ruleResult.Exception
            };
            ProductRuleExecutionRepository.Add(executionDetail);
            //resultDetails.Add(executionDetail);
            await EventHub.Publish(new ProductRuleExecuted { ProductRuleResult = executionDetail });
            return executionDetail;
        }
        private async Task<ProductRuleResult> RunRuleById(string entityType, string entityId, string productId, string ruleDefinationId, string secondaryName = null)
        {
            IRuleDefinition ruleDetail = await GetRuleDefinitionById(ruleDefinationId);

            if (ruleDetail == null)
                throw new NotFoundException($"{ruleDefinationId} is not found for {entityType}-{productId}");

            //  ruleDetail = await CheckDefinitionByDate(entityType, entityId, ruleDetail);

            IProductRuleSourceResult ruleResult = new ProductRuleSourceResult();
            ruleResult = await ExecuteRule(entityType, entityId, productId, ruleDetail.RuleDetail, ruleDetail.DataAttributeStoreName, secondaryName);

            if (!string.IsNullOrEmpty(ruleDetail.DataAttributeStoreName) && (ruleResult != null && ruleResult.Data != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDetail.DataAttributeStoreName, ruleResult.Data);
            }
            var executionDetail = new ProductRuleResult
            {
                EntityId = entityId,
                EntityType = entityType,
                ExecutedDate = new TimeBucket(TenantTime.Now),
                IntermediateData = ruleResult.Data,
                Name = ruleDetail.RuleName,
                ResultDetail = ruleResult.Detail,
                Result = ruleResult.Result,
                ConfigurationDetail = ruleDetail,
                SourceData = ruleResult.SourceData,
                ExceptionDetail = ruleResult.Exception
            };
            ProductRuleExecutionRepository.Add(executionDetail);
            await EventHub.Publish(new ProductRuleExecuted { ProductRuleResult = executionDetail });
            return executionDetail;
        }
        private async Task<ProductRuleResult> RunRuleForProduct(string entityType, string entityId, string productId, string ruleDefinationId, Dictionary<string, object> productParameters = null)
        {
            IRuleDefinition ruleDetail = await GetRuleDefinitionById(ruleDefinationId);

            if (ruleDetail == null)
                throw new NotFoundException($"{ruleDefinationId} is not found for {entityType}-{productId}");

            //  ruleDetail = await CheckDefinitionByDate(entityType, entityId, ruleDetail);

            IProductRuleSourceResult ruleResult = new ProductRuleSourceResult();
            ruleResult = await ExecuteRuleForProduct(entityType, entityId, productId, ruleDetail.RuleDetail, productParameters);

            if (!string.IsNullOrEmpty(ruleDetail.DataAttributeStoreName) && (ruleResult != null && ruleResult.Data != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDetail.DataAttributeStoreName, ruleResult.Data);
            }
            var executionDetail = new ProductRuleResult
            {
                EntityId = entityId,
                EntityType = entityType,
                ExecutedDate = new TimeBucket(TenantTime.Now),
                IntermediateData = ruleResult.Data,
                Name = ruleDetail.RuleName,
                ResultDetail = ruleResult.Detail,
                Result = ruleResult.Result,
                ConfigurationDetail = ruleDetail,
                SourceData = ruleResult.SourceData,
                ExceptionDetail = ruleResult.Exception
            };
            ProductRuleExecutionRepository.Add(executionDetail);
            await EventHub.Publish(new ProductRuleExecuted { ProductRuleResult = executionDetail });
            return executionDetail;
        }

        public IRuleDefinition CheckDefinitionByDate(string entityType, string entityId, IRuleDefinition ruleDefinition, IDictionary<string, object> allAttributes)
        {
            var applicationDate = TenantTime.Now;

            if (ruleDefinition.ApplicationType == ApplicationType.ApplicationDate)
            {
                object application = null;
                allAttributes.TryGetValue(Char.ToLowerInvariant("application"[0]) + "application".Substring(1), out application);

                if (application == null)
                    throw new NotFoundException("No data attributes found for sources.");
                //  var application = await DataAttributesEngine.GetAttribute(entityType, entityId, "application");
                var date = GetApplicationDateValue(application, applicationDate);
                applicationDate = DateTimeOffset.Parse(date);
            }
            else if (ruleDefinition.ApplicationType == ApplicationType.CustomDate)
            {
                if (ruleDefinition.CustomDate == null)
                    throw new ArgumentException($"CustomDate is mandatory");

                applicationDate = ruleDefinition.CustomDate.Time;
            }
            if (ruleDefinition.EffectiveDate.Time > applicationDate || ruleDefinition.ExpiryDate.Time < applicationDate)
                throw new NotFoundException($"No ruleDefinition found for Application Date {applicationDate}");
            return ruleDefinition;

        }

        public ILookup CheckDefinitionByDate(string entityType, string entityId, ILookup lookup, IDictionary<string, object> allAttributes)
        {
            var applicationDate = TenantTime.Now;

            if (lookup.ApplicationType == ApplicationType.ApplicationDate)
            {
                object application = null;
                allAttributes.TryGetValue(Char.ToLowerInvariant("application"[0]) + "application".Substring(1), out application);

                if (application == null)
                    throw new NotFoundException("No data attributes found for sources.");
                var date = GetApplicationDateValue(application, applicationDate);
                applicationDate = DateTimeOffset.Parse(date);
            }
            else if (lookup.ApplicationType == ApplicationType.CustomDate)
            {
                if (lookup.CustomDate == null)
                    throw new ArgumentException($"CustomDate is mandatory");

                applicationDate = lookup.CustomDate.Time;
            }
            if (lookup.EffectiveDate.Time > applicationDate || lookup.ExpiryDate.Time < applicationDate)
                throw new NotFoundException($"No lookup found for Application Date {applicationDate}");
            return lookup;

        }

        public IScoreCard CheckDefinitionByDate(string entityType, string entityId, IScoreCard scoreCard, IDictionary<string, object> allAttributes)
        {
            var applicationDate = TenantTime.Now;

            if (scoreCard.ApplicationType == ApplicationType.ApplicationDate)
            {
                object application = null;
                allAttributes.TryGetValue(Char.ToLowerInvariant("application"[0]) + "application".Substring(1), out application);

                if (application == null)
                    throw new NotFoundException("No data attributes found for sources.");
                var date = GetApplicationDateValue(application, applicationDate);
                applicationDate = DateTimeOffset.Parse(date);
            }
            else if (scoreCard.ApplicationType == ApplicationType.CustomDate)
            {
                if (scoreCard.CustomDate == null)
                    throw new ArgumentException($"CustomDate is mandatory");

                applicationDate = scoreCard.CustomDate.Time;
            }
            if (scoreCard.EffectiveDate.Time > applicationDate || scoreCard.ExpiryDate.Time < applicationDate)
                throw new NotFoundException($"No Scorecard found for Application Date {applicationDate}");
            return scoreCard;

        }

        public ISegment CheckDefinitionByDate(string entityType, string entityId, ISegment segment, IDictionary<string, object> allAttributes)
        {
            var applicationDate = TenantTime.Now;

            if (segment.ApplicationType == ApplicationType.ApplicationDate)
            {
                object application = null;
                allAttributes.TryGetValue(Char.ToLowerInvariant("application"[0]) + "application".Substring(1), out application);

                if (application == null)
                    throw new NotFoundException("No data attributes found for sources.");
                var date = GetApplicationDateValue(application, applicationDate);
                applicationDate = DateTimeOffset.Parse(date);
            }
            else if (segment.ApplicationType == ApplicationType.CustomDate)
            {
                if (segment.CustomDate == null)
                    throw new ArgumentException($"CustomDate is mandatory");

                applicationDate = segment.CustomDate.Time;
            }
            if (segment.EffectiveDate.Time > applicationDate || segment.ExpiryDate.Time < applicationDate)
                throw new NotFoundException($"No segment found for Application Date {applicationDate}");
            return segment;

        }

        private static string GetApplicationDateValue(dynamic data, DateTimeOffset applicationDate)
        {
            Func<dynamic> resultProperty = () => data.applicationDate.Time;
            return HasProperty(resultProperty) ? GetValue(resultProperty) : applicationDate.ToString();
        }

        private static string GetResult(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.Score;
            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

        public async Task<IProductRuleResult> RunSegment(string entityType, string entityId, string productId, string segmentId, string secondaryName = null)
        {
            return await RunSegmentInternalProcess(entityType, entityId, productId, segmentId, null, secondaryName);
        }
        public async Task<IProductRuleResult> RunSegmentInternal(string entityType, string entityId, string productId, string segmentId, string Weightage = null)
        {
            EnsureInputIsValid(entityType, entityId, productId);

            if (string.IsNullOrWhiteSpace(segmentId))
                throw new ArgumentException($"{nameof(segmentId)} is mandatory");

            //   entityType = EnsureEntityType(entityType);

            ISegment segmentDetail = await GetSegment(segmentId);

            if (segmentDetail == null)
                throw new NotFoundException($"{segmentId} is not found for {entityType}-{productId}");

            //   segmentDetail = await CheckDefinitionByDate(entityType, entityId, segmentDetail);

            SegmentationResult segmentResult = null;
            IProductRuleResult executionDetail = new ProductRuleResult
            {
                EntityType = entityType,
                EntityId = entityId,
                ExecutedDate = new TimeBucket(TenantTime.Now)
            };
            if (segmentDetail.SegmentType == SegmentType.Rule)
            {
                executionDetail = await RunRuleById(entityType, entityId, productId, segmentDetail.RuleDefinitionId);
            }
            else
            {
                var segmentVariableSource = segmentDetail.SegmentVariableSource;

                if (segmentVariableSource == null)
                    throw new NotFoundException($"No segment variable found for {segmentDetail.SegmentName} for {entityType}-{productId}");

                Dictionary<string, object> attributes = new Dictionary<string, object>();
                FaultRetry.RunWithAlwaysRetry(() =>
                 {
                     attributes = DataAttributesEngine.GetAttribute(entityType, entityId, new List<string> { segmentVariableSource.InputSource }).Result;
                     if (attributes == null || !attributes.Any())
                         throw new NotFoundException($"No attributes found for {segmentVariableSource.InputSource}  for {entityId}-{entityType}-{productId}");
                 });
                var input = GetResultValue(attributes.FirstOrDefault().Value, segmentVariableSource.InputVariable);

                if (segmentDetail.SegmentType == SegmentType.Range)
                {
                    double segmentInput = 0.0;
                    double.TryParse(input, out segmentInput);
                    if (segmentInput >= 0)
                    {
                        if (segmentDetail.Data == null)
                            throw new NotFoundException($"No data found for {segmentDetail.SegmentName}  for {entityId}-{entityType}-{productId}");

                        var rangeList = JsonConvert.DeserializeObject<List<SegmentationRange>>(segmentDetail.Data.ToString());
                        SegmentationRange range = null;
                        if (rangeList.Any(x => x.LowerLimit == null))
                        {
                            var rangeTempList = rangeList.Where(x => x.LowerLimit == null).ToList();
                            range = rangeTempList.Where(x => x.UpperLimit >= segmentInput).FirstOrDefault();
                        }
                        if (rangeList.Any(x => x.UpperLimit == null))
                        {
                            var rangeTempList = rangeList.Where(x => x.UpperLimit == null).ToList();
                            range = rangeTempList.Where(x => x.LowerLimit <= segmentInput).FirstOrDefault();
                        }
                        if (range == null)
                            range = rangeList.Where(x => x.LowerLimit <= segmentInput && x.UpperLimit >= segmentInput).FirstOrDefault();

                        if (range == null)
                            throw new NotFoundException($"No segmentation found for {segmentVariableSource.InputVariable}");

                        string segmentLabel = range.SegmentLabel;
                        segmentResult = new SegmentationResult
                        {
                            key = segmentVariableSource.InputVariable,
                            value = Weightage != null && Convert.ToDouble(Weightage) > 0 ? (Convert.ToDouble(segmentLabel) * Convert.ToDouble(Weightage)).ToString() : segmentLabel
                        };
                        Dictionary<string, object> data = new Dictionary<string, object>();
                        data.Add(segmentDetail.SegmentName, segmentResult);
                        executionDetail.ExceptionDetail = null;
                        executionDetail.IntermediateData = data;
                        executionDetail.Result = Result.Passed;
                        executionDetail.ResultDetail = null;
                        executionDetail.SourceData = attributes;
                        executionDetail.Name = segmentDetail.SegmentName;
                    }
                }
                else if (segmentDetail.SegmentType == SegmentType.List)
                {
                    string segmentLabel = string.Empty;
                    if (!string.IsNullOrWhiteSpace(input))
                    {
                        if (segmentDetail.Data == null)
                            throw new NotFoundException($"No data found for {segmentDetail.SegmentName}  for {entityId}-{entityType}-{productId}");

                        var list = JsonConvert.DeserializeObject<List<SegmentationList>>(segmentDetail.Data.ToString());
                        foreach (var lst in list)
                        {
                            if (lst.InListValues != null && lst.InListValues.Any())
                            {
                                if (lst.InListValues.Contains(input))
                                {
                                    segmentLabel = lst.SegmentLabel;
                                    break;
                                }
                            }
                            else if (lst.ExceptListValues != null && lst.ExceptListValues.Any())
                            {
                                if (lst.ExceptListValues.Contains(input))
                                {
                                    segmentLabel = lst.SegmentLabel;
                                    break;
                                }
                            }
                        }
                        if (string.IsNullOrWhiteSpace(segmentLabel))
                            throw new NotFoundException($"No segmentation found for {segmentVariableSource.InputVariable}");

                        segmentResult = new SegmentationResult
                        {
                            key = segmentVariableSource.InputVariable,
                            value = Weightage != null && Convert.ToDouble(Weightage) > 0 ? (Convert.ToDouble(segmentLabel) * Convert.ToDouble(Weightage)).ToString() : segmentLabel
                        };
                        Dictionary<string, object> data = new Dictionary<string, object>();
                        data.Add(segmentDetail.SegmentName, segmentResult);
                        executionDetail.ExceptionDetail = null;
                        executionDetail.IntermediateData = data;
                        executionDetail.Result = Result.Passed;
                        executionDetail.ResultDetail = null;
                        executionDetail.SourceData = attributes;
                        executionDetail.Name = segmentDetail.SegmentName;
                    }
                }
            }

            ProductRuleExecutionRepository.Add(executionDetail);
            if (!string.IsNullOrEmpty(segmentDetail.DataAttributeStoreName) && (segmentResult != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, segmentDetail.DataAttributeStoreName, segmentResult, segmentDetail.SegmentName);
            }
            return executionDetail;
        }
        public async Task<IProductRuleResult> RunScoreCard(string entityType, string entityId, string productId, string scoreCardId, Dictionary<string, string> referenceNumbers = null)
        {
            EnsureInputIsValid(entityType, entityId, productId);

            if (string.IsNullOrWhiteSpace(scoreCardId))
                throw new ArgumentException($"{nameof(scoreCardId)} is mandatory");

            //  entityType = EnsureEntityType(entityType);

            IScoreCard scoreCardDetail = await GetScoreCard(scoreCardId);

            if (scoreCardDetail == null)
                throw new NotFoundException($"{scoreCardId} is not found for {entityType}-{productId}");

            //   scoreCardDetail = await CheckDefinitionByDate(entityType, entityId, scoreCardDetail);


            var scoringVariables = scoreCardDetail.ScoreCardVariables;

            if (scoringVariables != null && scoringVariables.Any())
            {
                foreach (var score in scoringVariables)
                {

                    if (score.VariableType == VariableType.Rule)
                    {
                        IRuleDefinition ruleDefinition = null;
                        ruleDefinition = await GetRuleDefinitionById(score.DefinitionId);

                        if (ruleDefinition == null)
                            throw new NotFoundException($"Rule definition not found for {score.VariableName} for {scoreCardDetail.ScoreCardName}-{entityType}-{productId}");
                        // ruleDefinition = await CheckDefinitionByDate(entityType, entityId, ruleDefinition);

                        var result = await ExecuteRule(entityType, entityId, productId, ruleDefinition.RuleDetail, ruleDefinition.DataAttributeStoreName);
                        if (result.Result == Result.Failed)
                        {
                            throw new InvalidOperationException("");
                        }
                        if (score.VariableType == VariableType.Rule)
                        {
                            object input = null;
                            result.Data.TryGetValue("value", out input);
                            var weightedScore = (Convert.ToDouble(input) * Convert.ToDouble(score.Weightage));
                            result.Data["value"] = weightedScore;
                        }

                        if (!string.IsNullOrEmpty(ruleDefinition.DataAttributeStoreName) && (result != null && result.Data != null))
                        {
                            await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDefinition.DataAttributeStoreName, result.Data, scoreCardDetail.ScoreCardName);
                        }
                    }
                    else if (score.VariableType == VariableType.SegmentRule)
                    {
                        var segment = await RunSegmentInternal(entityType, entityId, productId, score.DefinitionId, score.Weightage.ToString());

                    }
                    else if (score.VariableType == VariableType.LookupRule)
                    {
                        var lookup = await RunLookup(entityType, entityId, productId, score.DefinitionId);

                    }
                    else if (score.VariableType == VariableType.DataAttribute)
                    {
                        var variableSource = score.ScoreCardVariableSource;

                        if (variableSource == null)
                            throw new NotFoundException($"No source found for {score.VariableName} for {scoreCardDetail.ScoreCardName}-{entityType}-{productId}");

                        Dictionary<string, object> attributes = new Dictionary<string, object>();
                        FaultRetry.RunWithAlwaysRetry(() =>
                        {
                            attributes = DataAttributesEngine.GetAttribute(entityType, entityId, new List<string> { variableSource.InputSource }).Result;
                            if (attributes == null || !attributes.Any())
                                throw new NotFoundException("No data attributes found for sources.");
                        });
                        var input = GetResultValue(attributes.FirstOrDefault().Value, variableSource.InputVariable);
                        ScoringResult scoringResult = new ScoringResult
                        {
                            VariableName = score.VariableName,
                            ScoringValue = input, // to be confirm
                            WeightedScore = (Convert.ToDouble(input) * Convert.ToDouble(score.Weightage)).ToString()
                        };

                        Dictionary<string, object> data = new Dictionary<string, object>();
                        data.Add(scoreCardDetail.ScoreCardName, scoringResult);

                        IProductRuleSourceResult ruleResult = new ProductRuleSourceResult();
                        var executionDetail = new ProductRuleResult
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            ExecutedDate = new TimeBucket(TenantTime.Now),
                            IntermediateData = ruleResult.Data,
                            Name = scoreCardDetail.ScoreCardName,
                            ResultDetail = ruleResult.Detail,
                            Result = ruleResult.Result,
                            SourceData = ruleResult.SourceData,
                            ExceptionDetail = ruleResult.Exception
                        };
                        ProductRuleExecutionRepository.Add(executionDetail);
                        if (!string.IsNullOrEmpty(score.DataAttributeStoreName) && (scoringResult != null))
                        {
                            await DataAttributesEngine.SetAttribute(entityType, entityId, score.DataAttributeStoreName, scoringResult, scoreCardDetail.ScoreCardName);
                        }
                    }
                }
            }

            var ruleDetail = await GetRuleDefinitionById(scoreCardDetail.RuleDefinitionId);

            if (ruleDetail == null)
                throw new NotFoundException($"{ruleDetail.RuleName} is not found for {scoreCardDetail.ScoreCardName}-{entityType}-{productId}");

            // ruleDetail = await CheckDefinitionByDate(entityType, entityId, ruleDetail);

            IProductRuleSourceResult ruleExecutionResult = new ProductRuleSourceResult();
            ruleExecutionResult = await ExecuteRule(entityType, entityId, productId, ruleDetail.RuleDetail, ruleDetail.DataAttributeStoreName, null);

            if (!string.IsNullOrEmpty(ruleDetail.DataAttributeStoreName) && (ruleExecutionResult != null && ruleExecutionResult.Data != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDetail.DataAttributeStoreName, ruleExecutionResult.Data);
            }
            //var executionResult = new ProductRuleResult();
            var ruleExecutionDetail = new ProductRuleResult
            {
                EntityId = entityId,
                EntityType = entityType,
                ExecutedDate = new TimeBucket(TenantTime.Now),
                IntermediateData = ruleExecutionResult.Data,
                Name = ruleDetail.RuleName,
                ResultDetail = ruleExecutionResult.Detail,
                Result = ruleExecutionResult.Result,
                ConfigurationDetail = ruleDetail,
                SourceData = ruleExecutionResult.SourceData,
                ExceptionDetail = ruleExecutionResult.Exception
            };
            ProductRuleExecutionRepository.Add(ruleExecutionDetail);

            await EventHub.Publish(new ProductRuleExecuted { ProductRuleResult = ruleExecutionDetail });
            return ruleExecutionDetail;
        }


        private async Task AddStrategyHistory(string entityType, string entityId, string productId, IPricingStrategy pricingStrategyDetail, IProductRuleResult scorecard)
        {
            IStrategyHistory strategyHistory = new StrategyHistory();
            strategyHistory.EntityId = entityId;
            strategyHistory.EntityType = entityType;
            strategyHistory.ProductId = productId;
            strategyHistory.Result = scorecard.Result;
            strategyHistory.StrategyName = pricingStrategyDetail.Name;
            strategyHistory.StrategyStepName = scorecard.Name;
            strategyHistory.IntermediateData = scorecard.IntermediateData;
            strategyHistory.ResultDetail = scorecard.ResultDetail;
            strategyHistory.ExceptionDetail = scorecard.ExceptionDetail;
            strategyHistory.ExecutedDate = new TimeBucket(TenantTime.Now);
            strategyHistory.StrategyId = pricingStrategyDetail.Id;
            await Task.Run(() => StrategyExceutionHistoryRepository.Add(strategyHistory));
        }

        private async Task<IProductRuleSourceResult> ExecuteRule(string entityType, string entityId, string productId, IRuleDetail ruleDetail, string DataAttributeStoreName, string secondaryName = null)
        {
            return await Task.Run(() =>
            {
                if (string.IsNullOrEmpty(ruleDetail?.DERuleName))
                    throw new InvalidOperationException($"Rule is not set for {ruleDetail.DERuleName}");

                var requiredSources = ruleDetail.RequiredSources;

                var sources = new List<string>();
                if (requiredSources != null && requiredSources.Any())
                {
                    sources.AddRange(requiredSources);
                }

                var optionalSources = ruleDetail.OptionalSources;
                if (optionalSources != null && optionalSources.Any())
                {
                    sources.AddRange(optionalSources);
                }
                Dictionary<string, object> attributes = new Dictionary<string, object>();
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    if (sources.Any())
                    {
                        if (!string.IsNullOrEmpty(secondaryName))
                        {
                            var requestModel = new AttributeRequest();
                            requestModel.Names = sources;
                            requestModel.SecondaryName = secondaryName;
                            attributes = DataAttributesEngine.GetAttribute(entityType, entityId, requestModel).Result;
                        }
                        else
                            attributes = DataAttributesEngine.GetAttribute(entityType, entityId, sources).Result;

                        if (attributes == null || !attributes.Any() || attributes.FirstOrDefault().Value == null)
                            throw new NotFoundException("No data attributes found for sources.");
                    }
                    if (requiredSources != null && requiredSources.Any())
                    {
                        var isAllAttributes = requiredSources.All(r => attributes.Any(a => string.Equals(r, a.Key, StringComparison.InvariantCultureIgnoreCase)));
                        if (!isAllAttributes)
                            throw new InvalidOperationException("Data attributes for all sources are not found");
                    }
                });
                attributes.Add("entityId", entityId);
                attributes.Add("entityType", entityType);

                var executionResult = DecisionEngineService.Execute<dynamic, ProductRuleSourceResult>(ruleDetail.DERuleName, new { payload = attributes });

                executionResult.SourceData = attributes;
                return executionResult;
            });
        }

        private async Task<IProductRuleSourceResult> ExecuteRuleForProduct(string entityType, string entityId, string productId, IRuleDetail ruleDetail, Dictionary<string, object> productParameters = null)
        {
            return await Task.Run(() => {
                if (string.IsNullOrEmpty(ruleDetail?.DERuleName))
                    throw new InvalidOperationException($"Rule is not set for {ruleDetail.DERuleName}");

                var requiredSources = ruleDetail.RequiredSources;

                var sources = new List<string>();
                if (requiredSources != null && requiredSources.Any())
                {
                    sources.AddRange(requiredSources);
                }

                var optionalSources = ruleDetail.OptionalSources;
                if (optionalSources != null && optionalSources.Any())
                {
                    sources.AddRange(optionalSources);
                }
                Dictionary<string, object> attributes = new Dictionary<string, object>();

                if (sources.Any())
                {
                    attributes = DataAttributesEngine.GetAttribute(entityType, entityId, sources).Result;
                    if (attributes == null || !attributes.Any())
                        throw new NotFoundException("No data attributes found for sources.");
                }
                if (requiredSources != null && requiredSources.Any())
                {
                    var isAllAttributes = requiredSources.All(r => attributes.Any(a => string.Equals(r, a.Key, StringComparison.InvariantCultureIgnoreCase)));
                    if (!isAllAttributes)
                        throw new InvalidOperationException("Data attributes for all sources are not found");
                }
                attributes.Add("entityId", entityId);
                attributes.Add("entityType", entityType);
                attributes.Add("productParameters", productParameters);

                var executionResult = DecisionEngineService.Execute<dynamic, ProductRuleSourceResult>(ruleDetail.DERuleName, new { payload = attributes });

                executionResult.SourceData = attributes;
                return executionResult;
            });            
        }

        public async Task<IProductRuleResult> RunLookup(string entityType, string entityId, string productId, string lookupId)
        {
            EnsureInputIsValid(entityType, productId);

            if (string.IsNullOrWhiteSpace(lookupId))
                throw new NotFoundException($"Lookup with id {lookupId} not found for {entityType}-{productId}");

            ILookup lookupDetail = await GetLookUp(lookupId);

            if (lookupDetail == null)
                throw new NotFoundException($"{lookupId} is not found for {entityType}-{productId}");

            //  lookupDetail = await CheckDefinitionByDate(entityType, entityId, lookupDetail);

            Dictionary<string, object> lookupData = new Dictionary<string, object>();
            lookupData.Add("lookupBy", lookupDetail.LookupBy);
            lookupData.Add("data", lookupDetail.Data);
            if (!string.IsNullOrEmpty(lookupDetail.DataAttributeStoreName) && (lookupData.Any()))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, lookupDetail.DataAttributeStoreName, lookupData, lookupDetail.LookupName);
            }

            if (string.IsNullOrWhiteSpace(lookupDetail.RuleDefinitionId))
                throw new NotFoundException($"Rule definition is not defined for {entityType}-{productId}");

            IProductRuleResult executionResult = await RunRuleById(entityType, entityId, productId, lookupDetail.RuleDefinitionId);
            return executionResult;
        }

        public async Task<IProductRuleResult> RunLookupFromStrategy(string entityType, string entityId, string productId, string lookupId, string secondaryName = null)
        {

            if (string.IsNullOrWhiteSpace(lookupId))
                throw new NotFoundException($"Lookup with id {lookupId} not found for {entityType}-{productId}");

            ILookup lookupDetail = await GetLookUp(lookupId);

            if (lookupDetail == null)
                throw new NotFoundException($"{lookupId} is not found for {entityType}-{productId}");

            //   lookupDetail = await CheckDefinitionByDate(entityType, entityId, lookupDetail);

            Dictionary<string, object> lookupData = new Dictionary<string, object>();
            lookupData.Add("lookupBy", lookupDetail.LookupBy);
            lookupData.Add("data", lookupDetail.Data);
            if (!string.IsNullOrEmpty(lookupDetail.DataAttributeStoreName) && (lookupData.Any()))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, lookupDetail.DataAttributeStoreName, lookupData);
            }

            if (string.IsNullOrWhiteSpace(lookupDetail.RuleDefinitionId))
                throw new NotFoundException($"Rule definition is not defined for {entityType}-{productId}");

            var attributes = await DataAttributesEngine.GetAllAttributes(entityType, entityId, secondaryName);
            lookupDetail = CheckDefinitionByDate(entityType, entityId, lookupDetail, attributes);

            IProductRuleResult executionResult = await RunRuleByIdDetails(entityType, entityId, productId, lookupDetail.RuleDefinitionId, attributes);
            return executionResult;
        }

        public async Task<List<IProductRuleResult>> GetProductRuleResultDetail(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entity id is required");

            var ProductRuleDetail = await ProductRuleExecutionRepository.GetProductRuleDetails(entityType, entityId);
            if (ProductRuleDetail == null || ProductRuleDetail.Count <= 0)
                throw new NotFoundException($"No score card detail found for {entityId} ");
            return ProductRuleDetail;
        }

        public async Task<List<ITemplate>> GetAllTemplate(string entityType)
        {
            return await TemplateRepository.GetAllTemplate(entityType);
        }
        public async Task<ITemplate> GetByEventName(string entityType, string productId, string eventName)
        {
            return await TemplateRepository.GetByEventName(entityType, productId, eventName, TenantTime.Now);
        }

        public async Task<ITemplate> AddTemplate(string entityType, string productId, ITemplateAddRequest templateAddRequest)
        {
            if (templateAddRequest == null)
                throw new ArgumentNullException(nameof(templateAddRequest));

            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrEmpty(productId))
                throw new ArgumentNullException(nameof(productId));

            if (string.IsNullOrWhiteSpace(templateAddRequest.EmailTemplateName) && string.IsNullOrWhiteSpace(templateAddRequest.MobileTemplateName))
                throw new InvalidOperationException($"Parameter name {nameof(templateAddRequest.EmailTemplateName)} and {nameof(templateAddRequest.MobileTemplateName)} can not be null");


            var templateNameAlreadyExists = await TemplateRepository.GetByEventName(entityType, productId, templateAddRequest.EventName, TenantTime.Now);

            if (templateNameAlreadyExists != null)
                throw new InvalidOperationException($"Template  is already exists with event {templateAddRequest.EventName} ");



            ITemplate template = new Template();
            template.EntityType = entityType;
            template.ProductId = productId;
            template.EmailTemplateName = templateAddRequest.EmailTemplateName;
            template.EmailTemplateVersion = templateAddRequest.EmailTemplateVersion;
            template.MobileTemplateName = templateAddRequest.MobileTemplateName;
            template.MobileTemplateVersion = templateAddRequest.MobileTemplateVersion;
            template.Description = templateAddRequest.Description;
            template.IsEnable = templateAddRequest.IsEnable;
            template.EventName = templateAddRequest.EventName;
            template.EffectiveDate = new TimeBucket(templateAddRequest.EffectiveDate);
            template.ExpiryDate = new TimeBucket(templateAddRequest.ExpiryDate);
            template.CreatedDate = new TimeBucket(TenantTime.Now);
            template.RuleDefinationName = templateAddRequest.RuleDefinationName;

            await Task.Run(() => TemplateRepository.Add(template));
            return template;
        }

        public async Task UpdateTemplate(string templateId, ITemplateAddRequest templateAddRequest)
        {
            if (templateAddRequest == null)
                throw new ArgumentNullException(nameof(templateAddRequest));

            if (string.IsNullOrEmpty(templateId))
                throw new ArgumentNullException(nameof(templateId));

            var templateDetails = await TemplateRepository.Get(templateId);

            if (templateDetails == null)
                throw new NotFoundException($"template {templateId} not found");

            templateDetails.EmailTemplateName = templateAddRequest.EmailTemplateName;
            templateDetails.EmailTemplateVersion = templateAddRequest.EmailTemplateVersion;
            templateDetails.MobileTemplateName = templateAddRequest.MobileTemplateName;
            templateDetails.MobileTemplateVersion = templateAddRequest.MobileTemplateVersion;
            templateDetails.Description = templateAddRequest.Description;
            templateDetails.IsEnable = templateAddRequest.IsEnable;
            templateDetails.EventName = templateAddRequest.EventName;
            templateDetails.EffectiveDate = new TimeBucket(templateAddRequest.EffectiveDate);
            templateDetails.ExpiryDate = new TimeBucket(templateAddRequest.ExpiryDate);
            templateDetails.LastUpdatedDate = new TimeBucket(TenantTime.Now);
            templateDetails.RuleDefinationName = templateAddRequest.RuleDefinationName;
            await Task.Run(() => TemplateRepository.Update(templateDetails));


        }





        private static string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString("N");
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
        private void EnsureInputIsValid(string entityType, string productId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(productId))
                throw new ArgumentException($"{nameof(productId)} is mandatory");
        }
        private void EnsureInputIsValid(string entityType, string entityId, string productId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (string.IsNullOrWhiteSpace(productId))
                throw new ArgumentException($"{nameof(productId)} is mandatory");
        }

        private static string GetResultValue(dynamic data, string variableName)
        {
            string result = null;
            if (data != null)
            {
                var myObj = JsonConvert.DeserializeObject<dynamic>(data[0].ToString());
                result = myObj[variableName];
            }
            return result;
        }

        private IProductRuleSourceResult ExecuteRule(Dictionary<string, object> data, IRuleDetail ruleDetail)
        {
            if (string.IsNullOrEmpty(ruleDetail.DERuleName) || string.IsNullOrEmpty(ruleDetail.RuleVersion))
                throw new InvalidOperationException($"Rule is not set for {ruleDetail.DERuleName}");

            var executionResult = DecisionEngineService.Execute<dynamic, ProductRuleSourceResult>(
                        ruleDetail.DERuleName,
                        new { payload = data });

            return executionResult;
        }



        public async Task<IProductRuleResult> RunPricingStrategy(string entityType, string entityId, string productId, string pricingStrategyName, string version, string secondaryName = null)
        {
            Logger.Info($"RunPricingStrategy started for {entityId} and name {pricingStrategyName} at {TenantTime.Now}");
            EnsureInputIsValid(entityType, entityId, productId);

            if (string.IsNullOrWhiteSpace(pricingStrategyName))
                throw new ArgumentException($"{nameof(pricingStrategyName)} is mandatory");


            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException($"{nameof(version)} is mandatory");

            entityType = EnsureEntityType(entityType);

            IPricingStrategy pricingStrategyDetail = await PricingStrategyRepository.GetByNameAndVersion(entityType, productId, pricingStrategyName, version);

            if (pricingStrategyDetail == null)
                throw new NotFoundException($"{pricingStrategyName} and {version} is not found for {entityType}-{productId}");

            var currentDate = TenantTime.Now;
            if (pricingStrategyDetail.EffectiveDate.Time > currentDate || pricingStrategyDetail.ExpiryDate.Time < currentDate)
                throw new NotFoundException($"Pricing Strategy is not activated for current date {currentDate}- EffectiveDate {pricingStrategyDetail.EffectiveDate.Time} and Expiry Date {pricingStrategyDetail.ExpiryDate.Time}");

            var pricingStrategySteps = pricingStrategyDetail.PricingStrategyStep;
            IProductRuleResult result = null;
            Logger.Info($"pricingStrategySteps started {TenantTime.Now}");
            if (pricingStrategySteps != null && pricingStrategySteps.Any())
            {

                var pricingStrategyStpesByOrder = pricingStrategySteps.OrderBy(i => i.SequenceNumber).ToList();
                foreach (var step in pricingStrategyStpesByOrder)
                {
                    if (step.StepType == StepType.ScoreCard)
                    {
                        Logger.Info($"pricingStrategySteps scorecard started {TenantTime.Now}");
                        result = await RunScoreCardFromStrategy(entityType, entityId, productId, step, secondaryName);
                        await AddStrategyHistory(entityType, entityId, productId, pricingStrategyDetail, result);
                        Logger.Info($"pricingStrategySteps scorecard ended {TenantTime.Now}");
                        if (!step.ContinueOnException && result.Result == Result.Failed)
                            return result;
                    }
                    else if (step.StepType == StepType.Segment)
                    {
                        Logger.Info($"pricingStrategySteps segment started {TenantTime.Now}");
                        var segmentDetails = await GetSegmentByName(entityType, productId, step.ParameterName);
                        result = await RunSegment(entityType, entityId, productId, segmentDetails.Id, secondaryName);
                        await AddStrategyHistory(entityType, entityId, productId, pricingStrategyDetail, result);
                        Logger.Info($"pricingStrategySteps segment ended {TenantTime.Now}");
                        if (!step.ContinueOnException && result.Result == Result.Failed)
                            return result;
                    }
                    else if (step.StepType == StepType.Lookup)
                    {
                        Logger.Info($"pricingStrategySteps Lookup started {TenantTime.Now}");
                        var lookupDetails = await GetLookupByName(entityType, productId, step.ParameterName);
                        result = await RunLookupFromStrategy(entityType, entityId, productId, lookupDetails.Id, secondaryName);
                        await AddStrategyHistory(entityType, entityId, productId, pricingStrategyDetail, result);
                        Logger.Info($"pricingStrategySteps Lookup ended {TenantTime.Now}");
                        if (!step.ContinueOnException && result.Result == Result.Failed)
                            return result;
                    }
                    else if (step.StepType == StepType.Rule)
                    {
                        Logger.Info($"pricingStrategySteps Rule started {TenantTime.Now}");
                        var ruleDetails = await GetRuleDefinitionByName(entityType, productId, step.ParameterName);
                        result = await RunRuleById(entityType, entityId, productId, ruleDetails.Id, secondaryName);

                        await AddStrategyHistory(entityType, entityId, productId, pricingStrategyDetail, result);
                        Logger.Info($"pricingStrategySteps Rule ended {TenantTime.Now}");
                        if (!step.ContinueOnException && result.Result == Result.Failed && result.ExceptionDetail.Count > 0)
                            return result;
                        else if (!step.ContinueOnNonEligibility && result.Result == Result.Failed)
                            return result;
                    }
                    else if (step.StepType == StepType.ProductParameters)
                    {
                        await ComputeProductParameters(entityType, entityId, productId, step);
                    }
                }
                Logger.Info($"pricingStrategySteps ended for  {entityId} name {pricingStrategyName} at {TenantTime.Now} ");
            }
            return result;
        }

        private async Task ComputeProductParameters(string entityType, string entityId, string productId, IPricingStrategyStep step)
        {
            var todayDate = DateTime.Now.Date;
            var productDetails = await DataAttributesEngine.GetAttribute(entityType, entityId, "product");

            var productObject = JsonConvert.DeserializeObject<List<ProductDefination>>(productDetails.ToString());
            var productParameterGroup = productObject.FirstOrDefault().ProductGroup.Where(i => i.Name == step.ParameterName).FirstOrDefault();
            if (productParameterGroup != null)
            {
                if (step.ParameterTypes != null && step.ParameterTypes.Count > 0)
                {
                    foreach (var parameterType in step.ParameterTypes)
                    {
                        var productParameters = productObject.FirstOrDefault().ProductParameter.Where(i => i.GroupId == productParameterGroup.Id && i.ParameterType == parameterType).ToList();
                        Dictionary<string, object> parameters = new Dictionary<string, object>();
                        foreach (var item in productParameters)
                        {
                            if (!string.IsNullOrEmpty(item.ApplicableRuleId))
                            {
                                var ruleData = await RunRuleForProduct(entityType, entityId, productId, item.ApplicableRuleId, parameters);
                                if (ruleData.Result == Result.Failed)
                                {
                                    parameters.Add(item.Name, string.Empty); // Adding dynamically named property
                                    continue;
                                }
                            }
                            if (!string.IsNullOrEmpty(item.CalculationRuleId))
                            {
                                var ruleData = await RunRuleForProduct(entityType, entityId, productId, item.CalculationRuleId, parameters);
                                if (ruleData.Result == Result.Failed)
                                    parameters.Add(item.Name, string.Empty); // Adding dynamically named property
                                else
                                {
                                    var intermediateResult = (Dictionary<string, object>)(ruleData.IntermediateData);
                                    parameters.Add(item.Name, intermediateResult.FirstOrDefault().Value);
                                }

                            }
                            if (!string.IsNullOrEmpty(item.Value))
                                parameters.Add(item.Name, item.Value);
                        }
                        await DataAttributesEngine.SetAttribute(entityType, entityId, parameterType, parameters);
                    }

                }
            }
            else
                throw new InvalidOperationException($"Product Parameter Group {step.ParameterName} Not Found For Product {productId}");
        }

        private async Task<IProductRuleResult> RunScoreCardFromStrategy(string entityType, string entityId, string productId, IPricingStrategyStep step, string secondaryName = null)
        {

            Logger.Info($"scorecard implementation started for {step.ParameterName} and {TenantTime.Now}");
            Logger.Info($"scorecard variable GetScoreCardByName started {TenantTime.Now}");
            var scoreCardDetails = await GetScoreCardByName(entityType, productId, step.ParameterName);
            Logger.Info($"scorecard variable GetScoreCardByName ended {TenantTime.Now}");
            var scoringVariables = scoreCardDetails.ScoreCardVariables;

            if (scoringVariables != null && scoringVariables.Any())
            {
                IDictionary<string, object> allAttributes = new Dictionary<string, object>();
                Logger.Info($"scorecard variable GetAllAttributes started {TenantTime.Now}");
                allAttributes = DataAttributesEngine.GetAllAttributes(entityType, entityId, secondaryName).Result;
                Logger.Info($"scorecard variable GetAllAttributes ended {TenantTime.Now}");

                Logger.Info($"scorecard variable implementation started {TenantTime.Now}");
                foreach (var score in scoringVariables)
                {
                    if (score.VariableType == VariableType.Rule)
                    {
                        IRuleDefinition ruleDefinition = null;
                        Logger.Info($"scorecard variable GetRuleDefinitionById started {TenantTime.Now}");
                        ruleDefinition = await GetRuleDefinitionById(score.DefinitionId);
                        Logger.Info($"scorecard variable GetRuleDefinitionById ended {TenantTime.Now}");
                        if (ruleDefinition == null)
                            throw new NotFoundException($"Rule definition not found for {score.VariableName} for {scoreCardDetails.ScoreCardName}-{entityType}-{productId}");
                        ruleDefinition = CheckDefinitionByDate(entityType, entityId, ruleDefinition, allAttributes); // TODO

                        Logger.Info($"scorecard variable ExceuteRuleScoreCard started {TenantTime.Now}");
                        ProductRuleSourceResult executionResult = ExceuteRuleScoreCard(entityType, entityId, allAttributes, ruleDefinition.RuleDetail);
                        Logger.Info($"scorecard variable ExceuteRuleScoreCard ended {TenantTime.Now}");
                        if (executionResult.Result == Result.Failed)
                        {
                            throw new InvalidOperationException("");
                        }
                        if (score.VariableType == VariableType.Rule)
                        {
                            object inputvalue = null;
                            executionResult.Data.TryGetValue("value", out inputvalue);
                            var weightedScore = (Convert.ToDouble(inputvalue) * Convert.ToDouble(score.Weightage));
                            executionResult.Data["value"] = weightedScore;
                        }
                        Logger.Info($"scorecard variable SetAttribute ended {TenantTime.Now}");
                        if (!string.IsNullOrEmpty(ruleDefinition.DataAttributeStoreName) && (executionResult != null && executionResult.Data != null))
                        {
                            await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDefinition.DataAttributeStoreName, executionResult.Data);
                        }
                        Logger.Info($"scorecard variable SetAttribute ended {TenantTime.Now}");
                    }
                    else if (score.VariableType == VariableType.SegmentRule)
                    {
                        var segment = await RunSegmentInternalProcess(entityType, entityId, productId, score.DefinitionId, score.Weightage.ToString(), secondaryName);

                    }
                    else if (score.VariableType == VariableType.LookupRule)
                    {
                        var lookup = await RunLookupFromStrategy(entityType, entityId, productId, score.DefinitionId);

                    }
                    else if (score.VariableType == VariableType.DataAttribute)
                    {
                        var variableSource = score.ScoreCardVariableSource;

                        if (variableSource == null)
                            throw new NotFoundException($"No source found for {score.VariableName} for {scoreCardDetails.ScoreCardName}-{entityType}-{productId}");

                        object inputDataAttributes = null;
                        allAttributes.TryGetValue(Char.ToLowerInvariant(variableSource.InputSource[0]) + variableSource.InputSource.Substring(1), out inputDataAttributes);

                        if (inputDataAttributes == null)
                            throw new NotFoundException("No data attributes found for sources.");

                        var input = GetResultValue(inputDataAttributes, variableSource.InputVariable);
                        ScoringResult scoringResult = new ScoringResult
                        {
                            VariableName = score.VariableName,
                            ScoringValue = input, // to be confirm
                            WeightedScore = (Convert.ToDouble(input) * Convert.ToDouble(score.Weightage)).ToString()
                        };
                        IProductRuleSourceResult ruleResult = new ProductRuleSourceResult();
                        var executionDetail = new ProductRuleResult
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            ExecutedDate = new TimeBucket(TenantTime.Now),
                            IntermediateData = ruleResult.Data,
                            Name = scoreCardDetails.ScoreCardName,
                            ResultDetail = ruleResult.Detail,
                            Result = ruleResult.Result,
                            SourceData = ruleResult.SourceData,
                            ExceptionDetail = ruleResult.Exception
                        };
                        ProductRuleExecutionRepository.Add(executionDetail);
                        if (!string.IsNullOrEmpty(score.DataAttributeStoreName) && (scoringResult != null))
                        {
                            await DataAttributesEngine.SetAttribute(entityType, entityId, score.DataAttributeStoreName, scoringResult);
                        }
                    }
                }

                Logger.Info($"scorecard variable implementation ended {TenantTime.Now}");
            }

            var ruleDetail = await GetRuleDefinitionById(scoreCardDetails.RuleDefinitionId);

            if (ruleDetail == null)
                throw new NotFoundException($"{ruleDetail.RuleName} is not found for {scoreCardDetails.ScoreCardName}-{entityType}-{productId}");

            //  ruleDetail = await CheckDefinitionByDate(entityType, entityId, ruleDetail);

            var allAttributesScoreCard = DataAttributesEngine.GetAllAttributes(entityType, entityId).Result;
            IProductRuleSourceResult ruleExecutionResult = ExceuteRuleScoreCard(entityType, entityId, allAttributesScoreCard, ruleDetail.RuleDetail);
            //  ruleExecutionResult = await ExecuteRule(entityType, entityId, productId, ruleDetail.RuleDetail, ruleDetail.DataAttributeStoreName, null);

            if (!string.IsNullOrEmpty(ruleDetail.DataAttributeStoreName) && (ruleExecutionResult != null && ruleExecutionResult.Data != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDetail.DataAttributeStoreName, ruleExecutionResult.Data);
            }
            var ruleExecutionDetail = new ProductRuleResult
            {
                EntityId = entityId,
                EntityType = entityType,
                ExecutedDate = new TimeBucket(TenantTime.Now),
                IntermediateData = ruleExecutionResult.Data,
                Name = ruleDetail.RuleName,
                ResultDetail = ruleExecutionResult.Detail,
                Result = ruleExecutionResult.Result,
                ConfigurationDetail = ruleDetail,
                SourceData = ruleExecutionResult.SourceData,
                ExceptionDetail = ruleExecutionResult.Exception
            };
            ProductRuleExecutionRepository.Add(ruleExecutionDetail);

            await EventHub.Publish(new ProductRuleExecuted { ProductRuleResult = ruleExecutionDetail });
            Logger.Info($"scorecard implementation ended for {step.ParameterName} and {TenantTime.Now}");
            return ruleExecutionDetail;
        }

        private ProductRuleSourceResult ExceuteRuleScoreCard(string entityType, string entityId, IDictionary<string, object> allAttributes, IRuleDetail ruleDefinition)
        {
            var requiredSources = ruleDefinition.RequiredSources;

            var sources = new List<string>();
            if (requiredSources != null && requiredSources.Any())
            {
                sources.AddRange(requiredSources);
            }

            var optionalSources = ruleDefinition.OptionalSources;
            if (optionalSources != null && optionalSources.Any())
            {
                sources.AddRange(optionalSources);
            }
            if (sources != null && sources.Any())
            {
                var isAllAttributes = requiredSources.All(r => allAttributes.Any(a => string.Equals(r, a.Key, StringComparison.InvariantCultureIgnoreCase)));
                if (!isAllAttributes)
                    throw new InvalidOperationException("Data attributes for all sources are not found");
            }

            Dictionary<string, object> attributes = new Dictionary<string, object>();
            foreach (var value in sources)
            {
                object input = null;
                allAttributes.TryGetValue(Char.ToLowerInvariant(value[0]) + value.Substring(1), out input);
                if (input != null)
                    attributes.Add(Char.ToLowerInvariant(value[0]) + value.Substring(1), input);
            }
            attributes.Add("entityId", entityId);
            attributes.Add("entityType", entityType);
            var executionResult = DecisionEngineService.Execute<dynamic, ProductRuleSourceResult>(
                        ruleDefinition.DERuleName, new { payload = attributes });

            executionResult.SourceData = new Dictionary<string, object>(attributes);
            return executionResult;
        }

        private async Task<ProductRuleResult> RunRuleByIdDetails(string entityType, string entityId, string productId, string ruleDefinationId, IDictionary<string, object> allAttributes)
        {
            IRuleDefinition ruleDetail = await GetRuleDefinitionById(ruleDefinationId);

            if (ruleDetail == null)
                throw new NotFoundException($"{ruleDefinationId} is not found for {entityType}-{productId}");

            ruleDetail = CheckDefinitionByDate(entityType, entityId, ruleDetail, allAttributes);

            IProductRuleSourceResult ruleResult = new ProductRuleSourceResult();
            ruleResult = ExceuteRuleScoreCard(entityType, entityId, allAttributes, ruleDetail.RuleDetail);

            if (!string.IsNullOrEmpty(ruleDetail.DataAttributeStoreName) && (ruleResult != null && ruleResult.Data != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, ruleDetail.DataAttributeStoreName, ruleResult.Data);
            }
            var executionDetail = new ProductRuleResult
            {
                EntityId = entityId,
                EntityType = entityType,
                ExecutedDate = new TimeBucket(TenantTime.Now),
                IntermediateData = ruleResult.Data,
                Name = ruleDetail.RuleName,
                ResultDetail = ruleResult.Detail,
                Result = ruleResult.Result,
                ConfigurationDetail = ruleDetail,
                SourceData = ruleResult.SourceData,
                ExceptionDetail = ruleResult.Exception
            };
            ProductRuleExecutionRepository.Add(executionDetail);
            await EventHub.Publish(new ProductRuleExecuted { ProductRuleResult = executionDetail });
            return executionDetail;
        }
        public async Task<IProductRuleResult> RunSegmentInternalProcess(string entityType, string entityId, string productId, string segmentId, string Weightage = null, string secondaryName = null)
        {
            Logger.Info($"segment execution started {TenantTime.Now}");
            if (string.IsNullOrWhiteSpace(segmentId))
                throw new ArgumentException($"{nameof(segmentId)} is mandatory");
            Logger.Info($"segment GetSegment started {TenantTime.Now}");
            ISegment segmentDetail = await GetSegment(segmentId);
            Logger.Info($"segment variable GetSegment ended {TenantTime.Now}");

            if (segmentDetail == null)
                throw new NotFoundException($"{segmentId} is not found for {entityType}-{productId}");

            SegmentationResult segmentResult = null;
            IProductRuleResult executionDetail = new ProductRuleResult
            {
                EntityType = entityType,
                EntityId = entityId,
                ExecutedDate = new TimeBucket(TenantTime.Now)
            };

            IDictionary<string, object> allAttributes = new Dictionary<string, object>();
            Logger.Info($"segment GetAllAttributes started {TenantTime.Now}");
            allAttributes = DataAttributesEngine.GetAllAttributes(entityType, entityId, secondaryName).Result;
            Logger.Info($"segment GetAllAttributes started {TenantTime.Now}");
            segmentDetail = CheckDefinitionByDate(entityType, entityId, segmentDetail, allAttributes);
            if (segmentDetail.SegmentType == SegmentType.Rule)
            {
                executionDetail = await RunRuleByIdDetails(entityType, entityId, productId, segmentDetail.RuleDefinitionId, allAttributes);
            }
            else
            {
                var segmentVariableSource = segmentDetail.SegmentVariableSource;

                if (segmentVariableSource == null)
                    throw new NotFoundException($"No segment variable found for {segmentDetail.SegmentName} for {entityType}-{productId}");

                Dictionary<string, object> attributes = new Dictionary<string, object>();

                object inputValue = null;
                allAttributes.TryGetValue(Char.ToLowerInvariant(segmentVariableSource.InputSource[0]) + segmentVariableSource.InputSource.Substring(1), out inputValue);
                if (inputValue != null)
                    attributes.Add(Char.ToLowerInvariant(segmentVariableSource.InputSource[0]) + segmentVariableSource.InputSource.Substring(1), inputValue);

                var input = GetResultValue(attributes.FirstOrDefault().Value, segmentVariableSource.InputVariable);

                if (segmentDetail.SegmentType == SegmentType.Range)
                {
                    double segmentInput = 0.0;
                    double.TryParse(input, out segmentInput);
                    if (segmentInput >= 0)
                    {
                        if (segmentDetail.Data == null)
                            throw new NotFoundException($"No data found for {segmentDetail.SegmentName}  for {entityId}-{entityType}-{productId}");

                        var rangeList = JsonConvert.DeserializeObject<List<SegmentationRange>>(segmentDetail.Data.ToString());
                        SegmentationRange range = null;
                        if (rangeList.Any(x => x.LowerLimit == null))
                        {
                            var rangeTempList = rangeList.Where(x => x.LowerLimit == null).ToList();
                            range = rangeTempList.Where(x => x.UpperLimit >= segmentInput).FirstOrDefault();
                        }
                        if (rangeList.Any(x => x.UpperLimit == null))
                        {
                            var rangeTempList = rangeList.Where(x => x.UpperLimit == null).ToList();
                            range = rangeTempList.Where(x => x.LowerLimit <= segmentInput).FirstOrDefault();
                        }
                        if (range == null)
                            range = rangeList.Where(x => x.LowerLimit <= segmentInput && x.UpperLimit >= segmentInput).FirstOrDefault();

                        if (range == null)
                            throw new NotFoundException($"No segmentation found for {segmentVariableSource.InputVariable}");

                        string segmentLabel = range.SegmentLabel;
                        segmentResult = new SegmentationResult
                        {
                            key = segmentVariableSource.InputVariable,
                            value = Weightage != null && Convert.ToDouble(Weightage) > 0 ? (Convert.ToDouble(segmentLabel) * Convert.ToDouble(Weightage)).ToString() : segmentLabel
                        };
                        Dictionary<string, object> data = new Dictionary<string, object>();
                        data.Add(segmentDetail.SegmentName, segmentResult);
                        executionDetail.ExceptionDetail = null;
                        executionDetail.IntermediateData = data;
                        executionDetail.Result = Result.Passed;
                        executionDetail.ResultDetail = null;
                        executionDetail.SourceData = attributes;
                        executionDetail.Name = segmentDetail.SegmentName;
                    }
                }
                else if (segmentDetail.SegmentType == SegmentType.List)
                {
                    string segmentLabel = string.Empty;
                    if (!string.IsNullOrWhiteSpace(input))
                    {
                        if (segmentDetail.Data == null)
                            throw new NotFoundException($"No data found for {segmentDetail.SegmentName}  for {entityId}-{entityType}-{productId}");

                        var list = JsonConvert.DeserializeObject<List<SegmentationList>>(segmentDetail.Data.ToString());
                        foreach (var lst in list)
                        {
                            if (lst.InListValues != null && lst.InListValues.Any())
                            {
                                if (lst.InListValues.Contains(input))
                                {
                                    segmentLabel = lst.SegmentLabel;
                                    break;
                                }
                            }
                            else if (lst.ExceptListValues != null && lst.ExceptListValues.Any())
                            {
                                if (lst.ExceptListValues.Contains(input))
                                {
                                    segmentLabel = lst.SegmentLabel;
                                    break;
                                }
                            }
                        }
                        if (string.IsNullOrWhiteSpace(segmentLabel))
                            throw new NotFoundException($"No segmentation found for {segmentVariableSource.InputVariable}");

                        segmentResult = new SegmentationResult
                        {
                            key = segmentVariableSource.InputVariable,
                            value = Weightage != null && Convert.ToDouble(Weightage) > 0 ? (Convert.ToDouble(segmentLabel) * Convert.ToDouble(Weightage)).ToString() : segmentLabel
                        };
                        Dictionary<string, object> data = new Dictionary<string, object>();
                        data.Add(segmentDetail.SegmentName, segmentResult);
                        executionDetail.ExceptionDetail = null;
                        executionDetail.IntermediateData = data;
                        executionDetail.Result = Result.Passed;
                        executionDetail.ResultDetail = null;
                        executionDetail.SourceData = attributes;
                        executionDetail.Name = segmentDetail.SegmentName;
                    }
                }
            }

            ProductRuleExecutionRepository.Add(executionDetail);
            Logger.Info($"segment SetAttribute started {TenantTime.Now}");
            if (!string.IsNullOrEmpty(segmentDetail.DataAttributeStoreName) && (segmentResult != null))
            {
                await DataAttributesEngine.SetAttribute(entityType, entityId, segmentDetail.DataAttributeStoreName, segmentResult);
            }
            Logger.Info($"segment SetAttribute started {TenantTime.Now}");

            Logger.Info($"segment execution ended {TenantTime.Now}");
            return executionDetail;
        }

    }

    public class SegmentationResult
    {
        public string key { get; set; }

        public string value { get; set; }
    }
    public class ScoringResult
    {
        public string VariableName { get; set; }

        public string ScoringValue { get; set; }

        public string WeightedScore { get; set; }
    }






}





