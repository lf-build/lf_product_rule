﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.ProductRule
{
    public static class ProductRuleListnerExtentions
    {
        public static void UseProductRuleListner(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IProductRuleListner>().Start();
        }
    }
}
