﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace LendFoundry.ProductRule
{
    public class FaultRetry
    {
        private readonly List<Exception> _errorList = new List<Exception>();

        public enum Status
        {
            Unknown,
            Success,
            FailedUnrecoverable,
            FailedAttemptsExceeded
        }
        public int MaxRetries
        {
            get;
        }
        public int SleepSeconds
        {
            get;
        }
        public int AttemptsRequired
        {
            get;
            private set;
        }
        public Status ExecutionStatus
        {
            get;
            private set;
        }
        public ReadOnlyCollection<Exception> Errors
        {
            get;
            private set;
        }

        public FaultRetry(int maxRetries, int sleepSeconds)
        {
            if (maxRetries < 1)
            {
                throw new InvalidOperationException("Invalid retry count");
            }
            MaxRetries = maxRetries;
            SleepSeconds = sleepSeconds;
            Errors = new ReadOnlyCollection<Exception>(_errorList);
        }

        public void Run(Action action, IRecoverabilityPolicy recoverabilityPolicy)
        {
            if (recoverabilityPolicy == null)
            {
                throw new ArgumentNullException(nameof(recoverabilityPolicy));
            }
            ExecutionStatus = Status.Unknown;
            AttemptsRequired = 0;
            _errorList.Clear();

            while (true)
            {
                try
                {
                    AttemptsRequired++;
                    action();
                    ExecutionStatus = Status.Success;
                    return;
                }

                catch (Exception ex)
                {
                    _errorList.Add(ex);
                    if (!recoverabilityPolicy.CanBeRecoveredFrom(ex))
                    {
                        ExecutionStatus = Status.FailedUnrecoverable;
                        throw;
                    }
                    if (AttemptsRequired >= MaxRetries)
                    {
                        ExecutionStatus = Status.FailedAttemptsExceeded;
                        throw;
                    }
                    Thread.Sleep(SleepSeconds * 1000);
                }
            }
        }

        public static void RunWithAlwaysRetry(Action action, int maxRetry = 3, int sleepSeconds = 3)
        {
            var faultRetry = new FaultRetry(maxRetry, sleepSeconds);
            faultRetry.Run(action, new AlwaysRetryPolicy());
        }
    }
}
