﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.ProductRule.Client
{
    public static class ProductRuleServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddProductRuleService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IProductRuleServiceClientFactory>(p => new ProductRuleServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IProductRuleServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddProductRuleService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IProductRuleServiceClientFactory>(p => new ProductRuleServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IProductRuleServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddProductRuleService(this IServiceCollection services)
        {
            services.AddTransient<IProductRuleServiceClientFactory>(p => new ProductRuleServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IProductRuleServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
    
}
