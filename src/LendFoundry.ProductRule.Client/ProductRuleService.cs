﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.ProductRule.Configurations;
using System;
using Newtonsoft.Json;

namespace LendFoundry.ProductRule.Client
{
    /// <summary>
    /// ProductRule Service class
    /// </summary>
    public class ProductRuleService : IProductRuleService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRuleService"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public ProductRuleService(IServiceClient client)
        {
            Client = client;
        }

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>
        /// The client.
        /// </value>
        private IServiceClient Client { get; }

        //public async Task<ProductRuleResult> RunRule(string entityType, string entityId, string productId, string ruleName, Dictionary<string, string> referenceNumbers = null)
        //{
        //    var request = new RestRequest("{entityType}/{entityId}/{productId}/{ruleName}/rule", Method.POST);
        //    request.AddUrlSegment("entityType", entityType);
        //    request.AddUrlSegment("entityId", entityId);
        //    request.AddUrlSegment("productId", productId);
        //    request.AddUrlSegment("ruleName", ruleName);
        //    return await Client.ExecuteAsync<ProductRuleResult>(request);
        //}

        public async Task<ILookup> AddLookUp(string entityType, string productId, IAddLookupRequest lookupRequest)
        {
            var request = new RestRequest("{entityType}/{productId}/lookup", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(lookupRequest);
            return await Client.ExecuteAsync<Lookup>(request);
        }

        public async Task<IPricingStrategy> AddPricingStrategy(string entityType, string productId, IPricingStrategyRequest pricingStrategyRequest)
        {
            var request = new RestRequest("{entityType}/{productId}/pricingstrategy", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(pricingStrategyRequest);
            return await Client.ExecuteAsync<PricingStrategy>(request);
        }

        public async Task<IPricingStrategy> AddPricingStrategyStep(string pricingStrategyId, IPricingStrategyStepRequest pricingStrategyStepRequest)
        {
            var request = new RestRequest("{pricingStrategyId}/pricingstrategystep", Method.POST);
            request.AddUrlSegment("pricingStrategyId", pricingStrategyId);
            request.AddJsonBody(pricingStrategyStepRequest);
            return await Client.ExecuteAsync<PricingStrategy>(request);
        }

        public async Task<IRuleDefinition> AddRuleDefinition(string entityType, string productId, IRuleDefinitionAddRequest addRuleRequest)
        {
            var request = new RestRequest("{entityType}/{productId}/ruledefinition", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(addRuleRequest);
            return await Client.ExecuteAsync<RuleDefinition>(request);
        }

        public async Task<IScoreCard> AddScoreCard(string entityType, string productId, IScoreCardRequest scoreCardRequest)
        {
            var request = new RestRequest("{entityType}/{productId}/scorecard", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(scoreCardRequest);
            return await Client.ExecuteAsync<ScoreCard>(request);
        }

        public async Task<IScoreCard> AddScoreCardVariable(string scoreCardId, IScoreCardVariableRequest scoreCardVariableRequest)
        {
            var request = new RestRequest("{scoreCardId}/scorecardvariable", Method.POST);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            request.AddJsonBody(scoreCardVariableRequest);
            return await Client.ExecuteAsync<ScoreCard>(request);
        }

        public async Task<ISegment> AddSegment(string entityType, string productId, ISegmentRequest segmentRequest)
        {
            var request = new RestRequest("{entityType}/{productId}/segment", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(segmentRequest);
            return await Client.ExecuteAsync<Segment>(request);
        }

        public async Task DeleteLookUp(string lookupId)
        {
            var request = new RestRequest("{lookupId}/lookup", Method.DELETE);
            request.AddUrlSegment("lookupId", lookupId);
            await Client.ExecuteAsync(request);
        }

        public async Task DeletePricingStrategy(string pricingStrategyId)
        {
            var request = new RestRequest("{pricingStrategyId}/pricingstrategy", Method.DELETE);
            request.AddUrlSegment("pricingStrategyId", pricingStrategyId);
            await Client.ExecuteAsync(request);
        }
        public async Task DeletePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId)
        {
            var request = new RestRequest("{pricingStrategyId}/{pricingStrategyStepId}/pricingstrategystep", Method.DELETE);
            request.AddUrlSegment("pricingStrategyId", pricingStrategyId);
            request.AddUrlSegment("pricingStrategyStepId", pricingStrategyStepId);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteRule(string ruleId)
        {
            var request = new RestRequest("{ruleId}/ruledefinition", Method.DELETE);
            request.AddUrlSegment("ruleId", ruleId);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteScoreCard(string scoreCardId)
        {
            var request = new RestRequest("{scoreCardId}/scorecard", Method.DELETE);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            await Client.ExecuteAsync(request);

        }

        public async Task DeleteScoreCardVariable(string scoreCardId, string scoreCardVariableId)
        {
            var request = new RestRequest("{scoreCardId}/{scoreCardVariableId}/scorecardvariable", Method.DELETE);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            request.AddUrlSegment("scoreCardVariableId", scoreCardVariableId);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteSegment(string segmentId)
        {
            var request = new RestRequest("{segmentId}/segment", Method.DELETE);
            request.AddUrlSegment("segmentId", segmentId);
            await Client.ExecuteAsync(request);
        }

        public async Task<List<ILookup>> GetAllLookup(string entityType)
        {
            var request = new RestRequest("{entityType}/all/lookup", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var result =await Client.ExecuteAsync<List<Lookup>>(request);
            return new List<ILookup>(result);
        }

        public async Task<List<IRuleDefinition>> GetAllRuleDefinition(string entityType, string productId, string ruleType)
        {
            var request = new RestRequest("{entityType}/{productId}/{ruleType}/ruledefinitions", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("ruleType", ruleType);
            var result = await Client.ExecuteAsync<List<RuleDefinition>>(request);
            return new List<IRuleDefinition>(result);
        }

        public async Task<List<IScoreCard>> GetAllScoreCard(string entityType)
        {
            var request = new RestRequest("{entityType}/all/scorecard", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var result = await Client.ExecuteAsync<List<ScoreCard>>(request);
            return new List<IScoreCard>(result);
        }

        public async Task<List<ISegment>> GetAllSegment(string entityType)
        {
            var request = new RestRequest("{entityType}/all/segment", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var result =await Client.ExecuteAsync<List<Segment>>(request);
            return new List<ISegment>(result);
        }

        public async Task<IScoreCardVariable> GetCardVariable(string scoreCardId, string scoreCardVariableId)
        {
            var request = new RestRequest("{scoreCardId}/{scoreCardVariableId}/scorecardvariable", Method.GET);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            request.AddUrlSegment("scoreCardVariableId", scoreCardVariableId);
            return await Client.ExecuteAsync<ScoreCardVariable>(request);

        }

        public async Task<ILookup> GetLookUp(string lookupId)
        {
            var request = new RestRequest("{lookupId}/lookup", Method.GET);
            request.AddUrlSegment("lookupId", lookupId);
            return await Client.ExecuteAsync<Lookup>(request);
        }


        public async Task<ILookup> GetLookupByName(string entityType, string productId, string name)
        {

            var request = new RestRequest("{entityType}/{productId}/{name}/lookup", Method.GET);
            request.AddUrlSegment("entityType", entityType);   
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("name", name);

            return await Client.ExecuteAsync<Lookup>(request);
            
        }

        public async Task<IPricingStrategy> GetPricingStrategy(string pricingStrategyId)
        {
            var request = new RestRequest("{pricingStrategyId}/pricingstrategy", Method.GET);
            request.AddUrlSegment("pricingStrategyId", pricingStrategyId);
            
            return await Client.ExecuteAsync<PricingStrategy>(request);
        }

        public async Task<IPricingStrategyStep> GetPricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId)
        {
            var request = new RestRequest("{pricingStrategyId}/{pricingStrategyStepId}/pricingstrategystep", Method.GET);
            request.AddUrlSegment("pricingStrategyId", pricingStrategyId);
            request.AddUrlSegment("pricingStrategyStepId", pricingStrategyStepId);

            return await Client.ExecuteAsync<PricingStrategyStep>(request);
        }

        public async Task<List<IProductRuleResult>> GetProductRuleResultDetail(string entityType, string entityId)
        {

            var request = new RestRequest("{entityType}/{entityId}", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);

            return await Client.ExecuteAsync<List<IProductRuleResult>>(request);
        }

        public async Task<IRuleDefinition> GetRuleDefinitionById(string ruleDefinitionId)
        {
            var request = new RestRequest("{ruleDefinitionId}/ruledefinition", Method.GET);
            request.AddUrlSegment("ruleDefinitionId", ruleDefinitionId);
           
            return await Client.ExecuteAsync<RuleDefinition>(request);
        }

        public async Task<IRuleDefinition> GetRuleDefinitionByName(string entityType, string productId, string ruleName)
        {
            var request = new RestRequest("{entityType}/{productId}/{ruleName}/ruledefinition", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("ruleName", ruleName);

            return await Client.ExecuteAsync<RuleDefinition>(request);
        }

        public async Task<List<IRuleDefinition>> GetRuleDefinitionDetails(string entityType, string productId, string className)
        {
            var request = new RestRequest("{entityType}/ruledetails/{className?}/{productId?}", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            if(productId != null)
            request.AddUrlSegment("productId", productId);
            if(className != null)
            request.AddUrlSegment("className", className);
            var result = await Client.ExecuteAsync<List<RuleDefinition>>(request);
            return new List<IRuleDefinition>(result);
        }

        public async Task<IScoreCard> GetScoreCard(string scoreCardId)
        {
            var request = new RestRequest("{scoreCardId}/scorecard", Method.GET);
            request.AddUrlSegment("scoreCardId", scoreCardId);          
            return await Client.ExecuteAsync<ScoreCard>(request);
        }

        public async Task<IScoreCard> GetScoreCardByName(string entityType, string productId, string name)
        {

            var request = new RestRequest("{entityType}/{productId}/{name}/scorecard", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("name", name);

            return await Client.ExecuteAsync<ScoreCard>(request);
        }

        public async Task<ISegment> GetSegment(string segmentId)
        {
            var request = new RestRequest("{segmentId}/segment", Method.GET);
            request.AddUrlSegment("segmentId", segmentId);           
            return await Client.ExecuteAsync<Segment>(request);
        }

        public async Task<ISegment> GetSegmentByName(string entityType, string productId, string name)
        {
            var request = new RestRequest("{entityType}/{productId}/{name}/segment", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("name", name);

            return await Client.ExecuteAsync<Segment>(request);
        }

        public async Task<IProductRuleResult> RunLookup(string entityType, string entityId, string productId, string lookupId)
        {
            var request = new RestRequest("{entityType}/{entityId}/{productId}/{lookupId}/lookup", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("lookupId", lookupId);          

            return await Client.ExecuteAsync<ProductRuleResult>(request);
        }

        public async Task<ProductRuleResult> RunRule(string entityType, string entityId, string productId, string ruleName, object eventData, string secondaryName = null)
        {
            string uri = string.Empty;
            if (string.IsNullOrEmpty(secondaryName))
                uri = "{entityType}/{entityId}/{productId}/{ruleName}/ruledetails";
            else
                uri = "{entityType}/{entityId}/{productId}/{ruleName}/ruledetails/{secondaryName}";
            var request = new RestRequest(uri, Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("ruleName", ruleName);
            if (!string.IsNullOrEmpty(secondaryName)) request.AddUrlSegment("secondaryName", secondaryName);
            var json = JsonConvert.SerializeObject(eventData);
            request.AddParameter("application/json", json, ParameterType.RequestBody);

            return await Client.ExecuteAsync<ProductRuleResult>(request);
        }


        public async Task<ProductRuleResult> RunRule(string entityType, string entityId, string productId, string ruleName, string secondaryName = null)
        {
            string uri = string.Empty;
            if (secondaryName == null)
                uri = "{entityType}/{entityId}/{productId}/{ruleName}/rule";
            else
                uri = "{entityType}/{entityId}/{productId}/{ruleName}/rule/{secondaryName}";
            var request = new RestRequest(uri, Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("ruleName", ruleName);
            if (!string.IsNullOrEmpty(secondaryName)) request.AddUrlSegment("secondaryName", secondaryName);

            return await Client.ExecuteAsync<ProductRuleResult>(request);
        }


        public async Task<IProductRuleResult> RunScoreCard(string entityType, string entityId, string productId, string scoreCardId, Dictionary<string, string> referenceNumbers = null)
        {
            var request = new RestRequest("{entityType}/{entityId}/{productId}/{scoreCardId}/scorecard", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            if (referenceNumbers != null)
                request.AddJsonBody(referenceNumbers);

            return await Client.ExecuteAsync<ProductRuleResult>(request);
        }
    
        public async Task<IProductRuleResult> RunSegment(string entityType, string entityId, string productId, string segmentId,string secondaryName = null)
        {
            string uri = string.Empty;
            if (secondaryName == null)
                uri = "{entityType}/{entityId}/{productId}/{segmentId}/segment";
            else
                uri = "{entityType}/{entityId}/{productId}/{segmentId}/{secondaryName}/segment";

            var request = new RestRequest(uri, Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("segmentId", segmentId);          

            return await Client.ExecuteAsync<ProductRuleResult>(request);
        }

        public async Task<IProductRuleResult> RunPricingStrategy(string entityType, string entityId, string productId, string pricingStrategyName, string version, string secondaryName = null)
        {
            string uri = string.Empty;
            if (secondaryName == null)
                uri = "{entityType}/{entityId}/{productId}/{pricingStrategyName}/{version}/pricingstrategy";
            else
                uri = "{entityType}/{entityId}/{productId}/{pricingStrategyName}/{version}/pricingstrategy/{secondaryName}";

            var request = new RestRequest(uri, Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("pricingStrategyName", pricingStrategyName);
            request.AddUrlSegment("version", version);
            if (!string.IsNullOrEmpty(secondaryName)) request.AddUrlSegment("secondaryName", secondaryName);
            return await Client.ExecuteAsync<ProductRuleResult>(request);
        }
        public async Task<ILookup> UpdateLookUp(string lookupId, IUpdateLookupRequest updateLookupRequest)
        {
            var request = new RestRequest("{lookupId}/lookup", Method.PUT);
            request.AddUrlSegment("lookupId", lookupId);
            
            request.AddJsonBody(updateLookupRequest);
            return await Client.ExecuteAsync<Lookup>(request);
        }

        public Task UpdatePricingStrategy(string pricingStrategyId, IPricingStrategyRequest pricingStrategyRequest)
        {
            var request = new RestRequest("{pricingStrategyId}/pricingstrategy", Method.PUT);
            request.AddUrlSegment("pricingStrategyId", pricingStrategyId);
            request.AddJsonBody(pricingStrategyRequest);
            return Client.ExecuteAsync(request);
        }

        public Task UpdatePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId, IPricingStrategyStepRequest pricingStrategyStepRequest)
        {
            var request = new RestRequest("{pricingStrategyId}/{pricingStrategyStepId}/pricingstrategystep", Method.PUT);
            request.AddUrlSegment("pricingStrategyId", pricingStrategyId);
            request.AddUrlSegment("pricingStrategyStepId", pricingStrategyStepId);

            request.AddJsonBody(pricingStrategyStepRequest);
            return Client.ExecuteAsync(request);
        }

        public async Task<IRuleDefinition> UpdateRuleDefinition(string ruleId, IRuleDefinitionUpdateRequest updateRuleRequest)
        {
            var request = new RestRequest("{ruleId}/ruledefinition", Method.PUT);
            request.AddUrlSegment("ruleId", ruleId);
            request.AddJsonBody(updateRuleRequest);
            return await Client.ExecuteAsync<RuleDefinition>(request);
        }

        public Task UpdateScoreCard(string scoreCardId, IScoreCardRequest scoreCardRequest)
        {
            var request = new RestRequest("{scoreCardId}/scorecard", Method.PUT);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            request.AddJsonBody(scoreCardRequest);
            return Client.ExecuteAsync(request);
        }

        public Task UpdateScoreCardVariable(string scoreCardId, string scoreCardVariableId, IScoreCardVariableRequest scoreCardVariableRequest)
        {
            var request = new RestRequest("{scoreCardId}/{scoreCardVariableId}/scorecardvariable", Method.PUT);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            request.AddUrlSegment("scoreCardVariableId", scoreCardVariableId);

            request.AddJsonBody(scoreCardVariableRequest);
            return Client.ExecuteAsync(request);
        }

        public Task UpdateSegment(string segmentId, ISegmentRequest segmentRequest)
        {
            var request = new RestRequest("{segmentId}/segment", Method.PUT);
            request.AddUrlSegment("segmentId", segmentId);
            request.AddJsonBody(segmentRequest);
            return Client.ExecuteAsync(request);
        }

        public async Task<ILookup> UploadLookupData(string lookupId, byte[] file)
        {
            var request = new RestRequest("{lookupId}/upload", Method.POST);
            request.AddUrlSegment("lookupId", lookupId);
            request.AddJsonBody(file);
            return await Client.ExecuteAsync<Lookup>(request);
        }

        public async Task<List<IStrategyHistory>> GetStrategyHistory(string entityType,string entityId,string productId,string strategyId)
        {
            var request = new RestRequest("{entityType}/all/scorecard", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var result = await Client.ExecuteAsync<List<StrategyHistory>>(request);
            return new List<IStrategyHistory>(result);
        }

        public async Task<IProductRuleSourceResult> ValidateScoreCard(string entityType, string productId, string scoreCardId)
        {
            var request = new RestRequest("{entityType}/{productId}/{scoreCardId}/validatescorecard", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("scoreCardId", scoreCardId);

            return await Client.ExecuteAsync<ProductRuleSourceResult>(request);
        }

        public async Task<List<IPricingStrategy>> GetAllPricingStrategy(string entityType)
        {
            var request = new RestRequest("{entityType}/all/pricingstrategy", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var result = await Client.ExecuteAsync<List<PricingStrategy>>(request);
            return new List<IPricingStrategy>(result);
        }

        public async Task<List<ITemplate>> GetAllTemplate(string entityType)
        {
            var request = new RestRequest("{entityType}/all/template", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            var result = await Client.ExecuteAsync<List<Template>>(request);
            return new List<ITemplate>(result);
        }

        public async Task<ITemplate> GetByEventName(string entityType, string productId, string eventName)
        {
            var request = new RestRequest("{entityType}/{productId}/{name}/template", Method.GET);
            request.AddUrlSegment("entityType", entityType);            
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("name", eventName);
            return await Client.ExecuteAsync<Template>(request);
        }

        public async Task<ITemplate> AddTemplate(string entityType, string productId, ITemplateAddRequest templateAddRequest)
        {
            var request = new RestRequest("{entityType}/{productId}/template", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            request.AddJsonBody(templateAddRequest);
            return await Client.ExecuteAsync<Template>(request);
        }

        public async Task UpdateTemplate(string templateId, ITemplateAddRequest templateAddRequest)
        {
            var request = new RestRequest("{templateId}/template", Method.PUT);
            request.AddUrlSegment("templateId", templateId);
            request.AddJsonBody(templateAddRequest);
            await Client.ExecuteAsync(request);
        }
    }
}
