﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.ProductRule.Client
{
    public class ProductRuleServiceClientFactory : IProductRuleServiceClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public ProductRuleServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public ProductRuleServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public IProductRuleService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("product_rule");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new ProductRuleService(client);
        }

       
    }
}
