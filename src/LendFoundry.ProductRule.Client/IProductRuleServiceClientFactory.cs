﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.ProductRule.Client
{
    /// <summary>
    /// ProductRule Service Client Factory interface
    /// </summary>
    public interface IProductRuleServiceClientFactory
    {
        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        IProductRuleService Create(ITokenReader reader);
    }
}
