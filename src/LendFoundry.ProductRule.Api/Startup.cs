﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.ProductRule.Configurations;
using LendFoundry.ProductRule.Persistence;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Configuration;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.ProductRule.Api
{
    internal class Startup
    {

        public void ConfigureServices(IServiceCollection services)
        {
			#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "ProductRule"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.ProductRule.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif


            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddConfigurationService<ProductRuleConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<ProductRuleConfiguration>(Settings.ServiceName);
            services.AddLookupService();
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<ProductRuleConfiguration>>().Get()); 
            services.AddDecisionEngine();
            services.AddDataAttributes();

            services.AddTransient<IProductRuleService, ProductRuleService>();
            services.AddTransient<IProductRuleExecutionRepository, ProductRuleExecutionRepository>();           
            services.AddTransient<IProductRuleLookupRepository, ProductRuleLookupRepository>();
            services.AddTransient<IProductRuleScoreCardRepository, ProductRuleScoreCardRepository>();
            services.AddTransient<IProductRuleScoreCardRepositoryFactory, ProductRuleScoreCardRepositoryFactory>();
            services.AddTransient<IProductRuleSegmentRepository, ProductRuleSegmentRepository>();
            services.AddTransient<IProductRuleSegmentRepositoryFactory, ProductRuleSegmentRepositoryFactory>();
            services.AddTransient<IProductRuleServiceFactory, ProductRuleServiceFactory>();
            services.AddTransient<IProductRuleExecutionRepositoryFactory, ProductRuleExecutionRepositoryFactory>();
            services.AddTransient<IProductRuleListner, ProductRuleListner>();
            services.AddTransient<IProductRuleLookupRepositoryFactory, ProductRuleLookupRepositoryFactory>();
            services.AddTransient<IRuleDefinitionRepositoryFactory, RuleDefinitionRepositoryFactory>();
            services.AddTransient<IProductRuleConfigurationRepositoryFactory, ProductRuleConfigurationRepositoryFactory>();
            services.AddTransient<IRuleDefinitionRepository, RuleDefinitionRepository>();
            services.AddTransient<IPricingStrategyRepository, PricingStrategyRepository>();
            services.AddTransient<IPricingStrategyRepositoryFactory, PricingStrategyRepositoryFactory>();
            services.AddTransient<IStrategyExceutionHistoryRepository, StrategyExceutionHistoryRepository>();                     
            services.AddTransient<IStrategyExceutionHistoryRepositoryFactory, StrategyExceutionHistoryRepositoryFactory>();
            services.AddTransient<ITemplateRepository, TemplateRepository>();
            services.AddTransient<ITemplateRepositoryFactory, TemplateRepositoryFactory>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "ProductRule Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseProductRuleListner();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}
