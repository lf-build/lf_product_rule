﻿#if DOTNET2

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;

namespace LendFoundry.ProductRule.Api
{
    /// <summary>
    /// entrypoint
    /// </summary>
    public class Program
    {
        /// <summary>
        /// entrypoint
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }

        }


    }
}

#endif