﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Net.Http.Headers;
#endif

namespace LendFoundry.ProductRule.Api.Controllers
{
    /// <summary>
    /// Represents product rule controller class.
    /// </summary>
    [Route("/")]
    public class ProductRuleController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="productRuleService"></param>
        public ProductRuleController(IProductRuleService productRuleService)
        {
            if (productRuleService == null)
                throw new ArgumentNullException(nameof(productRuleService));

            ProductRuleService = productRuleService;
        }

        private IProductRuleService ProductRuleService { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();


        #region Rule Execution
        /// <summary>
        ///  Execute with run rule on specify by product id with  rule name.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="productId"></param>
        /// <param name="ruleName"></param>
        /// <param name="secondaryName"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{productId}/{ruleName}/rule/{secondaryName?}")]
        [ProducesResponseType(typeof(ProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]      
        public async Task<IActionResult> RunRule(string entityType, string entityId, string productId, string ruleName,string secondaryName)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.RunRule(entityType, entityId, productId, ruleName, secondaryName)));
        }

        /// <summary>
        ///  Execute with run rule on specify by product id with rule name and event data as body params. 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="productId"></param>
        /// <param name="ruleName"></param>
        /// <param name="body"></param>
        /// <param name="secondaryName"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{productId}/{ruleName}/ruledetails/{secondaryName?}")]
        [ProducesResponseType(typeof(ProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
       
        public async Task<IActionResult> RunRule(string entityType, string entityId, string productId, string ruleName, [FromBody]object body, string secondaryName)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.RunRule(entityType, entityId, productId, ruleName, body, secondaryName)));
        }

        /// <summary>
        ///  Run score card on specify by product id with score card id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="productId"></param>
        /// <param name="scoreCardId"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{productId}/{scoreCardId}/scorecard")]
        [ProducesResponseType(typeof(IProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
       
        public async Task<IActionResult> RunScoreCard(string entityType, string entityId, string productId, string scoreCardId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.RunScoreCard(entityType, entityId, productId, scoreCardId)));
        }


        /// <summary>
        ///  Run segment on specify by product id with segment id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="productId"></param>
        /// <param name="segmentId"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{productId}/{segmentId}/segment")]
        [ProducesResponseType(typeof(IProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> RunSegment(string entityType, string entityId, string productId, string segmentId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.RunSegment(entityType, entityId, productId, segmentId)));
        }

        /// <summary>
        ///  Run lookup on specify by product id with lookup id. 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="productId"></param>
        /// <param name="lookupId"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{productId}/{lookupId}/lookup")]
        [ProducesResponseType(typeof(IProductRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> RunLookup(string entityType, string entityId, string productId, string lookupId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.RunLookup(entityType, entityId, productId, lookupId)));
        }

        /// <summary>
        ///  Upload lookup data on specify by lookup id.
        /// </summary>
        /// <param name="lookupId"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("{lookupId}/upload")]
        [ProducesResponseType(typeof(ILookup), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UploadFile(string lookupId, IFormFile file)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");
            var fileName = ContentDispositionHeaderValue
                       .Parse(file.ContentDisposition)
                       .FileName
                       .Trim('"');

            return await ExecuteAsync(async () =>
                Ok(await ProductRuleService.UploadLookupData(lookupId, file.OpenReadStream().ReadAsBytes())));
        }
        #endregion

        #region scorecard
        /// <summary>
        ///  Add score card on specify by entity type, productId and score card request.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="scoreCardRequestModel"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{productId}/scorecard")]
        [ProducesResponseType(typeof(IScoreCard), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        
        public async Task<IActionResult> AddScoreCard(string entityType, string productId, [FromBody]ScoreCardRequest scoreCardRequestModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.AddScoreCard(entityType, productId, scoreCardRequestModel)));
        }

        /// <summary>
        /// Get score card detail by id.
        /// </summary>
        /// <param name="scoreCardId"></param>
        /// <returns></returns>
        [HttpGet("{scoreCardId}/scorecard")]
        [ProducesResponseType(typeof(IScoreCard), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetScoreCard(string scoreCardId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.GetScoreCard(scoreCardId)));
        }

        /// <summary>
        ///  Update score card by id.
        /// </summary>
        /// <param name="scoreCardId"></param>
        /// <param name="scoreCardRequestModel"></param>
        /// <returns></returns>
        [HttpPut("{scoreCardId}/scorecard")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateScoreCard(string scoreCardId, [FromBody]ScoreCardRequest scoreCardRequestModel)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.UpdateScoreCard(scoreCardId, scoreCardRequestModel); return NoContentResult; });
        }

        /// <summary>
        /// Delete score card by id.
        /// </summary>
        /// <param name="scoreCardId"></param>
        /// <returns></returns>
        [HttpDelete("{scoreCardId}/scorecard")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteScoreCard(string scoreCardId)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.DeleteScoreCard(scoreCardId); return NoContentResult; });
        }
        /// <summary>
        ///  Add score card variable by id.
        /// </summary>
        /// <param name="scoreCardId"></param>
        /// <param name="scoreCardVariableRequest"></param>
        /// <returns></returns>
        [HttpPost("{scoreCardId}/scorecardvariable")]
        [ProducesResponseType(typeof(IScoreCard),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]       
        public async Task<IActionResult> AddScoreCardVariable(string scoreCardId, [FromBody] ScoreCardVariableRequest scoreCardVariableRequest)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.AddScoreCardVariable(scoreCardId, scoreCardVariableRequest)));
        }

        /// <summary>
        /// Get score card variable by score card id and variable id.
        /// </summary>
        /// <param name="scoreCardId"></param>
        /// <param name="scoreCardVariableId"></param>
        /// <returns></returns>
        [HttpGet("{scoreCardId}/{scoreCardVariableId}/scorecardvariable")]
        [ProducesResponseType(typeof(IScoreCardVariable), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
        public async Task<IActionResult> GetScoreCardVariable(string scoreCardId, string scoreCardVariableId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.GetCardVariable(scoreCardId, scoreCardVariableId)));
        }

        /// <summary>
        /// Update score card variable by score card id and variable id.
        /// </summary>
        /// <param name="scoreCardId"></param>
        /// <param name="scoreCardVariableId"></param>
        /// <param name="scoreCardVariableRequest"></param>
        /// <returns></returns>
        [HttpPut("{scoreCardId}/{scoreCardVariableId}/scorecardvariable")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateScoreCardVariable(string scoreCardId, string scoreCardVariableId, [FromBody] ScoreCardVariableRequest scoreCardVariableRequest)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.UpdateScoreCardVariable(scoreCardId, scoreCardVariableId, scoreCardVariableRequest); return NoContentResult; });
        }

        /// <summary>
        /// Delete score card variable by score card id and variable id.
        /// </summary>
        /// <param name="scoreCardId"></param>
        /// <param name="scoreCardVariableId"></param>
        /// <returns></returns>
        [HttpDelete("{scoreCardId}/{scoreCardVariableId}/scorecardvariable")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> DeleteScoreCardVariable(string scoreCardId, string scoreCardVariableId)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.DeleteScoreCardVariable(scoreCardId, scoreCardVariableId); return NoContentResult; });
        }
        #endregion

        #region pricingstrategy

        /// <summary>
        /// Run pricing strategy.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="productId"></param>
        /// <param name="pricingStrategyName"></param>
        /// <param name="version"></param>
        /// <param name="secondaryName"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/{productId}/{pricingStrategyName}/{version}/pricingstrategy/{secondaryName?}")]
        [ProducesResponseType(typeof(IProductRuleResult),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]       
        public async Task<IActionResult> RunPricingStrategy(string entityType, string entityId, string productId, string pricingStrategyName, string version,string secondaryName)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.RunPricingStrategy(entityType, entityId, productId, pricingStrategyName, version, secondaryName)));
        }

        /// <summary>
        /// Get all pricing strategy.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/all/pricingstrategy")]
        [ProducesResponseType(typeof(IPricingStrategy), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
       
        public async Task<IActionResult> GetAllPricingStrategy(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetAllPricingStrategy(entityType));
            });
        }

        /// <summary>
        /// Add pricing strategy.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="pricingStrategyRequestModel"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{productId}/pricingstrategy")]
        [ProducesResponseType(typeof(IPricingStrategy), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddPricingStrategy(string entityType, string productId, [FromBody]PricingStrategyRequest pricingStrategyRequestModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.AddPricingStrategy(entityType, productId, pricingStrategyRequestModel)));
        }

        /// <summary>
        /// Get pricing strategy by id.
        /// </summary>
        /// <param name="pricingStrategyId"></param>
        /// <returns></returns>
        [HttpGet("{pricingStrategyId}/pricingstrategy")]
        [ProducesResponseType(typeof(IPricingStrategy), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPricingStrategy(string pricingStrategyId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.GetPricingStrategy(pricingStrategyId)));
        }

        /// <summary>
        /// Get strategy history.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="productId"></param>
        /// <param name="strategyId"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{productId}/{strategyId}/strategyhistory")]
        [ProducesResponseType(typeof(IStrategyHistory), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> GetStrategyHistory(string entityType, string entityId, string productId, string strategyId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.GetStrategyHistory(entityType, entityId, productId, strategyId)));
        }

        /// <summary>
        /// Update pricing strategy.
        /// </summary>
        /// <param name="pricingStrategyId"></param>
        /// <param name="pricingStrategyRequestModel"></param>
        /// <returns></returns>
        [HttpPut("{pricingStrategyId}/pricingstrategy")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> UpdatePricingStrategy(string pricingStrategyId, [FromBody]PricingStrategyRequest pricingStrategyRequestModel)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.UpdatePricingStrategy(pricingStrategyId, pricingStrategyRequestModel); return NoContentResult; });
        }

        /// <summary>
        /// Delete pricing strategy.
        /// </summary>
        /// <param name="pricingStrategyId"></param>
        /// <returns></returns>
        [HttpDelete("{pricingStrategyId}/pricingstrategy")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeletePricingStrategy(string pricingStrategyId)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.DeletePricingStrategy(pricingStrategyId); return NoContentResult; });
        }

        /// <summary>
        /// Add pricing strategy steps.
        /// </summary>
        /// <param name="pricingStrategyId"></param>
        /// <param name="pricingStrategyStepRequest"></param>
        /// <returns></returns>
        [HttpPost("{pricingStrategyId}/pricingstrategystep")]
        [ProducesResponseType(typeof(IPricingStrategy),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]        
        public async Task<IActionResult> AddPricingStrategyStep(string pricingStrategyId, [FromBody] PricingStrategyStepRequest pricingStrategyStepRequest)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.AddPricingStrategyStep(pricingStrategyId, pricingStrategyStepRequest)));
        }

        /// <summary>
        /// Get pricing strategy step.
        /// </summary>
        /// <param name="pricingStrategyId"></param>
        /// <param name="pricingStrategyStepId"></param>
        /// <returns></returns>
        [HttpGet("{pricingStrategyId}/{pricingStrategyStepId}/pricingstrategystep")]
        [ProducesResponseType(typeof(IPricingStrategyStep), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.GetPricingStrategyStep(pricingStrategyId, pricingStrategyStepId)));
        }

        /// <summary>
        /// Update pricing strategy step.
        /// </summary>
        /// <param name="pricingStrategyId"></param>
        /// <param name="pricingStrategyStepId"></param>
        /// <param name="pricingStrategyStepRequest"></param>
        /// <returns></returns>
        [HttpPut("{pricingStrategyId}/{pricingStrategyStepId}/pricingstrategystep")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdatePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId, [FromBody] PricingStrategyStepRequest pricingStrategyStepRequest)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.UpdatePricingStrategyStep(pricingStrategyId, pricingStrategyStepId, pricingStrategyStepRequest); return NoContentResult; });
        }

        /// <summary>
        /// Delete pricing strategy step.
        /// </summary>
        /// <param name="pricingStrategyId"></param>
        /// <param name="pricingStrategyStepId"></param>
        /// <returns></returns>
        [HttpDelete("{pricingStrategyId}/{pricingStrategyStepId}/pricingstrategystep")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeletePricingStrategyStep(string pricingStrategyId, string pricingStrategyStepId)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.DeletePricingStrategyStep(pricingStrategyId, pricingStrategyStepId); return NoContentResult; });
        }
        #endregion

        #region Segment
        /// <summary>
        /// Add Segment with request model data.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="segmentRequestModel"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{productId}/segment")]
        [ProducesResponseType(typeof(ISegment),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddSegment(string entityType, string productId, [FromBody]SegmentRequest segmentRequestModel)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.AddSegment(entityType, productId, segmentRequestModel)));
        }


        /// <summary>
        /// Get all segments.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/all/segment")]
        [ProducesResponseType(typeof(List<ISegment>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        
        public async Task<IActionResult> GetAllSegment(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetAllSegment(entityType));
            });
        }

        /// <summary>
        /// Get segment by id.
        /// </summary>
        /// <param name="segmentId"></param>
        /// <returns></returns>
        [HttpGet("{segmentId}/segment")]
        [ProducesResponseType(typeof(ISegment), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetSegment(string segmentId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.GetSegment(segmentId)));
        }

        /// <summary>
        /// Update segment by id.
        /// </summary>
        /// <param name="segmentId"></param>
        /// <param name="segmentRequestModel"></param>
        /// <returns></returns>
        [HttpPut("{segmentId}/segment")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateSegment(string segmentId, [FromBody]SegmentRequest segmentRequestModel)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.UpdateSegment(segmentId, segmentRequestModel); return NoContentResult; });
        }

        /// <summary>
        /// Delete segment by id.
        /// </summary>
        /// <param name="segmentId"></param>
        /// <returns></returns>
        [HttpDelete("{segmentId}/segment")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteSegment(string segmentId)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.DeleteSegment(segmentId); return NoContentResult; });
        }
        #endregion

        /// <summary>
        /// Get all rule definitions by rule type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="ruleType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{productId}/{ruleType}/ruledefinitions")]
        [ProducesResponseType(typeof(List<IRuleDefinition>),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRuleDefinitions(string entityType, string productId, string ruleType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetAllRuleDefinition(entityType, productId, ruleType));
            });
        }

        /// <summary>
        /// Get rule definition details.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="className"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/ruledetails/{className?}/{productId?}")]
        [ProducesResponseType(typeof(List<IRuleDefinition>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRuleDetails(string entityType, string className, string productId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetRuleDefinitionDetails(entityType, className, productId));
            });
        }

        /// <summary>
        /// Get rule definition by name.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="ruleName"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{productId}/{ruleName}/ruledefinition")]
        [ProducesResponseType(typeof(IRuleDefinition), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRuleDefinition(string entityType, string productId, string ruleName)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetRuleDefinitionByName(entityType, productId, ruleName));
            });
        }
        /// <summary>
        /// Get rule definition by id.
        /// </summary>
        /// <param name="ruleDefinitionId"></param>
        /// <returns></returns>
        [HttpGet("{ruleDefinitionId}/ruledefinition")]
        [ProducesResponseType(typeof(IRuleDefinition), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetRuleDefinitionById(string ruleDefinitionId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetRuleDefinitionById(ruleDefinitionId));
            });
        }


        /// <summary>
        /// Get lookup by name
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="name"></param>
        /// <returns></returns>

        [HttpGet("{entityType}/{productId}/{name}/lookup")]
        [ProducesResponseType(typeof(ILookup), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLookupByName(string entityType, string productId, string name)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetLookupByName(entityType, productId, name));
            });
        }
        /// <summary>
        /// Get all lookup data by entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/all/lookup")]
        [ProducesResponseType(typeof(List<ILookup>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllLookup(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetAllLookup(entityType));
            });
        }

        /// <summary>
        /// Get segment by name.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{productId}/{name}/segment")]
        [ProducesResponseType(typeof(ISegment), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        
        public async Task<IActionResult> GetSegmentByName(string entityType, string productId, string name)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetSegmentByName(entityType, productId, name));
            });
        }


        /// <summary>
        /// Get score card by name
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{productId}/{name}/scorecard")]
        [ProducesResponseType(typeof(IScoreCard), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetScorecardByName(string entityType, string productId, string name)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetScoreCardByName(entityType, productId, name));
            });
        }

        /// <summary>
        /// Get all score card by entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/all/scorecard")]
        [ProducesResponseType(typeof(List<IScoreCard>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllScoreCard(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetAllScoreCard(entityType));
            });
        }

        /// <summary>
        /// Add rule definition by entity type and product id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{productId}/ruledefinition")]
        [ProducesResponseType(typeof(IRuleDefinition), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddRuleDefinition(string entityType, string productId, [FromBody]RuleDefinitionAddRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.AddRuleDefinition(entityType, productId, request));
            });
        }

        /// <summary>
        /// Update rule definition by rule id.
        /// </summary>
        /// <param name="ruleId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{ruleId}/ruledefinition")]
        [ProducesResponseType(typeof(IRuleDefinition), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateRuleDefinition(string ruleId, [FromBody]RuleDefinitionUpdateRequest request)
        {
            return Ok(await ProductRuleService.UpdateRuleDefinition(ruleId, request));
        }

        /// <summary>
        /// Delete rule definition by rule id.
        /// </summary>
        /// <param name="ruleId"></param>
        /// <returns></returns>
        [HttpDelete("{ruleId}/ruledefinition")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteRuleDefinition(string ruleId)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.DeleteRule(ruleId); return new NoContentResult(); });
        }

        /// <summary>
        /// Get lookup by lookup id.
        /// </summary>
        /// <param name="lookupId"></param>
        /// <returns></returns>
        [HttpGet("{lookupId}/lookup")]
        [ProducesResponseType(typeof(ILookup), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLookUp(string lookupId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetLookUp(lookupId));
            });
        }

        /// <summary>
        /// Add lookup by entity type and product id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{productId}/lookup")]
        [ProducesResponseType(typeof(ILookup), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddLookUp(string entityType, string productId, [FromBody]AddLookupRequest request)
        {

            return await ExecuteAsync(async () =>
             {
                 return Ok(await ProductRuleService.AddLookUp(entityType, productId, request));
             });
        }

        /// <summary>
        /// Update lookup by lookup id.
        /// </summary>
        /// <param name="lookupId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{lookupId}/lookup")]
        [ProducesResponseType(typeof(ILookup), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateLookUp(string lookupId, [FromBody]UpdateLookupRequest request)
        {
            return Ok(await ProductRuleService.UpdateLookUp(lookupId, request));
        }

        /// <summary>
        /// Delete lookup by lookup id.
        /// </summary>
        /// <param name="lookupId"></param>
        /// <returns></returns>
        [HttpDelete("{lookupId}/lookup")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteLookUp(string lookupId)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.DeleteLookUp(lookupId); return new NoContentResult(); });
        }

        /// <summary>
        /// Get product rule detail by entity type and entity id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(List<IProductRuleResult>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
       
        public async Task<IActionResult> GetProductRuleResultDetail(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.GetProductRuleResultDetail(entityType, entityId)));
        }

        /// <summary>
        /// Validate score card by entity type, product id and score card id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="scoreCardId"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{productId}/{scoreCardId}/validatescorecard")]
        [ProducesResponseType(typeof(IProductRuleSourceResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]       
        public async Task<IActionResult> ValidateScoreCard(string entityType, string productId, string scoreCardId)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.ValidateScoreCard(entityType, productId, scoreCardId)));
        }


        /// <summary>
        /// Get all template by entity type.
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/all/template")]
        [ProducesResponseType(typeof(List<ITemplate>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]       
        public async Task<IActionResult> GetAllTemplate(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetAllTemplate(entityType));
            });
        }

        /// <summary>
        /// Get template by event name.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{productId}/{name}/template")]
        [ProducesResponseType(typeof(ITemplate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTemplateByEvent(string entityType, string productId, string name)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await ProductRuleService.GetByEventName(entityType, productId, name));
            });
        }

        /// <summary>
        /// Add template by entity type and product id.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="productId"></param>
        /// <param name="templateAddRequest"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{productId}/template")]
        [ProducesResponseType(typeof(ITemplate), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddTemplate(string entityType, string productId, [FromBody]TemplateAddRequest templateAddRequest)
        {
            return await ExecuteAsync(async () => Ok(await ProductRuleService.AddTemplate(entityType, productId, templateAddRequest)));
        }

        /// <summary>
        /// Update template by template id.
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="templateRequest"></param>
        /// <returns></returns>
        [HttpPut("{templateId}/template")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateTemplate(string templateId, [FromBody]TemplateAddRequest templateRequest)
        {
            return await ExecuteAsync(async () => { await ProductRuleService.UpdateTemplate(templateId, templateRequest); return NoContentResult; });
        }


    }
}
