﻿using LendFoundry.ProductRule.Configurations;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Linq;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;

namespace LendFoundry.ProductRule.Persistence
{
    public class ProductRuleLookupRepository : MongoRepository<ILookup, Lookup>, IProductRuleLookupRepository
    {
        static ProductRuleLookupRepository()
        {
            BsonClassMap.RegisterClassMap<Lookup>(map =>
            {
                map.AutoMap();

                var type = typeof(Lookup);
                map.MapProperty(s => s.Data).SetSerializer(new JsonSerializer<object>());
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public ProductRuleLookupRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "lookup")
        {
            CreateIndexIfNotExists("lookup_id", Builders<ILookup>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
        }

        public async Task<List<ILookup>> GetAllLookup(string entityType)
        {
            return await Task.Run(() => Query.Where(x => x.EntityType == entityType).ToList());
        }
        public async Task<ILookup> GetById(string id)
        {
            return await Query.FirstOrDefaultAsync(sc => sc.Id == id);
        }
        public async Task<ILookup> GetByName(string entityType, string productId, string name)
        {
            return await Query.FirstOrDefaultAsync(sc => sc.EntityType == entityType && sc.ProductId == productId && sc.LookupName.ToLower() == name.ToLower());
        }
        public async Task<ILookup> Update(string id, ILookup updateRequest)
        {
            var lookupData = await GetById(id);
            if (lookupData == null)
            {
                throw new NotFoundException($"Lookup with id {id} is not found");
            }
            lookupData.LookupBy = updateRequest.LookupBy;
            lookupData.LookupValues = updateRequest.LookupValues;
            lookupData.LookupStorageName = updateRequest.LookupStorageName;
            lookupData.DataAttributeStoreName = updateRequest.DataAttributeStoreName;
            lookupData.Description = updateRequest.Description;
            lookupData.LastUpdatedBy = updateRequest.LastUpdatedBy;
            lookupData.EffectiveDate = updateRequest.EffectiveDate;
            lookupData.ExpiryDate = updateRequest.ExpiryDate;
            lookupData.LastUpdatedDate = updateRequest.LastUpdatedDate;
            lookupData.RuleDefinitionId = updateRequest.RuleDefinitionId;
            lookupData.ApplicationType = updateRequest.ApplicationType;
            lookupData.CustomDate = updateRequest.CustomDate;
            lookupData.Data = updateRequest.Data;

            await Collection.UpdateOneAsync(Builders<ILookup>.Filter.Where(a => a.TenantId == lookupData.TenantId &&
                                                                         a.Id == id),
               Builders<ILookup>.Update.Set(a => a.LookupBy, updateRequest.LookupBy)
                   .Set(a => a.LookupValues, updateRequest.LookupValues)
                   .Set(a => a.LookupStorageName, updateRequest.LookupStorageName)
                   .Set(a => a.DataAttributeStoreName, updateRequest.DataAttributeStoreName)
                   .Set(a => a.Description, updateRequest.Description)
                   .Set(a => a.LastUpdatedBy, updateRequest.LastUpdatedBy)
                   .Set(a => a.EffectiveDate, updateRequest.EffectiveDate)
                   .Set(a => a.ExpiryDate, updateRequest.ExpiryDate)
                   .Set(a => a.LastUpdatedDate, updateRequest.LastUpdatedDate)
                   .Set(a => a.RuleDefinitionId, updateRequest.RuleDefinitionId)
                   .Set(a => a.Data, updateRequest.Data)
                   );

            return lookupData;
        }
    }
}
