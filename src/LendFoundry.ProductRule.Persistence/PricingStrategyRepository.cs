﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule.Persistence
{
    public class PricingStrategyRepository : MongoRepository<IPricingStrategy, PricingStrategy>, IPricingStrategyRepository
    {
        static PricingStrategyRepository()
        {
            BsonClassMap.RegisterClassMap<PricingStrategy>(map =>
            {
                map.AutoMap();
                //map.MapProperty(s => s.EffectiveDate).SetSerializer(new timebucket(BsonType.Document));
                //map.MapProperty(s => s.ExpiryDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(ScoreCard);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<PricingStrategyStep>(map =>
            {
                map.AutoMap();
                var type = typeof(ScoreCardVariable);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            

        }
        public PricingStrategyRepository(ITenantService tenantService, IMongoConfiguration configuration)
        : base(tenantService, configuration, "pricingstrategy")
        {
            CreateIndexIfNotExists("pricingstrategy_id",
                Builders<IPricingStrategy>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.ProductId));
        }



        public async Task<List<IPricingStrategy>> GetAllPricingStrategy(string entityType)
        {
            return await Task.Run(() => Query.Where(x => x.EntityType == entityType ).ToList());
        }

        public async Task<IPricingStrategy> GetByNameAndVersion(string entityType, string productId, string Name,string Version)
        {
            return await Task.Run(() =>
            {
                return Query.Where(fact => fact.EntityType == entityType && fact.ProductId == productId && fact.Name.ToLower() == Name.ToLower() && fact.Version ==Version).FirstOrDefault();
            });
        }

       
    }
}
