﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.ProductRule;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule.Persistence
{
    public class StrategyExceutionHistoryRepository : MongoRepository<IStrategyHistory, StrategyHistory>, IStrategyExceutionHistoryRepository
    {
        static StrategyExceutionHistoryRepository()
        {
            BsonClassMap.RegisterClassMap<StrategyHistory>(map =>
            {
                map.AutoMap();
                //map.MapProperty(s => s.EffectiveDate).SetSerializer(new timebucket(BsonType.Document));
                //map.MapProperty(s => s.ExpiryDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapProperty(s => s.IntermediateData).SetSerializer(new JsonSerializer<object>());
                var type = typeof(StrategyHistory);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            
            

        }
        public StrategyExceutionHistoryRepository(ITenantService tenantService, IMongoConfiguration configuration)
        : base(tenantService, configuration, "strategyexecutionhistory")
        {
            CreateIndexIfNotExists("strategyhistory_id",
                Builders<IStrategyHistory>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.ProductId));
        }



       

        public async Task<List<IStrategyHistory>> GetByStrategyId(string entityType,string entityId, string productId, string strategyId)
        {
            return await Task.Run(() =>
            {
                return Query.Where(fact => fact.EntityType.ToLower() == entityType.ToLower() && fact.EntityId == entityId && fact.ProductId == productId && fact.StrategyId == strategyId).ToList();
            });
        }

    }
}
