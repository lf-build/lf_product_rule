﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule.Persistence
{
    public class TemplateRepository : MongoRepository<ITemplate, Template>, ITemplateRepository
    {
        static TemplateRepository()
        {
            BsonClassMap.RegisterClassMap<Template>(map =>
            {
                map.AutoMap();
                //map.MapProperty(s => s.EffectiveDate).SetSerializer(new timebucket(BsonType.Document));
                //map.MapProperty(s => s.ExpiryDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(Template);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
          
            

        }
        public TemplateRepository(ITenantService tenantService, IMongoConfiguration configuration)
        : base(tenantService, configuration, "template")
        {
            CreateIndexIfNotExists("template_id",
                Builders<ITemplate>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.ProductId));
        }



        public async Task<List<ITemplate>> GetAllTemplate(string entityType)
        {
            return await Task.Run(() => Query.Where(x => x.EntityType == entityType).ToList());
        }

        public async Task<ITemplate> GetByEventName(string entityType, string productId, string eventName, DateTimeOffset currentDate)
        {
            return await Task.Run(() =>
            {
                return Query.Where(fact => fact.EntityType == entityType && fact.ProductId == productId && fact.EventName == eventName && fact.EffectiveDate.Time <= currentDate && fact.ExpiryDate.Time >= currentDate).FirstOrDefault();
            });
        }

        //public async Task<ITemplate> GetEmailByNameAndVersion(string entityType, string productId, string Name, string Version)
        //{
        //    return await Task.Run(() =>
        //    {
        //        return Query.Where(fact => fact.EntityType == entityType && fact.ProductId == productId && fact.EmailTemplateName.ToLower() == Name.ToLower() && fact.EmailTemplateVersion == Version).FirstOrDefault();
        //    });
        //}

        //public async Task<ITemplate> GetMobileByNameAndVersion(string entityType, string productId, string Name, string Version)
        //{
        //    return await Task.Run(() =>
        //    {
        //        return Query.Where(fact => fact.EntityType == entityType && fact.ProductId == productId && fact.MobileTemplateName.ToLower() == Name.ToLower() && fact.MobileTemplateVersion == Version).FirstOrDefault();
        //    });
        //}

    }
}
