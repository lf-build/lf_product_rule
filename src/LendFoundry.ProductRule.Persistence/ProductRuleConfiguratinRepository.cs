﻿using LendFoundry.ProductRule.Configurations;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;

namespace LendFoundry.ProductRule.Persistence
{
    //public class ProductRuleConfiguratinRepository : MongoRepository<IRuleDefinantionDetail, RuleDefinantionDetail>, IProductRuleConfiguratinRepository
    //{
    //    static ProductRuleConfiguratinRepository()
    //    {
    //        BsonClassMap.RegisterClassMap<RuleDefinantionDetail>(map =>
    //        {
    //            map.AutoMap();
    //            //map.MapProperty(s => s.EffectiveDate).SetSerializer(new timebucket(BsonType.Document));
    //            //map.MapProperty(s => s.ExpiryDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
               
    //            var type = typeof(RuleDefinantionDetail);
    //            map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
    //            map.SetIsRootClass(true);
    //        });
    //        BsonClassMap.RegisterClassMap<ScoringVariable>(map =>
    //        {
    //            map.AutoMap();
    //            var type = typeof(ScoringVariable);
    //            map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
    //            map.SetIsRootClass(true);
    //        });
    //        BsonClassMap.RegisterClassMap<SegmentationRange>(map =>
    //        {
    //            map.AutoMap();
    //            var type = typeof(SegmentationRange);
    //            map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
    //            map.SetIsRootClass(true);
    //        });
    //        BsonClassMap.RegisterClassMap<RuleDetail>(map =>
    //        {
    //            map.AutoMap();
    //            var type = typeof(RuleDetail);
    //            map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
    //            map.SetIsRootClass(true);
    //        });
    //        BsonClassMap.RegisterClassMap<TimeBucket>(map =>
    //        {
    //            map.AutoMap();
    //            map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

    //            var type = typeof(TimeBucket);
    //            map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
    //            map.SetIsRootClass(false);
    //        });
    //    }
    //    public ProductRuleConfiguratinRepository(ITenantService tenantService, IMongoConfiguration configuration)
    //        : base(tenantService, configuration, "configuration")
    //    {
    //        CreateIndexIfNotExists("configuration_id",
    //            Builders<IRuleDefinantionDetail>.IndexKeys.Ascending(i => i.EntityType)
    //                .Ascending(i => i.ProductId));
    //    }

    //    public async Task<List<IRuleDefinantionDetail>> GetProductRuleConfigurationDetails(string entityType, string productId,string ruleDefination,DateTimeOffset currentDate)
    //    {
    //        return  Query.Where(sc => sc.EntityType == entityType && sc.ProductId == productId && sc.RuleDefinition == ruleDefination && (sc.EffectiveDate.Time.Date) <= (currentDate.Date) && sc.ExpiryDate.Time > currentDate.Date).ToList();
    //    }
    //}
}
