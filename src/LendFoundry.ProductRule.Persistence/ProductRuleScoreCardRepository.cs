﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule.Persistence
{
    public class ProductRuleScoreCardRepository : MongoRepository<IScoreCard, ScoreCard>, IProductRuleScoreCardRepository
    {
        static ProductRuleScoreCardRepository()
        {
            BsonClassMap.RegisterClassMap<ScoreCard>(map =>
            {
                map.AutoMap();
                //map.MapProperty(s => s.EffectiveDate).SetSerializer(new timebucket(BsonType.Document));
                //map.MapProperty(s => s.ExpiryDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(ScoreCard);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<ScoreCardVariable>(map =>
            {
                map.AutoMap();
                var type = typeof(ScoreCardVariable);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<VariableSource>(map =>
            {
                map.AutoMap();
                var type = typeof(VariableSource);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

        }
        public ProductRuleScoreCardRepository(ITenantService tenantService, IMongoConfiguration configuration)
        : base(tenantService, configuration, "scorecard")
        {
            CreateIndexIfNotExists("scorecard_id",
                Builders<IScoreCard>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.ProductId));
        }



        public async Task<List<IScoreCard>> GetAllScoreCard(string entityType)
        {
            return await Task.Run(() => Query.Where(x => x.EntityType == entityType ).ToList());
        }

        public async Task<IScoreCard> GetByName(string entityType, string productId, string Name)
        {
            return await Task.Run(() =>
            {
                return Query.Where(fact => fact.EntityType == entityType && fact.ProductId == productId && fact.ScoreCardName.ToLower() == Name.ToLower()).FirstOrDefault();
            });
        }

    }
}
