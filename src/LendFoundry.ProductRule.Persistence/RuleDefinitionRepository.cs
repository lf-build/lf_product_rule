﻿using LendFoundry.ProductRule.Configurations;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Linq;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;

namespace LendFoundry.ProductRule.Persistence
{
    public class RuleDefinitionRepository : MongoRepository<IRuleDefinition, RuleDefinition>, IRuleDefinitionRepository
    {
        static RuleDefinitionRepository()
        {
            BsonClassMap.RegisterClassMap<RuleDefinition>(map =>
            {
                map.AutoMap();

                var type = typeof(Lookup);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<RuleDetail>(map =>
            {
                map.AutoMap();

                var type = typeof(RuleDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            //BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            //{
            //    map.AutoMap();
            //    map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

            //    var type = typeof(TimeBucket);
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});

        }
        public RuleDefinitionRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "ruledefinitions")
        {
            CreateIndexIfNotExists("rule_id", Builders<IRuleDefinition>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Id));
        }
        public async Task<IRuleDefinition> GetById(string ruleId)
        {
            return await Query.FirstOrDefaultAsync(sc => sc.Id == ruleId);
        }
        public async Task<IRuleDefinition> GetByName(string entityType, string productId, string ruleName)
        {
            return await Query.FirstOrDefaultAsync(sc => sc.EntityType == entityType && sc.ProductId == productId && sc.RuleName.ToLower() == ruleName.ToLower());
        }

        public async Task<List<IRuleDefinition>> GetByRuleDetails(string entityType, string className, string productId)
        {
            if (!string.IsNullOrEmpty(productId) && !string.IsNullOrEmpty(className))
                return await Query.Where(sc => sc.EntityType == entityType && sc.ProductId == productId && sc.RuleClass.ToLower() == className.ToLower()).ToListAsync();
            else if (!string.IsNullOrEmpty(className))
                return await Query.Where(sc => sc.EntityType == entityType  && sc.RuleClass.ToLower() == className.ToLower()).ToListAsync();
            else
                return await Query.Where(sc => sc.EntityType == entityType).ToListAsync();

        }
        public async Task<List<IRuleDefinition>> GetAll(string entityType, string productId, string ruleName)
        {
            return await Query.Where(sc => sc.EntityType == entityType && sc.ProductId == productId).ToListAsync();
        }
        public async Task<IRuleDefinition> Update(string ruleId, IRuleDefinitionUpdateRequest updateRuleRequest)
        {
            var ruleData = await GetById(ruleId);
            if (ruleData == null)
            {
                throw new NotFoundException($"Rule definition with id {ruleId} is not found");
            }
            if (ruleData.RuleDetail == null)
            {
                ruleData.RuleDetail = new RuleDetail
                {
                    DERuleName = updateRuleRequest.DERuleName,
                    OptionalSources = updateRuleRequest.OptionalSources,
                    RequiredSources = updateRuleRequest.RequiredSources,
                    RuleVersion = updateRuleRequest.RuleVersion
                };
            }
            else
            {
                ruleData.RuleDetail.DERuleName = updateRuleRequest.DERuleName;
                ruleData.RuleDetail.OptionalSources = updateRuleRequest.OptionalSources;
                ruleData.RuleDetail.RequiredSources = updateRuleRequest.RequiredSources;
                ruleData.RuleDetail.RuleVersion = updateRuleRequest.RuleVersion;
            }
            ruleData.DataAttributeStoreName = updateRuleRequest.DataAttributeStoreName;
            ruleData.Description = updateRuleRequest.Description;
            ruleData.EffectiveDate = new TimeBucket(updateRuleRequest.EffectiveDate);
            ruleData.ExpiryDate = new TimeBucket(updateRuleRequest.ExpiryDate);
            ruleData.LastUpdatedBy = updateRuleRequest.LastUpdatedBy;
            ruleData.LastUpdatedDate = updateRuleRequest.LastUpdatedDate;
            ruleData.ApplicationType = updateRuleRequest.ApplicationType;
            ruleData.CustomDate = new TimeBucket(updateRuleRequest.CustomDate);
            ruleData.RuleClass = updateRuleRequest.RuleClass;

            await Collection.ReplaceOneAsync(Builders<IRuleDefinition>.Filter.Where(a => a.TenantId == ruleData.TenantId &&
                                                                         a.Id == ruleId), ruleData);

            return ruleData;
        }

    }
}
