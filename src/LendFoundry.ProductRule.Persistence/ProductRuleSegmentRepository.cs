﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.ProductRule.Persistence
{
    public class ProductRuleSegmentRepository : MongoRepository<ISegment, Segment>, IProductRuleSegmentRepository
    {
        static ProductRuleSegmentRepository()
        {
            BsonClassMap.RegisterClassMap<Segment>(map =>
            {
                map.AutoMap();
                //map.MapProperty(s => s.EffectiveDate).SetSerializer(new timebucket(BsonType.Document));
                //map.MapProperty(s => s.ExpiryDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(Segment);
                map.MapProperty(s => s.Data).SetSerializer(new JsonSerializer<object>());
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<SegmentType>(map =>
            {
                map.AutoMap();
                var type = typeof(SegmentType);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            //BsonClassMap.RegisterClassMap<VariableSource>(map =>
            //{
            //    map.AutoMap();
            //    var type = typeof(VariableSource);
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(true);
            //});

          
        }
        public ProductRuleSegmentRepository(ITenantService tenantService, IMongoConfiguration configuration)
        : base(tenantService, configuration, "segment")
        {
            CreateIndexIfNotExists("segment_id",
                Builders<ISegment>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.ProductId));
        }

        public async Task<List<ISegment>> GetAllSegment(string entityType)
        {
            return await Task.Run(() => Query.Where(x => x.EntityType == entityType).ToList());
        }
        public async Task<ISegment> GetByName(string entityType, string productId, string Name)
        {
            return await Task.Run(() =>
            {
                return Query.Where(fact => fact.EntityType == entityType && fact.ProductId == productId && fact.SegmentName.ToLower() == Name.ToLower()).FirstOrDefault();
            });
        }
    }
}
