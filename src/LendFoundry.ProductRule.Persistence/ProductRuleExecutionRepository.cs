﻿using LendFoundry.ProductRule.Configurations;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;


namespace LendFoundry.ProductRule.Persistence
{
    public class JsonSerializer<T> : IBsonSerializer<object>
    {
        public object Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            var valueAsJson = JsonConvert.SerializeObject(value);
            context.Writer.WriteString(valueAsJson);
        }

        public Type ValueType => typeof(T);
    }
    public class ProductRuleExecutionRepository : MongoRepository<IProductRuleResult, ProductRuleResult>, IProductRuleExecutionRepository
    {
        static ProductRuleExecutionRepository()
        {
            BsonClassMap.RegisterClassMap<ProductRuleResult>(map =>
            {
                map.AutoMap();

            //    map.MapProperty(s => s.ExecutedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapProperty(s => s.IntermediateData).SetSerializer(new JsonSerializer<object>());
                map.MapProperty(s => s.SourceData).SetSerializer(new JsonSerializer<Dictionary<string, object>>());
                var type = typeof(ProductRuleResult);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
            BsonClassMap.RegisterClassMap<ProductRuleDetail>(map =>
            {
                map.AutoMap();
                var type = typeof(ProductRuleDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            //BsonClassMap.RegisterClassMap<RuleDetail>(map =>
            //{
            //    map.AutoMap();
            //    var type = typeof(RuleDetail);
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(true);
            //});   
        }
        public ProductRuleExecutionRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "ProductRuleresult")
        {
            CreateIndexIfNotExists("ruleresult_id",
                Builders<IProductRuleResult>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId));
        }

        public async Task<List<IProductRuleResult>> GetProductRuleDetails(string entityType, string entityId)
        {
            return await Query.Where(sc => sc.EntityType == entityType && sc.EntityId == entityId).ToListAsync();
        }
    }
}
